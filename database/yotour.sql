-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2020 at 01:29 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yotour`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint(127) NOT NULL,
  `channel_id` bigint(127) NOT NULL,
  `tour_type_id` bigint(127) NOT NULL,
  `destination_id` bigint(127) NOT NULL,
  `tour_id` bigint(127) NOT NULL,
  `number_of_people` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `booking_amount` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `booking_amt_paytype` int(11) NOT NULL,
  `discount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_amt_paytupe` int(11) DEFAULT NULL,
  `reason_additional_amount` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` bigint(127) NOT NULL,
  `booking_reference` text COLLATE utf8_unicode_ci NOT NULL,
  `is_allocate` tinyint(1) NOT NULL DEFAULT 0,
  `storyteller` bigint(127) NOT NULL DEFAULT 0,
  `rescheduleMsg` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancelMsg` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 Unconfirmed, 1 Confirmed, 2 Cancel',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `channel_id`, `tour_type_id`, `destination_id`, `tour_id`, `number_of_people`, `date`, `time`, `booking_amount`, `booking_amt_paytype`, `discount`, `additional_amount`, `additional_amt_paytupe`, `reason_additional_amount`, `customer_id`, `booking_reference`, `is_allocate`, `storyteller`, `rescheduleMsg`, `cancelMsg`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 4, 8, 2, '11', '2019-12-30', '15:38:15', '500', 2, '100', '200', 2, 'test', 2, 'test', 1, 2, 'test', 'test sdfsdf', 1, '2019-12-18 13:08:04', '2019-11-19 08:12:22'),
(2, 2, 4, 8, 2, '5', '2019-11-19', '20:05:45', '600', 2, NULL, NULL, NULL, NULL, 2, 'test', 1, 3, NULL, NULL, 1, '2019-11-19 13:45:57', '2019-11-19 08:15:57'),
(3, 2, 3, 7, 1, '4', '2019-12-24', '17:10:30', '600', 2, '100', '200', 2, 'test', 2, 'test', 0, 0, NULL, NULL, 0, '2020-01-08 10:36:11', '2019-11-19 06:10:53'),
(4, 2, 3, 7, 1, '6', '2019-11-30', '17:54:00', '900', 2, '100', '200', 2, 'test', 2, 'test', 1, 3, NULL, NULL, 1, '2020-01-08 10:36:09', '2019-11-19 08:12:40'),
(5, 2, 3, 7, 2, '11', '2019-11-28', '16:50:00', '122', 2, '100', NULL, 2, 'test', 2, 'test', 0, 0, NULL, NULL, 0, '2020-01-08 10:36:07', '2019-11-20 05:51:52'),
(6, 2, 1, 6, 1, '3', '2020-01-31', '15:41:30', '100', 2, NULL, NULL, NULL, NULL, 2, 'test', 1, 2, 'test', NULL, 0, '2020-01-08 10:51:57', '2020-01-08 05:21:57');

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE `channels` (
  `id` bigint(127) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`id`, `name`, `website`, `logo`, `status`, `created_at`, `updated_at`) VALUES
(2, 'test', 'https://test.com/', '10080297441573649013.jpg', 1, '2019-11-13 07:13:33', '2019-11-13 07:13:33'),
(3, 'Lorem Ipsum', NULL, NULL, 1, '2019-11-15 01:20:40', '2019-11-15 01:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `combos`
--

CREATE TABLE `combos` (
  `id` bigint(127) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_price` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `combo_price` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `combos`
--

INSERT INTO `combos` (`id`, `title`, `slug`, `original_price`, `combo_price`, `description`, `status`, `created_at`, `updated_at`) VALUES
(4, 'SOFT PLAY HIRE', 'soft-play-hire', '100', '50', 'asdas asdasd', 1, '2020-01-14 06:54:35', '2020-01-14 06:54:35');

-- --------------------------------------------------------

--
-- Table structure for table `combo_products`
--

CREATE TABLE `combo_products` (
  `id` bigint(127) NOT NULL,
  `combo_id` bigint(127) DEFAULT NULL,
  `product_id` bigint(127) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `combo_products`
--

INSERT INTO `combo_products` (`id`, `combo_id`, `product_id`) VALUES
(8, 4, 1),
(9, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `slug`, `status`) VALUES
(2, 'india', 'india', 1),
(3, 'America', 'america', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(127) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `coupon_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `min_amount` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1 percentage, 2 amount',
  `discount_amt_per` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `max_discount_amount` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `name`, `coupon_code`, `start_date`, `expire_date`, `min_amount`, `type`, `discount_amt_per`, `max_discount_amount`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'test', 'blgq3l', '2019-11-14', '2019-11-30', '500', 2, '100', '100', '<p>test</p>', 1, '2019-11-14 07:06:02', '2019-11-14 01:36:02');

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `id` bigint(127) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_top` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`id`, `name`, `slug`, `country_id`, `featured_img`, `banner_img`, `description`, `is_top`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Bhopal', 'bhopal', 2, '12655795341573643567.jpg', '1573643567.jpg', '<p>test</p>', 1, 1, '2019-12-19 13:03:30', '2019-12-19 07:33:30'),
(7, 'Indore', 'indore', 2, '16728367671573643588.jpg', '1573643588.jpg', '<p>test</p>', 0, 1, '2019-12-19 13:03:15', '2019-12-19 07:33:15'),
(8, 'test', 'test', 3, NULL, NULL, NULL, 0, 1, '2019-12-19 13:03:22', '2019-12-19 07:33:22'),
(9, 'tes12', 'tes12', 3, NULL, NULL, '<p>ds</p>', 0, 1, '2019-12-19 13:03:38', '2019-12-19 07:33:38');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `paytypes`
--

CREATE TABLE `paytypes` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `paytypes`
--

INSERT INTO `paytypes` (`id`, `name`, `status`) VALUES
(2, 'Prepaid', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(127) NOT NULL,
  `country` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `product_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holiday_category` int(11) DEFAULT NULL,
  `transport_category` int(11) DEFAULT NULL,
  `rent_category` int(11) DEFAULT NULL,
  `rent_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticket_type` int(11) DEFAULT NULL,
  `minimum_age` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tour_category` int(11) DEFAULT NULL,
  `activity_category` int(11) DEFAULT NULL,
  `theme` int(11) DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_link` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_delivery` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `starting_point` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `starting_latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `starting_longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup_latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup_longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dropoff` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `drop_latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `drop_longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tour_language` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_wheelchair` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_stroller` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_animals_allowed` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_travelers_easily` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_infants_required` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_infant_seats_available` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `travelers_with_back_problems` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pregnant_travelers` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serious_medical` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `physical_difficulty` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancellation_policy` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_sub_type` int(11) DEFAULT NULL COMMENT '''1 Tour/Activity'', ''2 Tickets'', ''3 Transport/Rental'', ''4 Services''',
  `product_type` int(11) DEFAULT NULL COMMENT '''1 Experience'', ''2 Holiday''',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `country`, `city`, `product_name`, `slug`, `featured_img`, `holiday_category`, `transport_category`, `rent_category`, `rent_type`, `ticket_type`, `minimum_age`, `tour_category`, `activity_category`, `theme`, `short_description`, `product_description`, `video_link`, `home_delivery`, `starting_point`, `starting_latitude`, `starting_longitude`, `pickup`, `pickup_latitude`, `pickup_longitude`, `dropoff`, `drop_latitude`, `drop_longitude`, `tour_language`, `is_wheelchair`, `is_stroller`, `is_animals_allowed`, `is_travelers_easily`, `is_infants_required`, `is_infant_seats_available`, `travelers_with_back_problems`, `pregnant_travelers`, `serious_medical`, `physical_difficulty`, `cancellation_policy`, `additional_info`, `product_sub_type`, `product_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 6, 'test tour product', 'test-tour-product', '13820747661578641333.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 7, 13, 3, 'adasd123', '<p>asdas asdasdasd asdasd 123</p>', 'yyyyy123', NULL, NULL, NULL, NULL, 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.41261499999996', 'Hoshangabad, Madhya Pradesh, India', '22.7438944', '77.73592239999994', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', '1', '1', NULL, 'Moderate', '1', 'fsfsdfsdfd12', 1, 1, 1, '2020-01-10 07:28:53', '2020-01-10 01:58:53'),
(5, 2, 6, 'SOFT PLAY HIRE 123', 'soft-play-hire-123', '6480698621578641537.jpg', NULL, NULL, NULL, NULL, 52, '25', 0, 0, NULL, NULL, '<p>adasdas</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'test', 2, 1, 1, '2020-01-10 07:32:17', '2020-01-10 02:02:17'),
(6, 2, 7, 'test rental product', 'test-rental-product', '18905501801578641644.jpg', NULL, 57, 60, 'hatchback,sedan', NULL, '25', 0, 0, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, NULL, 'Barcelona, Spain', '41.38506389999999', '2.1734034999999494', 'Brisbane QLD, Australia', '-27.4697707', '153.02512350000006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'test', 3, 1, 1, '2020-01-10 07:34:04', '2020-01-10 02:04:04'),
(7, 2, 7, 'test holiday', 'test-holiday', '9547481971578641727.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Arera Colony, Bhopal, Madhya Pradesh, India', '23.2115829', '77.4311017', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.41261499999996', 'Habibganj, Habibganj Pedestrian overpass, Zone-II, Habib Ganj, Bhopal, Madhya Pradesh, India', '23.2218222', '77.43919729999993', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', '1', '1', NULL, 'Moderate', '1', 'adasdasd', 1, 2, 1, '2020-01-10 07:35:27', '2020-01-10 02:05:27'),
(8, 2, 6, 'test holiday 2', 'test-holiday-2', NULL, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Arera Colony, Bhopal, Madhya Pradesh, India', '23.2115829', '77.4311017', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.41261499999996', 'Habibganj, Habibganj Pedestrian overpass, Zone-II, Habib Ganj, Bhopal, Madhya Pradesh, India', '23.2218222', '77.43919729999993', 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asdas', NULL, 2, 0, '2020-01-08 12:07:03', '2020-01-08 06:37:03'),
(9, 2, 7, 'new tour product', 'new-tour-product', '14554720061578914053.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 6, 11, 3, 'test desc', '<p>testing description</p>', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/02pj1Ew_HMU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, NULL, NULL, NULL, 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Hoshangabad, Madhya Pradesh, India', '22.7438944', '77.73592239999999', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', '1', 'Easy', '1', 'asdas', 1, 1, 0, '2020-01-13 05:44:13', '2020-01-13 05:44:13'),
(10, 2, 7, 'new tour product', 'new-tour-product', '20616932161578914123.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 6, 11, 3, 'test desc', '<p>testing description</p>', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/02pj1Ew_HMU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, NULL, NULL, NULL, 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Hoshangabad, Madhya Pradesh, India', '22.7438944', '77.73592239999999', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', '1', 'Easy', '1', 'asdas', 1, 1, 0, '2020-01-13 05:45:23', '2020-01-13 05:45:23'),
(11, 2, 7, 'new tour product', 'new-tour-product', '18393727191578914138.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 6, 11, 3, 'test desc', '<p>testing description</p>', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/02pj1Ew_HMU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, NULL, NULL, NULL, 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Hoshangabad, Madhya Pradesh, India', '22.7438944', '77.73592239999999', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', '1', 'Easy', '1', 'asdas', 1, 1, 0, '2020-01-13 05:45:38', '2020-01-13 05:45:38'),
(12, 2, 7, 'new tour product', 'new-tour-product', '14972058901578914194.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 6, 11, 3, 'test desc', '<p>testing description</p>', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/02pj1Ew_HMU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, NULL, NULL, NULL, 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Hoshangabad, Madhya Pradesh, India', '22.7438944', '77.73592239999999', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', '1', 'Easy', '1', 'asdas', 1, 1, 0, '2020-01-13 05:46:34', '2020-01-13 05:46:34'),
(13, 2, 7, 'new tour product', 'new-tour-product', '567418001578914212.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 6, 11, 3, 'test desc', '<p>testing description</p>', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/02pj1Ew_HMU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', NULL, NULL, NULL, NULL, 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Hoshangabad, Madhya Pradesh, India', '22.7438944', '77.73592239999999', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', '1', 'Easy', '1', 'asdas', 1, 1, 0, '2020-01-13 05:46:52', '2020-01-13 05:46:52'),
(14, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '4607516251578985558.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:35:58', '2020-01-14 01:35:58'),
(15, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '1768813461578985697.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:38:18', '2020-01-14 01:38:18'),
(16, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '13358293931578985868.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:41:08', '2020-01-14 01:41:08'),
(17, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '17861877311578985933.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:42:13', '2020-01-14 01:42:13'),
(18, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '18167240421578985949.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:42:29', '2020-01-14 01:42:29'),
(19, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '415745881578986017.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:43:37', '2020-01-14 01:43:37'),
(20, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '12681027001578986143.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:45:43', '2020-01-14 01:45:43'),
(21, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '1490995051578986157.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:45:57', '2020-01-14 01:45:57'),
(22, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '12270246191578986180.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:46:20', '2020-01-14 01:46:20'),
(23, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '12392979771578986187.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:46:27', '2020-01-14 01:46:27'),
(24, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '13818457561578986220.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:47:00', '2020-01-14 01:47:00'),
(25, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '3562182691578986243.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:47:23', '2020-01-14 01:47:23'),
(26, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '1044771321578986266.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:47:46', '2020-01-14 01:47:46'),
(27, 2, 7, 'new testing holiday package', 'new-testing-holiday-package', '14037264031578986286.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL, 'Bhopal Railway Station, Bajariya, Navbahar Colony, Bhopal, Madhya Pradesh, India', '23.2676091', '77.4139675', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Indore, Madhya Pradesh, India', '22.7195687', '75.8577258', 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '1', '1', NULL, 'Challenging', '1', 'asdasdasd', NULL, 2, 0, '2020-01-14 01:48:06', '2020-01-14 01:48:06'),
(28, 2, 7, 'new holiday', 'new-holiday', '10354053111578986794.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 'Bhosari, Pimpri-Chinchwad, Maharashtra, India', '18.6320627', '73.84679779999999', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'N;', 'Yes', NULL, 'Yes', 'No', NULL, NULL, '1', '1', NULL, NULL, '1', 'asdasd', NULL, 2, 0, '2020-01-14 01:56:34', '2020-01-14 01:56:34'),
(29, 2, 7, 'new holiday', 'new-holiday', '10139278551578986895.jpg', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 'Bhosari, Pimpri-Chinchwad, Maharashtra, India', '18.6320627', '73.84679779999999', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'Bhopal, Madhya Pradesh, India', '23.2599333', '77.412615', 'N;', 'Yes', NULL, 'Yes', 'No', NULL, NULL, '1', '1', NULL, NULL, '1', 'asdasd', NULL, 2, 0, '2020-01-14 01:58:15', '2020-01-14 01:58:15'),
(30, 2, 7, 'test tour product', 'test-tour-product', NULL, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2020-01-14 02:00:01', '2020-01-14 02:00:01'),
(31, 2, 7, 'latest holiday', 'latest-holiday', NULL, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a:1:{i:0;s:1:\"2\";}', NULL, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2020-01-14 02:01:19', '2020-01-14 02:01:19'),
(32, 2, 7, 'latest holiday', 'latest-holiday', NULL, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a:1:{i:0;s:1:\"2\";}', NULL, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2020-01-14 02:02:28', '2020-01-14 02:02:28'),
(33, 2, 6, 'test tour product', 'test-tour-product', NULL, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2020-01-14 02:07:08', '2020-01-14 02:07:08'),
(34, 2, 6, 'asasas', 'asasas', NULL, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2020-01-14 02:14:09', '2020-01-14 02:14:09'),
(35, 2, 6, 'test tour product', 'test-tour-product', NULL, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2020-01-14 02:16:19', '2020-01-14 02:16:19'),
(36, 2, 7, 'test holiday', 'test-holiday', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, '2020-01-14 02:16:41', '2020-01-14 02:16:41'),
(37, 2, 6, 'SOFT PLAY HIRE', 'soft-play-hire', '536883751578990706.jpg', NULL, NULL, NULL, NULL, 54, '25', NULL, NULL, NULL, NULL, '<p>asdas</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'test', 2, 1, 0, '2020-01-14 03:01:46', '2020-01-14 03:01:46'),
(38, 2, 6, 'test tour product', 'test-tour-product', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, '2020-01-14 04:10:01', '2020-01-14 04:10:01'),
(39, 2, 7, 'SOFT PLAY HIRE', 'soft-play-hire', NULL, NULL, NULL, NULL, NULL, 54, '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 0, '2020-01-14 04:10:14', '2020-01-14 04:10:14'),
(40, 2, 7, 'SOFT PLAY HIRE', 'soft-play-hire', NULL, NULL, NULL, NULL, NULL, 54, '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'test', 2, 1, 0, '2020-01-14 04:11:45', '2020-01-14 04:11:45'),
(41, 2, 7, 'SOFT PLAY HIRE', 'soft-play-hire', NULL, NULL, NULL, NULL, NULL, 54, '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'test', 2, 1, 0, '2020-01-14 04:13:01', '2020-01-14 04:13:01'),
(42, 2, 7, 'SOFT PLAY HIRE', 'soft-play-hire', NULL, NULL, NULL, NULL, NULL, 54, '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'test', 2, 1, 0, '2020-01-14 04:13:29', '2020-01-14 04:13:29'),
(43, 2, 7, 'test rental p[roduct', 'test-rental-product', '11062258911578996075.png', NULL, 57, 60, 'hatchback,sedan', NULL, '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'test', 3, 1, 0, '2020-01-14 04:31:15', '2020-01-14 04:31:15'),
(44, 2, 7, 'test rental test', 'test-rental-test', NULL, NULL, 57, 61, 'hatchback,sedan', NULL, '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'test', 3, 1, 0, '2020-01-14 04:37:06', '2020-01-14 04:37:06');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parent` tinyint(1) NOT NULL DEFAULT 0,
  `type` int(11) NOT NULL COMMENT '''1 Tour'', ''2 Activity'', ''3 Tickets'', ''4 Transport'', ''5 Rental'', ''6 Services'', ''7 Inclusions'', ''8 Exclusions'', ''9 Holiday''',
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `parent`, `type`, `status`) VALUES
(5, 'Free Walking Tour', 'free-walking-tour', 0, 1, 1),
(6, 'Paid Walking Tour', 'paid-walking-tour', 0, 1, 1),
(7, 'Car Tour', 'car-tour', 0, 1, 1),
(8, 'Bike Tour', 'bike-tour', 0, 1, 1),
(9, 'Day Trip', 'day-trip', 0, 1, 1),
(10, 'Classes/Lessons/Workshop', 'classeslessonsworkshop', 0, 2, 1),
(11, 'Air Activities', 'air-activities', 0, 2, 1),
(12, 'Fun & Games', 'fun-games', 0, 2, 1),
(13, 'Outdoor Activities', 'outdoor-activities', 0, 2, 1),
(14, 'Water Activities', 'water-activities', 0, 2, 1),
(15, 'Guide', 'guide', 0, 7, 1),
(16, 'Food & Drinks', 'food-drinks', 0, 7, 1),
(17, 'Fee', 'fee', 0, 7, 1),
(18, 'Transportation', 'transportation', 0, 7, 1),
(19, 'Souvenir', 'souvenir', 0, 7, 1),
(25, 'In-person Storyteller', 'in-person-storyteller', 15, 7, 1),
(26, 'Audio Guide', 'audio-guide', 15, 7, 1),
(27, 'Written Guide', 'written-guide', 15, 7, 1),
(28, 'Tea/Coffee', 'teacoffee', 16, 7, 1),
(29, 'Breakfast', 'breakfast', 16, 7, 1),
(30, 'Lunch', 'lunch', 16, 7, 1),
(31, 'Dinner', 'dinner', 16, 7, 1),
(32, 'Snack', 'snack', 16, 7, 1),
(33, 'Ice-cream', 'ice-cream', 16, 7, 1),
(34, 'Juice', 'juice', 16, 7, 1),
(35, 'Soda', 'soda', 16, 7, 1),
(36, 'Chocolates', 'chocolates', 16, 7, 1),
(37, 'Candy', 'candy', 16, 7, 1),
(38, 'Sweets', 'sweets', 16, 7, 1),
(39, 'Bottled Water', 'bottled-water', 16, 7, 1),
(40, 'Alcoholic Beverage', 'alcoholic-beverage', 16, 7, 1),
(41, 'Non-alcoholic beverage', 'non-alcoholic-beverage', 16, 7, 1),
(42, 'All Taxes', 'all-taxes', 17, 7, 1),
(43, 'Parking Fee', 'parking-fee', 17, 7, 1),
(44, 'Gratuities', 'gratuities', 17, 7, 1),
(45, 'Air-conditioned Vehicle', 'air-conditioned-vehicle', 18, 7, 1),
(46, 'Private Car', 'private-car', 18, 7, 1),
(47, 'Bicycle', 'bicycle', 18, 7, 1),
(48, 'Public Transportation - Bus/Metro/Tram', 'public-transportation-busmetrotram', 18, 7, 1),
(49, 'Postcard', 'postcard', 19, 7, 1),
(50, 'Local Popular Food Snack', 'local-popular-food-snack', 19, 7, 1),
(51, 'Attraction Tickets', 'attraction-tickets', 0, 3, 1),
(52, 'Cards & Passes', 'cards-passes', 0, 3, 1),
(53, 'Events & Festival Tickets', 'events-festival-tickets', 0, 3, 1),
(54, 'Show & Concert Tickets', 'show-concert-tickets', 0, 3, 1),
(55, 'Sports Tickets', 'sports-tickets', 0, 3, 1),
(57, 'Land Transport', 'land-transport', 0, 4, 1),
(58, 'Water Transport', 'water-transport', 0, 4, 1),
(59, 'mini bus', 'mini-bus', 57, 5, 1),
(60, 'car', 'car', 57, 5, 1),
(61, 'bike', 'bike', 57, 5, 1),
(62, 'bicycle', 'bicycle', 57, 5, 1),
(63, 'e-scooter', 'e-scooter', 57, 5, 1),
(64, 'boat', 'boat', 58, 5, 1),
(65, 'water scooter', 'water-scooter', 58, 5, 1),
(66, 'Backpacking', 'backpacking', 0, 9, 1),
(67, 'Family', 'family', 0, 9, 1),
(68, 'Couple', 'couple', 0, 9, 1),
(69, 'Group', 'group', 0, 9, 1),
(70, 'Luxury', 'luxury', 0, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(127) NOT NULL,
  `product_id` bigint(127) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`) VALUES
(2, 1, '15771943795e02138bbb7a2.jpg'),
(4, 5, '15782975695e12e8e15f255.png'),
(5, 5, '15782975695e12e8e17353f.png'),
(6, 6, '15783082655e1312a96fa63.png'),
(7, 6, '15783082655e1312a9835d3.png'),
(8, 7, '15783921755e145a6ff0b06.png'),
(9, 7, '15783921765e145a7008d9d.png'),
(10, 9, '15789140535e1c5105d0901.jpg'),
(11, 9, '15789140535e1c5105e26b7.jpg'),
(12, 10, '15789141235e1c514b3e853.jpg'),
(13, 10, '15789141235e1c514b564e3.jpg'),
(14, 11, '15789141385e1c515ad8e67.jpg'),
(15, 11, '15789141385e1c515ae2b78.jpg'),
(16, 12, '15789141945e1c51926c2fa.jpg'),
(17, 12, '15789141945e1c519283b88.jpg'),
(18, 13, '15789142125e1c51a469e8b.jpg'),
(19, 13, '15789142125e1c51a47ebe4.jpg'),
(20, 14, '15789855585e1d685634ce2.jpg'),
(21, 14, '15789855585e1d68564b127.jpg'),
(22, 14, '15789855585e1d6856513e1.jpg'),
(23, 15, '15789856985e1d68e215dac.jpg'),
(24, 15, '15789856985e1d68e2201a7.jpg'),
(25, 15, '15789856985e1d68e22a2a3.jpg'),
(26, 16, '15789858685e1d698c72319.jpg'),
(27, 16, '15789858685e1d698c86843.jpg'),
(28, 16, '15789858685e1d698c8fb83.jpg'),
(29, 17, '15789859335e1d69cddc204.jpg'),
(30, 17, '15789859335e1d69cde617e.jpg'),
(31, 17, '15789859335e1d69cdf057a.jpg'),
(32, 18, '15789859495e1d69dd6a0bf.jpg'),
(33, 18, '15789859495e1d69dd70390.jpg'),
(34, 18, '15789859495e1d69dd7a4e4.jpg'),
(35, 19, '15789860175e1d6a2127dd3.jpg'),
(36, 19, '15789860175e1d6a2138463.jpg'),
(37, 19, '15789860175e1d6a2148719.jpg'),
(38, 20, '15789861435e1d6a9fbe004.jpg'),
(39, 20, '15789861435e1d6a9fc6013.jpg'),
(40, 20, '15789861435e1d6a9fd630d.jpg'),
(41, 21, '15789861575e1d6aadb2dcc.jpg'),
(42, 21, '15789861575e1d6aadb8055.jpg'),
(43, 21, '15789861575e1d6aadc84d8.jpg'),
(44, 22, '15789861805e1d6ac46d2e3.jpg'),
(45, 22, '15789861805e1d6ac47d6ac.jpg'),
(46, 22, '15789861805e1d6ac489a4f.jpg'),
(47, 23, '15789861875e1d6acbdb054.jpg'),
(48, 23, '15789861875e1d6acbea21e.jpg'),
(49, 23, '15789861885e1d6acc0248e.jpg'),
(50, 24, '15789862205e1d6aecb9df9.jpg'),
(51, 24, '15789862205e1d6aecd6303.jpg'),
(52, 24, '15789862205e1d6aece7aaa.jpg'),
(53, 25, '15789862435e1d6b0377257.jpg'),
(54, 25, '15789862435e1d6b03864a9.jpg'),
(55, 25, '15789862435e1d6b038c489.jpg'),
(56, 26, '15789862665e1d6b1a9db37.jpg'),
(57, 26, '15789862665e1d6b1aae0cd.jpg'),
(58, 26, '15789862665e1d6b1abb4dd.jpg'),
(59, 27, '15789862865e1d6b2e23148.jpg'),
(60, 27, '15789862865e1d6b2e2f3b8.jpg'),
(61, 27, '15789862865e1d6b2e3b5dc.jpg'),
(62, 28, '15789867945e1d6d2a3d9ae.jpg'),
(63, 28, '15789867945e1d6d2a438f3.jpg'),
(64, 28, '15789867945e1d6d2a4fdfa.jpg'),
(65, 29, '15789868955e1d6d8f46fd8.jpg'),
(66, 29, '15789868955e1d6d8f5345f.jpg'),
(67, 29, '15789868955e1d6d8f647dd.jpg'),
(68, 37, '15789907065e1d7c72adc51.jpg'),
(69, 37, '15789907065e1d7c72bc02e.png'),
(70, 43, '15789960755e1d916b4fc8c.jpg'),
(71, 43, '15789960755e1d916b57c57.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_inclusions_exclusions`
--

CREATE TABLE `product_inclusions_exclusions` (
  `id` bigint(127) NOT NULL,
  `product_id` bigint(127) DEFAULT NULL,
  `inclusion_package` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inclusion_category` int(11) DEFAULT NULL,
  `inclusion_sub_category` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inclusion_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `exclusion_package` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exclusion_category` int(11) DEFAULT NULL,
  `exclusion_sub_category` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exclusion_description` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_inclusions_exclusions`
--

INSERT INTO `product_inclusions_exclusions` (`id`, `product_id`, `inclusion_package`, `inclusion_category`, `inclusion_sub_category`, `inclusion_description`, `exclusion_package`, `exclusion_category`, `exclusion_sub_category`, `exclusion_description`) VALUES
(1, 13, 'For Basic Tour', 16, 'a:3:{i:0;s:2:\"29\";i:1;s:2:\"32\";i:2;s:2:\"33\";}', 'wdas', 'For Budget Tour', 17, 'a:2:{i:0;s:2:\"43\";i:1;s:2:\"44\";}', 'asdasdas'),
(2, 27, 'For Budget Tour', 16, 'a:3:{i:0;s:2:\"29\";i:1;s:2:\"31\";i:2;s:2:\"33\";}', 'wdas', 'For Premium Tour', 19, 'a:2:{i:0;s:2:\"49\";i:1;s:2:\"50\";}', 'asdasd'),
(3, 29, 'For Basic Tour', 15, 'a:1:{i:0;s:2:\"26\";}', 'asdasd', 'For Budget Tour', 18, 'a:1:{i:0;s:2:\"45\";}', 'asdasd'),
(4, 31, 'For Basic Tour', 15, 'a:1:{i:0;s:2:\"25\";}', 'wdas', 'For Budget Tour', 17, 'a:1:{i:0;s:2:\"43\";}', NULL),
(5, 32, 'For Basic Tour', 15, 'a:1:{i:0;s:2:\"25\";}', 'wdas', 'For Budget Tour', 17, 'a:1:{i:0;s:2:\"43\";}', NULL),
(6, 37, 'For Budget Tour', 19, 'a:2:{i:0;s:2:\"49\";i:1;s:2:\"50\";}', 'wdas', 'For Basic Tour', 18, 'a:2:{i:0;s:2:\"46\";i:1;s:2:\"48\";}', 'asd asdasd'),
(7, 40, 'For Basic Tour', 18, 'a:2:{i:0;s:2:\"45\";i:1;s:2:\"46\";}', NULL, NULL, 18, 'a:1:{i:0;s:2:\"47\";}', 'asdasdas'),
(8, 41, 'For Basic Tour', 18, 'a:2:{i:0;s:2:\"45\";i:1;s:2:\"46\";}', NULL, NULL, 18, 'a:1:{i:0;s:2:\"47\";}', 'asdasdas'),
(9, 42, 'For Basic Tour', 18, 'a:2:{i:0;s:2:\"45\";i:1;s:2:\"46\";}', NULL, NULL, 18, 'a:1:{i:0;s:2:\"47\";}', 'asdasdas'),
(10, 43, NULL, 16, 'a:1:{i:0;s:2:\"30\";}', NULL, NULL, NULL, 'N;', NULL),
(11, 44, NULL, 15, 'a:1:{i:0;s:2:\"26\";}', NULL, NULL, 18, 'a:1:{i:0;s:2:\"48\";}', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_itinerary`
--

CREATE TABLE `product_itinerary` (
  `id` bigint(127) NOT NULL,
  `product_id` bigint(127) DEFAULT NULL,
  `day_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itinerary_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itinerary_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `place_type` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `describe_experience` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `spent_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meal_type` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `meal_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `accomodation_details` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `included_price_tour` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_itinerary`
--

INSERT INTO `product_itinerary` (`id`, `product_id`, `day_title`, `itinerary_title`, `itinerary_img`, `place_type`, `describe_experience`, `spent_time`, `meal_type`, `meal_description`, `accomodation_details`, `included_price_tour`) VALUES
(1, 11, NULL, 'test 1', '7880096821578914138.png', 'Monument,Museum,test1', 'asdas asdasd1', 'a:2:{i:0;s:24:\"Pass by without stopping\";i:1;s:7:\"Minutes\";}', NULL, NULL, NULL, 'Yes'),
(2, 12, NULL, 'test 1', '5449162921578914194.png', 'Monument,Museum,test1', 'asdas asdasd1', 'a:2:{i:0;s:24:\"Pass by without stopping\";i:1;s:7:\"Minutes\";}', NULL, NULL, NULL, 'Yes'),
(3, 13, NULL, 'test 1', '14801552681578919967.jpg', 'Monument,Museum,test1', 'asdas asdasd1', 'a:2:{i:0;s:24:\"Pass by without stopping\";i:1;s:7:\"Minutes\";}', NULL, NULL, NULL, 'Yes'),
(4, 13, NULL, 'test 2', '21142138951578919967.jpg', 'Monument,Museum,test2', 'asdas asdasd2', 'a:1:{i:0;s:24:\"Pass by without stopping\";}', NULL, NULL, NULL, 'N/A (Admission is free)'),
(5, 13, NULL, 'test 3', '11216122681578914212.jpg', 'Monument,Museum,test3', 'asdas asdasd3', 'a:1:{i:1;s:7:\"Minutes\";}', NULL, NULL, NULL, 'No'),
(7, 26, 'day one', 'test 1', '10851288461578986266.png', 'Monument,Museum,test', 'asdas asdasd', 'a:2:{i:0;s:24:\"Pass by without stopping\";i:1;s:7:\"Minutes\";}', 'Breakfast,Lunch,Dinner', 'asdfasda', 'asdasd', 'Yes'),
(8, 27, 'day one', 'test 1', '2329113171578986286.png', 'Monument,Museum,test', 'asdas asdasd', 'a:2:{i:0;s:24:\"Pass by without stopping\";i:1;s:7:\"Minutes\";}', 'Breakfast,Lunch,Dinner', 'asdfasda', 'asdasd', 'Yes'),
(9, 27, 'day two', 'test 3', '1754689281578986286.jpg', 'Monument,Museum,test', 'asdas asdasd', 'a:2:{i:0;s:24:\"Pass by without stopping\";i:1;s:7:\"Minutes\";}', 'Breakfast,Lunch,Dinner', 'asdasd', 'asdasdasd', 'No'),
(11, 29, 'day one', 'test 3', '16302010831578986895.jpg', 'Monument,Museum,test', 'asdas asdasd', 'a:2:{i:0;s:24:\"Pass by without stopping\";i:1;s:7:\"Minutes\";}', 'Breakfast,Lunch,Dinner', 'asdasd', 'asdas', NULL),
(12, 29, 'day one', 'test 3', '7157573861578986895.jpg', 'Monument,Museum,test', 'asdas asdasd', 'a:2:{i:0;s:24:\"Pass by without stopping\";i:1;s:7:\"Minutes\";}', 'Breakfast,Lunch,Dinner', 'asdasd', 'asdas', 'Yes'),
(13, 31, 'asdas', 'asdasd', '17223809141578987079.jpg', 'Monument,Museum,test', 'asdas asdasd', 'N;', 'Breakfast,Lunch,Dinner', 'asdas', 'asd', NULL),
(14, 32, 'asdas', 'asdasd', '12376080701578987148.jpg', 'Monument,Museum,test', 'asdas asdasd', 'N;', 'Breakfast,Lunch,Dinner', 'asdas', 'asd', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_pricing`
--

CREATE TABLE `product_pricing` (
  `id` bigint(127) NOT NULL,
  `product_id` bigint(127) DEFAULT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_age_adult` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_age_adult` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_age_child` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_age_child` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_age_youth` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_age_youth` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_no_allow_single_booking` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_no_allow_travelers` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pricing_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pricing_options` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_attributes` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_for_tour_free` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_people_book_for_free` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `limit_cross_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_for_tour_paid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_person_required` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pricing_tour_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adult_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `child_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youth_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_age_adult_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_age_adult_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_age_child_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_age_child_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_age_youth_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_age_youth_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_pricing` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_of_people_in_group` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_pricing_min` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_pricing_max` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_pricing_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiered_pricing` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiered_pricing_from` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiered_pricing_to` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiered_pricing_per_person_price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposit` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_rate` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pricing_days` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_pricing`
--

INSERT INTO `product_pricing` (`id`, `product_id`, `currency`, `min_age_adult`, `max_age_adult`, `min_age_child`, `max_age_child`, `min_age_youth`, `max_age_youth`, `max_no_allow_single_booking`, `max_no_allow_travelers`, `pricing_type`, `pricing_options`, `add_attributes`, `price_for_tour_free`, `no_people_book_for_free`, `limit_cross_price`, `price_for_tour_paid`, `min_person_required`, `pricing_tour_type`, `adult_price`, `child_price`, `youth_price`, `min_age_adult_price`, `max_age_adult_price`, `min_age_child_price`, `max_age_child_price`, `min_age_youth_price`, `max_age_youth_price`, `group_pricing`, `no_of_people_in_group`, `group_pricing_min`, `group_pricing_max`, `group_pricing_price`, `tiered_pricing`, `tiered_pricing_from`, `tiered_pricing_to`, `tiered_pricing_per_person_price`, `deposit`, `tax_name`, `tax_rate`, `pricing_days`) VALUES
(1, 13, 'INR', '12', '14', '13', '15', '17', '20', '44', '55', 'Per Person', 'More than one', '12', 'Free', '3', 'dfd', 'Paid', '121', 'a:3:{i:0;s:5:\"Basic\";i:1;s:6:\"Budget\";i:2;s:7:\"Premium\";}', '232', '3', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '455', '7', '87', NULL, 'fdf', 'dfdfd', '43', NULL, '120', '34', 'a:5:{i:0;s:1:\"1\";i:1;s:1:\"3\";i:2;s:1:\"4\";i:3;s:1:\"5\";i:4;s:1:\"6\";}'),
(4, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 35, 'EUR', '12', NULL, '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 36, 'INR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;'),
(12, 38, 'INR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;'),
(13, 39, 'INR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;'),
(14, 42, 'EUR', '12', '14', '13', '15', '17', '20', '44', '55', 'Per Person', 'One', '12', NULL, NULL, NULL, NULL, '121', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;'),
(15, 43, 'EUR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Per Person', 'More than one', 'dasdas', NULL, NULL, NULL, NULL, '121', NULL, '232', '3', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'N;'),
(16, 43, 'EUR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Per Person', 'More than one', 'dasdas', NULL, NULL, NULL, NULL, '121', NULL, '232', '3', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'N;'),
(17, 43, 'EUR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Per Person', 'More than one', 'dasdas', NULL, NULL, NULL, NULL, '121', NULL, '232', '3', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'N;'),
(18, 43, 'EUR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Per Person', 'More than one', 'dasdas', NULL, NULL, NULL, NULL, '121', NULL, '232', '3', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'N;'),
(19, 43, 'EUR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Per Person', 'More than one', 'dasdas', NULL, NULL, NULL, NULL, '121', NULL, '232', '3', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, 'N;'),
(20, 44, 'EUR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Per Person', 'More than one', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `product_time_duration_slot`
--

CREATE TABLE `product_time_duration_slot` (
  `id` bigint(127) NOT NULL,
  `product_id` bigint(127) DEFAULT NULL,
  `tour_duration` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fixed_duration` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flexible_duration_minus` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flexible_duration_hour` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_date_time_slot` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holiday_start_date` date DEFAULT NULL,
  `holiday_end_date` date DEFAULT NULL,
  `holiday_start_tour_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holiday_end_tour_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_people` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_people` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_booking_date` date DEFAULT NULL,
  `time_slot_days` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `specific_booking_date` date DEFAULT NULL,
  `specific_block_booking_date` date DEFAULT NULL,
  `specific_entry_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hour_before_tour_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `advance_notice_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_time_duration_slot`
--

INSERT INTO `product_time_duration_slot` (`id`, `product_id`, `tour_duration`, `fixed_duration`, `flexible_duration_minus`, `flexible_duration_hour`, `additional_date_time_slot`, `holiday_start_date`, `holiday_end_date`, `holiday_start_tour_time`, `holiday_end_tour_time`, `min_people`, `max_people`, `start_booking_date`, `time_slot_days`, `specific_booking_date`, `specific_block_booking_date`, `specific_entry_time`, `hour_before_tour_time`, `advance_notice_time`) VALUES
(1, 13, 'Fixed', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-30', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"3\";i:2;s:1:\"4\";}', '2020-01-30', '2020-01-31', NULL, '12', '33'),
(4, 32, NULL, NULL, NULL, NULL, '12', '2020-01-30', '2020-02-28', NULL, NULL, '12', '50', NULL, NULL, NULL, NULL, NULL, '12', NULL),
(5, 33, NULL, NULL, NULL, NULL, '12', '2020-01-30', '2020-01-30', '1:00 AM', '1:00 AM', '12', '50', NULL, NULL, NULL, NULL, NULL, '12', NULL),
(6, 34, NULL, NULL, NULL, NULL, '12', '2020-02-02', '2020-02-12', '1:00 AM', '12:00 AM', '12', '50', NULL, NULL, NULL, NULL, NULL, '12', NULL),
(7, 36, 'Fixed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL),
(8, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-30', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"4\";}', '2020-01-31', '2020-02-02', '4:00 AM', '12', '33'),
(9, 38, 'Fixed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL),
(10, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-30', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"4\";}', '2020-01-31', '2020-02-02', '8:00 AM', '12', '33'),
(11, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-30', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"4\";}', '2020-01-31', '2020-02-02', '8:00 AM', '12', '33'),
(12, 42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-30', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"4\";}', '2020-01-31', '2020-02-02', '8:00 AM', '12', '33'),
(13, 43, 'Flexible', NULL, '21', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-30', 'a:2:{i:0;s:1:\"2\";i:1;s:1:\"4\";}', '2020-01-31', '2020-02-02', NULL, '12', NULL),
(14, 43, 'Flexible', NULL, '21', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N;', NULL, NULL, NULL, NULL, NULL),
(15, 44, 'Fixed', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-03', 'a:3:{i:0;s:1:\"3\";i:1;s:1:\"6\";i:2;s:1:\"7\";}', '2020-02-04', '2020-02-06', NULL, '12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_tour_time`
--

CREATE TABLE `product_tour_time` (
  `id` bigint(127) NOT NULL,
  `product_id` bigint(127) DEFAULT NULL,
  `start_tour_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_tour_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_tour_time`
--

INSERT INTO `product_tour_time` (`id`, `product_id`, `start_tour_time`, `end_tour_time`) VALUES
(13, 13, '5:42 PM', '6:42 PM'),
(14, 13, '6:14 PM', '6:14 PM'),
(15, 13, '6:23 PM', '6:23 PM'),
(17, 42, '1:00 AM', '12:00 AM');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(127) NOT NULL,
  `product_id` bigint(127) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_profile_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `product_id`, `is_admin`, `customer_name`, `customer_profile_img`, `rating`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'shirram chaurasiya', '473419091578999248.png', '5', 'sasd', 1, '2020-01-14 05:24:08', '2020-01-14 05:24:08'),
(3, 1, 0, 'shirram chaurasiya', '473419091578999248.png', '5', 'sasd', 1, '2020-01-14 05:24:08', '2020-01-14 05:24:08');

-- --------------------------------------------------------

--
-- Table structure for table `storytellers`
--

CREATE TABLE `storytellers` (
  `id` bigint(127) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `tour_type_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `profile_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `recruited_form` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `poj` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptf` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ptp` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allowance` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `local_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `education` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blood_group` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_proof_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_proof_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `police_clearance_certificate` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_holder_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `storytellers`
--

INSERT INTO `storytellers` (`id`, `destination_id`, `tour_type_id`, `name`, `profile_img`, `dob`, `recruited_form`, `doj`, `poj`, `ptf`, `ptp`, `allowance`, `email`, `phone`, `emergency_number`, `local_address`, `education`, `blood_group`, `id_proof_number`, `id_proof_type`, `police_clearance_certificate`, `account_no`, `account_holder_name`, `ifsc_code`, `bank_name`, `branch`, `status`, `created_at`, `updated_at`) VALUES
(2, 7, 3, 'Tapas Vishwas', '1315145261573819712.png', '2019-11-30', 'asdas', '2019-11-24', '1 yr', 'asd', 'asd', 'asd', 'tpsvishwas78@gmail.com', '9999999999', '09999999999', 'asd', 'asd', 'A', '221', 'adasd', 'obtained', '12121212', 'tapas', '1212AA', 'pnb', 'hbj', 1, '2019-11-15 06:38:32', '2019-11-15 06:38:32'),
(3, 7, 2, 'tanish', NULL, '2019-11-30', 'adas', '2019-11-30', '1 yr', 'asdas', 'asdas', 'asdasd', 'tanish@gmail.com', '09999999999', '09999999999', 'Bhopal, Madhya Pradesh, India', 'asdasd', 'A', '1212asdas', 'sdasd', 'obtained', '1212121212', 'dasd', '1212AA', 'wdsds', 'hbj', 1, '2019-11-19 06:50:07', '2019-11-19 06:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Nature', 1, '2020-01-10 01:33:07', '2020-01-10 01:33:07'),
(3, 'Beach', 1, '2020-01-10 01:33:14', '2020-01-10 01:33:14'),
(4, 'Mountains', 1, '2020-01-10 01:33:22', '2020-01-10 01:33:22'),
(5, 'Adventure', 1, '2020-01-10 01:33:29', '2020-01-10 01:33:29'),
(6, 'History', 1, '2020-01-10 01:33:35', '2020-01-10 01:33:35'),
(7, 'Culture', 1, '2020-01-10 01:33:41', '2020-01-10 01:33:41'),
(8, 'Romantic', 1, '2020-01-10 01:33:50', '2020-01-10 01:33:50'),
(9, 'Religious', 1, '2020-01-10 01:33:57', '2020-01-10 01:33:57'),
(10, 'Spiritual', 1, '2020-01-10 01:34:03', '2020-01-10 01:34:03'),
(11, 'Village', 1, '2020-01-10 01:34:12', '2020-01-10 01:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `timeslots`
--

CREATE TABLE `timeslots` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tourimages`
--

CREATE TABLE `tourimages` (
  `id` bigint(127) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tour_id` bigint(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tourimages`
--

INSERT INTO `tourimages` (`id`, `image`, `tour_id`) VALUES
(1, '15738001015dce48a53007b.jpg', 4),
(3, '15738001015dce48a542518.jpg', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE `tours` (
  `id` bigint(127) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `price` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`id`, `title`, `slug`, `type`, `destination_id`, `price`, `featured_img`, `location`, `short_description`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Play football whenever you want, socialize and have fun.', 'play-football-whenever-you-want-socialize-and-have-fun', 4, 8, '2', '16832093971573799903.jpg', 'asdasd', 'asdas', '<p>asdasd</p>', 1, '2019-11-15 01:08:23', '2019-11-15 01:08:23'),
(3, 'SOFT PLAY HIRE', 'soft-play-hire', 5, 8, '3', '12122194111573800004.jpg', 'asd', 'asdasd', '<p>asdasd</p>', 1, '2019-11-15 01:10:04', '2019-11-15 01:10:04'),
(4, 'FOOTBALL PARTY', 'football-party', 3, 7, '11', '2826739731573800101.jpg', 'asdas', 'asdasdasd', '<p>asdasd</p>', 1, '2019-11-15 01:11:41', '2019-11-15 01:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `tourtypes`
--

CREATE TABLE `tourtypes` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tourtypes`
--

INSERT INTO `tourtypes` (`id`, `name`, `slug`, `featured_img`, `banner_img`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Walking Tour', 'walking-tour', NULL, NULL, NULL, 1, '2019-11-14 04:48:13', '2019-11-14 04:48:13'),
(3, 'Car Tour', 'car-tour', NULL, NULL, NULL, 1, '2019-11-14 10:18:25', '2019-11-14 04:48:25'),
(4, 'Audio Tour', 'audio-tour', NULL, NULL, NULL, 1, '2019-11-14 04:48:37', '2019-11-14 04:48:37'),
(5, 'Multiday Trip', 'multiday-trip', NULL, NULL, NULL, 1, '2019-11-14 10:18:51', '2019-11-14 04:48:51');

-- --------------------------------------------------------

--
-- Table structure for table `tour_languages`
--

CREATE TABLE `tour_languages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tour_languages`
--

INSERT INTO `tour_languages` (`id`, `name`, `slug`, `icon`, `status`) VALUES
(1, 'English', 'english', '8562067251577176124.png', 1),
(2, 'Hindi', 'hindi', '18065844561577176137.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `channel_id` bigint(127) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `age` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(100) DEFAULT NULL COMMENT '1 male, 2 female',
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_role` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 User,1 Admin, 3 StoryTeller',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `channel_id`, `name`, `email`, `phone`, `dob`, `age`, `gender`, `address`, `profile_img`, `email_verified_at`, `password`, `remember_token`, `user_role`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'admin', 'admin@yotour.in', NULL, NULL, NULL, NULL, NULL, '1573626511.png', NULL, '$2y$10$gwB.y/iUNKJPZUWEmC/IheBVEehjPC7QSrHrBsmtOUpQQG6sStmwW', NULL, 1, 1, '2019-11-12 05:35:40', '2019-11-22 02:20:50'),
(2, 2, 'test', 'test@gmail.com', '8982127797', '2019-11-28', '25', 1, 'jk hospital road', '10131344661573803913.jpg', NULL, '$2y$10$zlspWoYymdK.naP9.C4A6.zbBIOW9g.OzkK1F2zrglwgsdNJtGyh.', NULL, 0, 1, '2019-11-15 02:15:13', '2019-11-15 06:59:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `combos`
--
ALTER TABLE `combos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `combo_products`
--
ALTER TABLE `combo_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `paytypes`
--
ALTER TABLE `paytypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_inclusions_exclusions`
--
ALTER TABLE `product_inclusions_exclusions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_itinerary`
--
ALTER TABLE `product_itinerary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_pricing`
--
ALTER TABLE `product_pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_time_duration_slot`
--
ALTER TABLE `product_time_duration_slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tour_time`
--
ALTER TABLE `product_tour_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `storytellers`
--
ALTER TABLE `storytellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeslots`
--
ALTER TABLE `timeslots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourimages`
--
ALTER TABLE `tourimages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourtypes`
--
ALTER TABLE `tourtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_languages`
--
ALTER TABLE `tour_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `channels`
--
ALTER TABLE `channels`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `combos`
--
ALTER TABLE `combos`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `combo_products`
--
ALTER TABLE `combo_products`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `paytypes`
--
ALTER TABLE `paytypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `product_inclusions_exclusions`
--
ALTER TABLE `product_inclusions_exclusions`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `product_itinerary`
--
ALTER TABLE `product_itinerary`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_pricing`
--
ALTER TABLE `product_pricing`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_time_duration_slot`
--
ALTER TABLE `product_time_duration_slot`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `product_tour_time`
--
ALTER TABLE `product_tour_time`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `storytellers`
--
ALTER TABLE `storytellers`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `timeslots`
--
ALTER TABLE `timeslots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tourimages`
--
ALTER TABLE `tourimages`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tours`
--
ALTER TABLE `tours`
  MODIFY `id` bigint(127) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tourtypes`
--
ALTER TABLE `tourtypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tour_languages`
--
ALTER TABLE `tour_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
