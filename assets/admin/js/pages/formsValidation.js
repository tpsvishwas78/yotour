/*
 *  Document   : formsValidation.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Validation page
 */

var FormsValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    username: {
                        required: true,
                        minlength: 3
                    },
                    name: {
                        required: true,
                        minlength: 3
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    old_password: {
                        required: true,
                        minlength: 6
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: '#password'
                    },
                    bio: {
                        required: true,
                        minlength: 5
                    },
                    skill: {
                        required: true
                    },
                    website: {
                        required: false,
                        url: true
                    },
                    digits: {
                        required: true,
                        digits: true
                    },
                    number: {
                        required: true,
                        number: true
                    },
                    range: {
                        required: true,
                        range: [1, 1000]
                    },
                    terms: {
                        required: true
                    }
                },
                messages: {
                    username: {
                        required: 'Please enter a username',
                        minlength: 'Your username must consist of at least 3 characters'
                    },
                    name: {
                        required: 'Please enter a name',
                        minlength: 'Your name must consist of at least 3 characters'
                    },
                    email: 'Please enter a valid email address',
                    old_password: {
                        required: 'Please provide old password',
                        minlength: 'Your password must be at least 6 characters long'
                    },
                    password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 6 characters long'
                    },
                    confirm_password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 6 characters long',
                        equalTo: 'Please enter the same password as above'
                    },
                    bio: 'Don\'t be shy, share something with us :-)',
                    skill: 'Please select a skill!',
                    website: 'Please enter your website!',
                    digits: 'Please enter only digits!',
                    number: 'Please enter a number!',
                    range: 'Please enter a number between 1 and 1000!',
                    terms: 'You must agree to the service terms!'
                }
            });

            $('#form-validation-storyteller').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    destination: {
                        required: true,
                    },
                    tourtype: {
                        required: true,
                    },
                    name: {
                        required: true,
                        minlength: 3
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        minlength: 10,
                        number: true
                    },
                    emergency_number: {
                        required: true,
                        minlength: 10,
                        number: true
                    },
                    dob: {
                        required: true,
                    },
                    recruited_form: {
                        required: true
                    },
                    doj: {
                        required: true,
                    },
                    poj: {
                        required: true,
                    },
                    ptf: {
                        required: true,
                    },
                    ptp: {
                        required: true,
                    },
                    allowance: {
                        required: true
                    },
                    local_address: {
                        required: true
                    },
                    education: {
                        required: true
                    },
                    blood_group: {
                        required: true
                    },
                    id_proof_number: {
                        required: true
                    },
                    id_proof_type: {
                        required: true
                    },
                    police_clearance_certificate: {
                        required: true
                    },
                    account_no: {
                        required: true
                    },
                    account_holder_name: {
                        required: true
                    },
                    ifsc_code: {
                        required: true
                    },
                    bank_name: {
                        required: true
                    },
                    branch: {
                        required: true
                    }
                },
                messages: {
                    destination: 'Please select a destination',
                    tourtype:'Please select a tourtype',
                    name: {
                        required: 'Please enter a username',
                        minlength: 'Name must consist of at least 3 characters'
                    },
                    email: 'Please enter a valid email address',
                    phone: {
                        required: 'Please enter a phone number',
                        minlength: 'Phone number must consist of at least 10 digit',
                        number: 'Please enter number!'
                    },
                    emergency_number: {
                        required: 'Please enter a emergency number',
                        minlength: 'Emergency number must consist of at least 10 digit',
                        number: 'Please enter number!'
                    },
                    dob: 'Please enter DOB',
                    recruited_form: 'Please enter recruited form',
                    doj: 'Please enter doj',
                    poj: 'Please enter poj',
                    ptf: 'Please enter ptf',
                    ptp: 'Please enter ptp',
                    allowance: 'Please enter allowance',
                    local_address: 'Please enter local address',
                    education: 'Please enter education',
                    blood_group: 'Please enter blood group',
                    id_proof_number: 'Please enter id proof number',
                    id_proof_type: 'Please enter id proof type',
                    police_clearance_certificate: 'Please enter police clearance certificate',
                    account_no: 'Please enter account no',
                    account_holder_name: 'Please enter account holder name',
                    ifsc_code: 'Please enter ifsc code',
                    bank_name: 'Please enter bank name',
                    branch: 'Please enter branch name',
                    
                }
            });

            $('#form-validation-booking').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    channel: {
                        required: true,
                    },
                    tourtype: {
                        required: true,
                    },
                    reschedule: {
                        required: true,
                    },
                    city: {
                        required: true,
                    },
                    tour_name: {
                        required: true,
                    },
                    number_of_people: {
                        required: true,
                        number: true
                    },
                    booking_amount: {
                        required: true,
                        number: true
                    },
                    booking_amt_paytype: {
                        required: true,
                    },
                    date: {
                        required: true,
                    },
                    time: {
                        required: true
                    },
                    customer: {
                        required: true,
                    },
                    booking_reference: {
                        required: true,
                    },
                    discount: {
                        number: true
                    },
                    
                },
                messages: {
                    tourtype:'Please select a tourtype',
                    reschedule:'Please enter reschedule message',
                    number_of_people: {
                        required: 'Please enter number of people',
                        number: 'Please enter number!'
                    },
                    booking_amount: {
                        required: 'Please enter booking amount',
                        number: 'Please enter number!'
                    },
                    discount: {
                        number: 'Please enter number!'
                    },
                    channel: 'Please select a channel',
                    city: 'Please select a city',
                    tour_name: 'Please select tour name',
                    date: 'Please enter booking date',
                    time: 'Please enter booking time',
                    booking_amt_paytype: 'Please select booking amount payment type',
                    customer: 'Please enter allowance',
                    booking_reference: 'Please enter booking reference',
                    
                    
                }
            });

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();