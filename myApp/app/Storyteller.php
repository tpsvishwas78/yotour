<?php

namespace App;

//use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Storyteller extends Authenticatable {

//    use HasApiTokens,
//        Notifiable;

    use HasApiTokens,Notifiable;

    protected $fillable = [
         'email', 'phone',
    ];

}
