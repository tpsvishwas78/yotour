<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class StorytellerLoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user()->user_role);
        if(Auth::guard('storyteller')->check() and Auth::guard('storyteller')->user()->status == 1)
        {
            return true;
        }elseif(Auth::guard('admin')->check()){
            return false;
        }
        return $next($request);
    }
}
