<?php

namespace App\Http\Controllers\STORYTELLER;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Storyteller;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Storyteller as Storytellers;
use Illuminate\Support\Str;

class UserController extends Controller {

    public $successStatus = 200;

    /**
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function login(Request $request)
    {


        $storyteller = Storytellers::where(['email' => request('email'),'phone' => request('password')])->first();
        if ($storyteller) {
            $success['token'] =  $var = Str::random(32);
            $user = $storyteller;
            $storyteller->accessToken = $success['token'];
            $storyteller->save();
//            $success['token'] = $user->createToken('MyApp')->accessToken;
            
            return response()->json([
                        'status' => 'success',
                        'message' => 'Login Successfully',
                        'data' => $user,
                        'access_token' => $success['token'],
                            ],$this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], $this->successStatus);
        }
    }

    /**
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|email',
                    'password' => 'required',
                    'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], $this->successStatus);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;
        return response()->json(['success' => $success], $this->successStatus);
    }

    /**
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

}
