<?php

namespace App\Http\Controllers\STORYTELLER;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Booking;
use App\Models\Storyteller;

class BookingController extends Controller {

    public $successStatus = 200;

    /**
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function Allbooking(Request $request)
    {
        
        $booking = Storyteller::with('booking.channel','booking.tourtype','booking.destination','booking.tour','booking.paytype','booking.storyteller')->where(['accessToken' => $request->accessToken, 'status' => 1])->first();
        if ($booking) {
            return response()->json([
                        'status' => 'success',
                        'message' => '',
                        'data' => $booking->booking,
                            ], $this->successStatus);
        } else {
            return response()->json(['error' => $validator->errors()], $this->successStatus);
        }
    }
}
