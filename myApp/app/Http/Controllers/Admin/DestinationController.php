<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Destination;
use App\Models\Country;
use App\Models\User;
Use Auth;

class DestinationController extends Controller
{
    public function index()
    {
        $results=Destination::get();
        
        return view('admin.all_destination')->with('results',$results);
    }
    public function add_destination($id=0)
    {
        if($id>0){
            $data=Destination::where('id',$id)->first();
        }else{
            $data=array();
        }
        $countries=Country::where('status',1)->get();
        
        return view('admin.add_destination')->with('edit',$data)->with('countries',$countries);
    }
    public function submitdestination(Request $req)
    {
        $req->validate([
            'name' => 'required|unique:destinations,name,'.$req->id.',id',
            'country' => 'required',
        ]);
        if($req->id){
            $destination = Destination::find($req->id);
        }
        else{
            $destination = new Destination;
        }
        if($req->featured_img)
            {
                @unlink("upload\images\/".$destination->featured_img);
                $featured_img = rand().time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $featured_img);
                $destination->featured_img = $featured_img;
            }
            if($req->banner_img)
            {
                @unlink("upload\images\/".$destination->banner_img);
                $banner_img = time().'.'.$req->banner_img->getClientOriginalExtension();
                $req->banner_img->move('upload/images', $banner_img);
                $destination->banner_img = $banner_img;
            }
            $slug = str_slug($req->name, "-"); 

            $destination->name = $req->name;
            $destination->slug = $slug;
            $destination->country_id = $req->country;
            $destination->is_top = (isset($req->is_top) and $req->is_top > 0) ? 1 : 0;
            $destination->status = (isset($req->status) and $req->status > 0) ? 1 : 0;
            $destination->description = $req->description;

            if($destination->save())
            {
                return redirect('admin/all-destination')->with('success', 'Destination '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-destination')->with('warning', 'failed!');
            }
    }

    public function status_destination($id=0)
    {
        $data=Destination::where('id',$id)->first();
        
        if($data->status==1){
            Destination::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Destination::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Destination status changed successfully.');
    }
    public function delete_destination($id=0)
    {
        $data=Destination::where('id',$id)->first();
        @unlink("upload\images\/".$data->featured_img);
        @unlink("upload\images\/".$data->banner_img);
        Destination::where('id',$id)->delete();
        return back()->with('success', 'Destination Deleted Successfully.');
        
    }

    //country
    public function all_country()
    {
        $results=Country::get();
        return view('admin.all_country')->with('results',$results);
    }
    public function add_country($id=0)
    {
        if($id>0){
            $data=Country::where('id',$id)->first();
        }else{
            $data=array();
        }
        
        return view('admin.add_country')->with('edit',$data);
    }
    public function submitcountry(Request $req)
    {
        $req->validate([
            'name' => 'required|unique:countries,name,'.$req->id.',id',
        ]);
        if($req->id){
            $country = Country::find($req->id);
        }
        else{
            $country = new Country;
        }
       
            $slug = str_slug($req->name, "-"); 

            $country->name = $req->name;
            $country->slug = $slug;
            $country->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($country->save())
            {
                return redirect('admin/all-country')->with('success', 'country '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-country')->with('warning', 'failed!');
            }
    }

    public function status_country($id=0)
    {
        $data=Country::where('id',$id)->first();
        
        if($data->status==1){
            Country::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Country::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'country status changed successfully.');
    }
    public function delete_country($id=0)
    {
        $data=Country::where('id',$id)->first();
        Country::where('id',$id)->delete();
        return back()->with('success', 'country Deleted Successfully.');
        
    }
}
