<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Storyteller;
use App\Models\Destination;
use App\Models\Tourtype;
Use Auth;
use Hash;

class StorytellerController extends Controller
{
    public function index()
    {
        $results=Storyteller::get();
        return view('admin.all_storyteller')->with('results',$results);
    }
    public function add_storyteller($id=0)
    {
        if($id>0){
            $data=Storyteller::where('id',$id)->first();
        }else{
            $data=array();
        }
        $destinations=Destination::where('status',1)->get();
        $tourtypes=Tourtype::where('status',1)->get();
        
        return view('admin.add_storyteller')
        ->with('edit',$data)
        ->with('tourtypes',$tourtypes)
        ->with('destinations',$destinations);
    }
    public function submitstoryteller(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'destination' => 'required',
            'tourtype' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'emergency_number' => 'required',
            'dob' => 'required',
            'recruited_form' => 'required',
            'doj' => 'required',
            'poj' => 'required',
            'ptf' => 'required',
            'ptp' => 'required',
            'allowance' => 'required',
            'local_address' => 'required',
            'education' => 'required',
            'blood_group' => 'required',
            'id_proof_number' => 'required',
            'id_proof_type' => 'required',
            'police_clearance_certificate' => 'required',
            'account_no' => 'required',
            'account_holder_name' => 'required',
            'ifsc_code' => 'required',
            'bank_name' => 'required',
            'branch' => 'required',
        ]);
        if($req->id){
            $storyteller = Storyteller::find($req->id);
        }
        else{
            $storyteller = new Storyteller;
        }
        if($req->profile_img)
            {
                @unlink("upload\profileimages\/".$storyteller->profile_img);
                $profile_img = rand().time().'.'.$req->profile_img->getClientOriginalExtension();
                $req->profile_img->move('upload/profileimages', $profile_img);
                $storyteller->profile_img = $profile_img;
            }
            $storyteller->destination_id = $req->destination;
            $storyteller->tour_type_id = $req->tourtype;
            $storyteller->name = $req->name;
            $storyteller->email = $req->email;
            $storyteller->phone = $req->phone;
            $storyteller->emergency_number = $req->emergency_number;
            $storyteller->dob = $req->dob;
            $storyteller->recruited_form = $req->recruited_form;
            $storyteller->doj = $req->doj;
            $storyteller->poj = $req->poj;
            $storyteller->ptf = $req->ptf;
            $storyteller->ptp = $req->ptp;
            $storyteller->allowance = $req->allowance;
            $storyteller->local_address = $req->local_address;
            $storyteller->education = $req->education;
            $storyteller->blood_group = $req->blood_group;
            $storyteller->id_proof_number = $req->id_proof_number;
            $storyteller->id_proof_type = $req->id_proof_type;
            $storyteller->police_clearance_certificate = $req->police_clearance_certificate;
            $storyteller->account_no = $req->account_no;
            $storyteller->account_holder_name = $req->account_holder_name;
            $storyteller->ifsc_code = $req->ifsc_code;
            $storyteller->bank_name = $req->bank_name;
            $storyteller->branch = $req->branch;
            
            $storyteller->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($storyteller->save())
            {
                return redirect('admin/all-storyteller')->with('success', 'Storyteller '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-storyteller')->with('warning', 'failed!');
            }
    }

    public function status_storyteller($id=0)
    {
        $data=Storyteller::where('id',$id)->first();
        
        if($data->status==1){
            Storyteller::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Storyteller::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Storyteller status changed successfully.');
    }
    public function delete_storyteller($id=0)
    {
        $data=Storyteller::where('id',$id)->first();
        @unlink("upload\images\/".$data->profile_img);
        Storyteller::where('id',$id)->delete();
        return back()->with('success', 'Storyteller Deleted Successfully.');
        
    }
}
