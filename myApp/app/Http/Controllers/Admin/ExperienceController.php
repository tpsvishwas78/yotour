<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\ProductCategory;
use App\Models\TourLanguage;
use App\Models\Destination;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Theme;
use App\Models\ProductInclusionsExclusions;
use App\Models\ProductItinerary;
use App\Models\ProductPricing;
use App\Models\ProductTimeDurationSlot;
use App\Models\ProductTourTime;
use App\Models\Product_paid_tour_price;


class ExperienceController extends Controller
{
    public function add_tour_activity($id=0)
    {

        if($id>0){
            $edit=Product::where('id',$id)->first();
        }else{
            $edit=array();
        }
        
        $countries=Country::where('status',1)->get();
        $themes=Theme::where('status',1)->get();
        $languages=TourLanguage::where('status',1)->get();
        $tourcats=ProductCategory::where('status',1)->where('type',1)->where('parent',0)->get();
        $activitycats=ProductCategory::where('status',1)->where('type',2)->where('parent',0)->get();
        $inclusionscats=ProductCategory::where('status',1)->where('type',7)->where('parent',0)->get();
        return view('admin.add_tour_activity')
        ->with('countries',$countries)
        ->with('activitycats',$activitycats)
        ->with('inclusionscats',$inclusionscats)
        ->with('languages',$languages)
        ->with('themes',$themes)
        ->with('edit',$edit)
        ->with('tourcats',$tourcats);
        
    }
    public function submittouractivity(Request $req)
    {
               
        
        $req->validate([
            'country' => 'required',
            'city' => 'required',
            'product_name' => 'required',
            'tour_category' => 'required',
            'activity_category' => 'required',
            ]);

            if($req->id){
                $product = Product::find($req->id);
            }
            else{
                $product = new Product;
            }
            if($req->featured_img)
            {
                @unlink("upload\images\/".$product->featured_img);
                $featured_img = rand().time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $featured_img);
                $product->featured_img = $featured_img;
            }
            $slug = str_slug($req->product_name, "-"); 
            $product->product_name = $req->product_name;
            $product->slug = $slug;
            $product->country = $req->country;
            $product->city = $req->city;
            $product->tour_category = $req->tour_category;
            $product->activity_category = $req->activity_category;
            $product->theme = $req->theme;
            $product->short_description = $req->short_description;
            $product->highlight_tour = $req->highlight_tour;
            $product->product_description = $req->product_description;
            $product->video_link = $req->video_link;
            $product->pickup = $req->pickup;
            $product->pickup_latitude = $req->pickup_latitude;
            $product->pickup_longitude = $req->pickup_longitude;
            $product->dropoff = $req->dropoff;
            $product->drop_latitude = $req->drop_latitude;
            $product->drop_longitude = $req->drop_longitude;
            $product->tour_language = serialize($req->tour_language);
            $product->is_wheelchair = $req->is_wheelchair;
            $product->is_stroller = $req->is_stroller;
            $product->is_animals_allowed = $req->is_animals_allowed;
            $product->is_travelers_easily = $req->is_travelers_easily;
            $product->is_infants_required = $req->is_infants_required;
            $product->is_infant_seats_available = $req->is_infant_seats_available;
            $product->travelers_with_back_problems = $req->travelers_with_back_problems;
            $product->pregnant_travelers = $req->pregnant_travelers;
            $product->serious_medical = $req->serious_medical;
            $product->physical_difficulty = $req->physical_difficulty;

            
            $product->cancellation_policy = $req->cancellation_policy;
            $product->additional_info = $req->additional_info;
            $product->product_type = 1;
            $product->product_sub_type = 1;
            
            if($product->save())
            {
                if($req->hasfile('gallary'))
                {
                    if($req->id){
                        foreach($req->file('gallary') as $image)
                    {
                    $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new ProductImage;
                            $galery->product_id=$req->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                    }else{
                        foreach($req->file('gallary') as $image)
                    {
                        $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new ProductImage;
                            $galery->product_id=$product->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                     }
                    
                }
                if($req->has('itinerary_title')){
            
                    foreach ($req->itinerary_title as $key => $value) {
        
                        if($value){

                        
                        if(@$req->itinerary_id[$key]){
                            $itinerary = ProductItinerary::find($req->itinerary_id[$key]);
                        }
                        else{
                            $itinerary = new ProductItinerary;
                        }
                        if($req->hasfile('itinerary_img'))
                        {
                            if(@$req->itinerary_img[$key]){
                                @unlink("upload\images\/".$itinerary->itinerary_img);
                                $itinerary_img = rand().time().'.'.$req->itinerary_img[$key]->getClientOriginalExtension();
                                $req->itinerary_img[$key]->move('upload/images', $itinerary_img);
                                $itinerary->itinerary_img = $itinerary_img;
                            }
                        }
                      
                        $itinerary->itinerary_title = $value;
                        $itinerary->place_type = $req->place_type[$key];
                        $itinerary->describe_experience = $req->describe_experience[$key];
                        $itinerary->spent_time = serialize($req->spent_time[$key]);
                        $itinerary->included_price_tour = $req->included_price_tour[$key];
                        $itinerary->product_id=$product->id;
                        $itinerary->save();
                    }
                }
                    
                }

                if($req->inclusion_package){
                    if($req->inclusion_id){
                        $inclusion = ProductInclusionsExclusions::find($req->inclusion_id);
                    }
                    else{
                        $inclusion = new ProductInclusionsExclusions;
                    }
                    $inclusion->inclusion_package = $req->inclusion_package;
                    $inclusion->inclusion_category = $req->inclusion_category;
                    $inclusion->inclusion_sub_category = serialize($req->inclusion_sub_category);
                    $inclusion->inclusion_description = $req->inclusion_description;
                    $inclusion->exclusion_package = $req->exclusion_package;
                    $inclusion->exclusion_category = $req->exclusion_category;
                    $inclusion->exclusion_sub_category = serialize($req->exclusion_sub_category);
                    $inclusion->exclusion_description = $req->exclusion_description;
                    $inclusion->product_id=$product->id;
                    $inclusion->save();
                }

                if($req->tour_duration){
                    if($req->tour_duration_id){
                        $timeslot = ProductTimeDurationSlot::find($req->tour_duration_id);
                    }
                    else{
                        $timeslot = new ProductTimeDurationSlot;
                    }
                    $timeslot->tour_duration = $req->tour_duration;
                    $timeslot->fixed_duration = $req->fixed_duration;
                    $timeslot->flexible_duration_minus = $req->flexible_duration_minus;
                    $timeslot->flexible_duration_hour = $req->flexible_duration_hour;
                    $timeslot->start_booking_date = $req->start_booking_date;
                    $timeslot->time_slot_days = serialize($req->time_slot_days);
                    $timeslot->specific_booking_date = $req->specific_booking_date;
                    $timeslot->specific_block_booking_date = $req->specific_block_booking_date;
                    $timeslot->hour_before_tour_time = $req->hour_before_tour_time;
                    $timeslot->advance_notice_time = $req->advance_notice_time;
                    $timeslot->product_id=$product->id;
                    $timeslot->save();
                }
               
                if($req->has('start_tour_time')){
            
                    foreach ($req->start_tour_time as $key => $tourtime) {
                        
                        if($tourtime){
                            if(@$req->time_id[$key]){
                                $tourtiming = ProductTourTime::find($req->time_id[$key]);
                            }
                            else{
                                $tourtiming = new ProductTourTime;
                            }
                            
                            $tourtiming->start_tour_time = $tourtime;
                            $tourtiming->end_tour_time = $req->end_tour_time[$key];
                            $tourtiming->product_id=$product->id;
                            $tourtiming->save();
                        }
                    }
                    
                }

                if($req->currency){
                    if($req->price_id){
                        $pricing = ProductPricing::find($req->price_id);
                    }
                    else{
                        $pricing = new ProductPricing;
                    }
                    
                    $pricing->currency = $req->currency;
                    $pricing->min_age_adult = $req->min_age_adult;
                    $pricing->max_age_adult = $req->max_age_adult;
                    $pricing->min_age_child = $req->min_age_child;
                    $pricing->max_age_child = $req->max_age_child;
                    $pricing->min_age_youth = $req->min_age_youth;
                    $pricing->max_age_youth = $req->max_age_youth;
                    $pricing->max_no_allow_single_booking = $req->max_no_allow_single_booking;
                    $pricing->max_no_allow_travelers = $req->max_no_allow_travelers;
                    $pricing->pricing_type = $req->pricing_type;
                    $pricing->pricing_options = $req->pricing_options;
                    $pricing->add_attributes = $req->add_attributes;
                    
                    $pricing->no_people_book_for_free = $req->no_people_book_for_free;
                    $pricing->limit_cross_price = $req->limit_cross_price;
                    $pricing->price_for_tour_paid = $req->price_for_tour_paid;
                    $pricing->product_id=$product->id;
                    $pricing->save();
                   
                    if($req->has('min_person_required')){
					foreach ($req->min_person_required as $key => $pricedata) {                        
                        
                        if($pricedata){
                            if(@$req->paidtourprice_id[$key]){ 
                                $paidtourprice = Product_paid_tour_price::find($req->paidtourprice_id[$key]);
                            }
                            else{
                                $paidtourprice = new Product_paid_tour_price;
                            }
                       
                    $paidtourprice->min_person_required = $req->min_person_required[$key];
                    $paidtourprice->pricing_tour_type = serialize($req->pricing_tour_type[$key]);                
                    $paidtourprice->adult_price = $req->adult_price[$key];
                    $paidtourprice->child_price = $req->child_price[$key];
                    $paidtourprice->youth_price = $req->youth_price[$key];
                    $paidtourprice->group_pricing = $req->group_pricing[$key];
                    $paidtourprice->no_of_people_in_group = $req->no_of_people_in_group[$key];
                    $paidtourprice->group_pricing_min = $req->group_pricing_min[$key];
                    $paidtourprice->group_pricing_max = $req->group_pricing_max[$key];
                    $paidtourprice->group_pricing_price = $req->group_pricing_price[$key];
                    $paidtourprice->tiered_pricing_from = $req->tiered_pricing_from[$key];
                    $paidtourprice->tiered_pricing_to = $req->tiered_pricing_to[$key];
                    $paidtourprice->tiered_pricing = $req->tiered_pricing[$key];
                    $paidtourprice->tiered_pricing_per_person_price = $req->tiered_pricing_per_person_price[$key];
                    $paidtourprice->tax_name = $req->tax_name[$key];
                    $paidtourprice->tax_rate = $req->tax_rate[$key];
                    $paidtourprice->pricing_days = serialize($req->pricing_days[$key]);
                    $paidtourprice->product_id=$product->id;
                      //echo '<pre>';
                     // echo $req->paidtourprice_id[$key];
                      // print_r(serialize($req->pricing_tour_type[$key])); 
                       // die();
                    $paidtourprice->save();
                  
                        }
                    }
                    }
					
                    
                }
                
            
                

                return redirect('admin/experience/add-tour-activity')->with('success', 'New Tour/Activity '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/experience/add-tour-activity')->with('warning', 'failed!');
            }
            
    }

    //add tickets

    public function add_tickets($id=0)
    {

        if($id>0){
            $edit=Product::where('id',$id)->first();
        }else{
            $edit=array();
        }
        
        $countries=Country::where('status',1)->get();
        $languages=TourLanguage::where('status',1)->get();
        $types=ProductCategory::where('status',1)->where('type',3)->where('parent',0)->get();
        $inclusionscats=ProductCategory::where('status',1)->where('type',7)->where('parent',0)->get();
        return view('admin.add_tickets')
        ->with('countries',$countries)
        ->with('types',$types)
        ->with('inclusionscats',$inclusionscats)
        ->with('languages',$languages)
        ->with('edit',$edit);
        
    }
    public function submittickets(Request $req)
    {
        $req->validate([
            'country' => 'required',
            'city' => 'required',
            'title' => 'required',
            'ticket_type' => 'required',
            'minimum_age' => 'required',
            ]);

            if($req->id){
                $product = Product::find($req->id);
            }
            else{
                $product = new Product;
            }
            if($req->featured_img)
            {
                @unlink("upload\images\/".$product->featured_img);
                $featured_img = rand().time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $featured_img);
                $product->featured_img = $featured_img;
            }
            $slug = str_slug($req->title, "-"); 
            $product->product_name = $req->title;
            $product->slug = $slug;
            $product->country = $req->country;
            $product->city = $req->city;
            $product->ticket_type = $req->ticket_type;
            $product->minimum_age = $req->minimum_age;
            $product->product_description = $req->product_description;
            $product->video_link = $req->video_link;

            $product->cancellation_policy = $req->cancellation_policy;
            $product->additional_info = $req->additional_info;
            $product->product_type = 1;
            $product->product_sub_type = 2;
            
            if($product->save())
            {
                if($req->hasfile('gallary'))
                {
                    if($req->id){
                        foreach($req->file('gallary') as $image)
                    {
                    $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new ProductImage;
                            $galery->product_id=$req->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                    }else{
                        foreach($req->file('gallary') as $image)
                    {
                        $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new ProductImage;
                            $galery->product_id=$product->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                     }
                    
                }

                if($req->inclusion_package){
                    if($req->inclusion_id){
                        $inclusion = ProductInclusionsExclusions::find($req->inclusion_id);
                    }
                    else{
                        $inclusion = new ProductInclusionsExclusions;
                    }
                    $inclusion->inclusion_package = $req->inclusion_package;
                    $inclusion->inclusion_category = $req->inclusion_category;
                    $inclusion->inclusion_sub_category = serialize($req->inclusion_sub_category);
                    $inclusion->inclusion_description = $req->inclusion_description;
                    $inclusion->exclusion_package = $req->exclusion_package;
                    $inclusion->exclusion_category = $req->exclusion_category;
                    $inclusion->exclusion_sub_category = serialize($req->exclusion_sub_category);
                    $inclusion->exclusion_description = $req->exclusion_description;
                    $inclusion->product_id=$product->id;
                    $inclusion->save();
                }

                if($req->start_booking_date){
                    if($req->tour_duration_id){
                        $timeslot = ProductTimeDurationSlot::find($req->tour_duration_id);
                    }
                    else{
                        $timeslot = new ProductTimeDurationSlot;
                    }
                    $timeslot->start_booking_date = $req->start_booking_date;
                    $timeslot->time_slot_days = serialize($req->time_slot_days);
                    $timeslot->specific_booking_date = $req->specific_booking_date;
                    $timeslot->specific_block_booking_date = $req->specific_block_booking_date;
                    $timeslot->hour_before_tour_time = $req->hour_before_tour_time;
                    $timeslot->advance_notice_time = $req->advance_notice_time;
                    $timeslot->specific_entry_time = $req->specific_entry_time;
                    $timeslot->product_id=$product->id;
                    $timeslot->save();
                }
                if($req->has('start_tour_time')){
            
                    foreach ($req->start_tour_time as $key => $time) {
                        
                        if($time){
                            if(@$req->time_id[$key]){
                                $timing = ProductTourTime::find($req->time_id[$key]);
                            }
                            else{
                                $timing = new ProductTourTime;
                            }
                            
                            $timing->start_tour_time = $time;
                            $timing->end_tour_time = $req->end_tour_time[$key];
                            $timing->product_id=$product->id;
                            $timing->save();
                        }
                    }
                    
                }

                if($req->currency){
                    if($req->price_id){
                        $pricing = ProductPricing::find($req->price_id);
                    }
                    else{
                        $pricing = new ProductPricing;
                    }
                    $pricing->currency = $req->currency;
                    $pricing->min_age_adult = $req->min_age_adult;
                    $pricing->max_age_adult = $req->max_age_adult;
                    $pricing->min_age_child = $req->min_age_child;
                    $pricing->max_age_child = $req->max_age_child;
                    $pricing->min_age_youth = $req->min_age_youth;
                    $pricing->max_age_youth = $req->max_age_youth;
                    $pricing->max_no_allow_single_booking = $req->max_no_allow_single_booking;
                    $pricing->max_no_allow_travelers = $req->max_no_allow_travelers;
                    $pricing->pricing_type = $req->pricing_type;
                    $pricing->pricing_options = $req->pricing_options;
                    $pricing->add_attributes = $req->add_attributes;
                    $pricing->min_person_required = $req->min_person_required;
                    $pricing->adult_price = $req->adult_price;
                    $pricing->child_price = $req->child_price;
                    $pricing->youth_price = $req->youth_price;
                    $pricing->group_pricing = $req->group_pricing;
                    $pricing->no_of_people_in_group = $req->no_of_people_in_group;
                    $pricing->group_pricing_min = $req->group_pricing_min;
                    $pricing->group_pricing_max = $req->group_pricing_max;
                    $pricing->group_pricing_price = $req->group_pricing_price;
                    $pricing->tax_name = $req->tax_name;
                    $pricing->tax_rate = $req->tax_rate;
                    $pricing->pricing_days = serialize($req->pricing_days);
                    $pricing->product_id=$product->id;
                    $pricing->save();
                }

                return redirect('admin/experience/add-tickets')->with('success', 'New Tickets '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/experience/add-tickets')->with('warning', 'failed!');
            }
            
    }

    // tranceport - rental

    public function add_transport_rental($id=0)
    {

        if($id>0){
            $edit=Product::where('id',$id)->first();
        }else{
            $edit=array();
        }
        
        $countries=Country::where('status',1)->get();
        $languages=TourLanguage::where('status',1)->get();
        $transportcats=ProductCategory::where('status',1)->where('type',4)->where('parent',0)->get();
        $inclusionscats=ProductCategory::where('status',1)->where('type',7)->where('parent',0)->get();
        return view('admin.add_transport_rental')
        ->with('countries',$countries)
        ->with('transportcats',$transportcats)
        ->with('inclusionscats',$inclusionscats)
        ->with('languages',$languages)
        ->with('edit',$edit);
        
    }
    public function submittransportrental(Request $req)
    {
        $req->validate([
            'country' => 'required',
            'city' => 'required',
            'product_title' => 'required',
            'transport_category' => 'required',
            'rent_category' => 'required',
            ]);

            if($req->id){
                $product = Product::find($req->id);
            }
            else{
                $product = new Product;
            }
            if($req->featured_img)
            {
                @unlink("upload\images\/".$product->featured_img);
                $featured_img = rand().time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $featured_img);
                $product->featured_img = $featured_img;
            }
            $slug = str_slug($req->product_title, "-"); 
            $product->product_name = $req->product_title;
            $product->slug = $slug;
            $product->country = $req->country;
            $product->city = $req->city;
            $product->transport_category = $req->transport_category;
            $product->rent_category = $req->rent_category;
            $product->rent_type = $req->rent_type;
            $product->minimum_age = $req->minimum_age;
            $product->video_link = $req->video_link;
            $product->pickup = $req->pickup;
            $product->pickup_latitude = $req->pickup_latitude;
            $product->pickup_longitude = $req->pickup_longitude;
            $product->dropoff = $req->dropoff;
            $product->drop_latitude = $req->drop_latitude;
            $product->drop_longitude = $req->drop_longitude;
            $product->home_delivery = $req->home_delivery;

            $product->cancellation_policy = $req->cancellation_policy;
            $product->additional_info = $req->additional_info;
            $product->product_type = 1;
            $product->product_sub_type = 3;
            
            if($product->save())
            {
                if($req->hasfile('gallary'))
                {
                    if($req->id){
                        foreach($req->file('gallary') as $image)
                    {
                    $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new ProductImage;
                            $galery->product_id=$req->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                    }else{
                        foreach($req->file('gallary') as $image)
                    {
                        $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new ProductImage;
                            $galery->product_id=$product->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                     }
                    
                }

                if($req->inclusion_category){
                    if($req->inclusion_id){
                        $inclusion = ProductInclusionsExclusions::find($req->inclusion_id);
                    }
                    else{
                        $inclusion = new ProductInclusionsExclusions;
                    }
                    $inclusion->inclusion_category = $req->inclusion_category;
                    $inclusion->inclusion_sub_category = serialize($req->inclusion_sub_category);
                    $inclusion->inclusion_description = $req->inclusion_description;
                    $inclusion->exclusion_category = $req->exclusion_category;
                    $inclusion->exclusion_sub_category = serialize($req->exclusion_sub_category);
                    $inclusion->exclusion_description = $req->exclusion_description;
                    $inclusion->product_id=$product->id;
                    $inclusion->save();
                }
                if($req->tour_duration){
                    if($req->tour_duration_id){
                        $timeslot = ProductTimeDurationSlot::find($req->tour_duration_id);
                    }
                    else{
                        $timeslot = new ProductTimeDurationSlot;
                    }
                    $timeslot->tour_duration = $req->tour_duration;
                    $timeslot->fixed_duration = $req->fixed_duration;
                    $timeslot->flexible_duration_minus = $req->flexible_duration_minus;
                    $timeslot->flexible_duration_hour = $req->flexible_duration_hour;
                    $timeslot->start_booking_date = $req->start_booking_date;
                    $timeslot->time_slot_days = serialize($req->time_slot_days);
                    $timeslot->specific_booking_date = $req->specific_booking_date;
                    $timeslot->specific_block_booking_date = $req->specific_block_booking_date;
                    $timeslot->hour_before_tour_time = $req->hour_before_tour_time;
                    $timeslot->product_id=$product->id;
                    $timeslot->save();
                }

                if($req->currency){
                    if($req->price_id){
                        $pricing = ProductPricing::find($req->price_id);
                    }
                    else{
                        $pricing = new ProductPricing;
                    }
                    $pricing->currency = $req->currency;
                    $pricing->pricing_type = $req->pricing_type;
                    $pricing->pricing_options = $req->pricing_options;
                    $pricing->add_attributes = $req->add_attributes;
                    $pricing->min_person_required = $req->min_person_required;
                    $pricing->adult_price = $req->adult_price;
                    $pricing->child_price = $req->child_price;
                    $pricing->youth_price = $req->youth_price;
                    $pricing->deposit = $req->deposit;
                    $pricing->tax_name = $req->tax_name;
                    $pricing->tax_rate = $req->tax_rate;
                    $pricing->pricing_days = serialize($req->pricing_days);
                    $pricing->product_id=$product->id;
                    $pricing->save();
                }


                return redirect('admin/experience/add-transport-rental')->with('success', 'New Transport/Rental '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/experience/add-transport-rental')->with('warning', 'failed!');
            }
            
    }

    //create holiday

    public function create_holiday($id=0)
    {
        if($id>0){
            $edit=Product::where('id',$id)->first();
        }else{
            $edit=array();
        }
        
        $countries=Country::where('status',1)->get();
        $themes=Theme::where('status',1)->get();
        $languages=TourLanguage::where('status',1)->get();
        $holidaycats=ProductCategory::where('status',1)->where('type',9)->where('parent',0)->get();
        $inclusionscats=ProductCategory::where('status',1)->where('type',7)->where('parent',0)->get();
        return view('admin.create_holiday')
        ->with('countries',$countries)
        ->with('inclusionscats',$inclusionscats)
        ->with('languages',$languages)
        ->with('themes',$themes)
        ->with('edit',$edit)
        ->with('holidaycats',$holidaycats);
        
    }
    public function submitholiday(Request $req)
    {
        
        $req->validate([
            'country' => 'required',
            'city' => 'required',
            'product_name' => 'required',
            'holiday_category' => 'required',
            ]);

            if($req->id){
                $product = Product::find($req->id);
            }
            else{
                $product = new Product;
            }
            if($req->featured_img)
            {
                @unlink("upload\images\/".$product->featured_img);
                $featured_img = rand().time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $featured_img);
                $product->featured_img = $featured_img;
            }
            $slug = str_slug($req->product_name, "-"); 
            $product->product_name = $req->product_name;
            $product->slug = $slug;
            $product->country = $req->country;
            $product->city = $req->city;
            $product->holiday_category = $req->holiday_category;
            $product->theme = $req->theme;
            $product->video_link = $req->video_link;
            $product->starting_point = $req->starting_point;
            $product->starting_latitude = $req->starting_latitude;
            $product->starting_longitude = $req->starting_longitude;
            $product->pickup = $req->pickup;
            $product->pickup_latitude = $req->pickup_latitude;
            $product->pickup_longitude = $req->pickup_longitude;
            $product->dropoff = $req->dropoff;
            $product->drop_latitude = $req->drop_latitude;
            $product->drop_longitude = $req->drop_longitude;

            $product->tour_language = serialize($req->tour_language);
            $product->is_wheelchair = $req->is_wheelchair;
            $product->is_stroller = $req->is_stroller;
            $product->is_animals_allowed = $req->is_animals_allowed;
            $product->is_travelers_easily = $req->is_travelers_easily;
            $product->is_infants_required = $req->is_infants_required;
            $product->is_infant_seats_available = $req->is_infant_seats_available;
            $product->travelers_with_back_problems = $req->travelers_with_back_problems;
            $product->pregnant_travelers = $req->pregnant_travelers;
            $product->serious_medical = $req->serious_medical;
            $product->physical_difficulty = $req->physical_difficulty;
            
            $product->cancellation_policy = $req->cancellation_policy;
            $product->additional_info = $req->additional_info;
            $product->product_type = 2;
            
            if($product->save())
            {
                if($req->hasfile('gallary'))
                {
                    if($req->id){
                        foreach($req->file('gallary') as $image)
                    {
                    $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new ProductImage;
                            $galery->product_id=$req->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                    }else{
                        foreach($req->file('gallary') as $image)
                    {
                        $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new ProductImage;
                            $galery->product_id=$product->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                     }
                    
                }

                if($req->has('day_title')){
            
                    foreach ($req->day_title as $key => $value) {
        
                        if($value){

                        
                        if(@$req->itinerary_id[$key]){
                            $itinerary = ProductItinerary::find($req->itinerary_id[$key]);
                        }
                        else{
                            $itinerary = new ProductItinerary;
                        }
                        if($req->hasfile('itinerary_img'))
                        {
                            if(@$req->itinerary_img[$key]){
                                @unlink("upload\images\/".$itinerary->itinerary_img);
                                $itinerary_img = rand().time().'.'.$req->itinerary_img[$key]->getClientOriginalExtension();
                                $req->itinerary_img[$key]->move('upload/images', $itinerary_img);
                                
                                $itinerary->itinerary_img = $itinerary_img;
                                
                            }
                        }
                        $itinerary->day_title = $value;
                        $itinerary->itinerary_title = @$req->itinerary_title[$key];
                        $itinerary->place_type = @$req->place_type[$key];
                        $itinerary->describe_experience = @$req->describe_experience[$key];
                        $itinerary->spent_time = serialize(@$req->spent_time[$key]);
                        $itinerary->included_price_tour = @$req->included_price_tour[$key];
                        $itinerary->meal_type = @$req->meal_type[$key];
                        $itinerary->meal_description = @$req->meal_description[$key];
                        $itinerary->accomodation_details = @$req->accomodation_details[$key];
                        $itinerary->product_id=$product->id;
                        $itinerary->save();
                    }
                }
                    
                }

                if($req->inclusion_package){
                    if($req->inclusion_id){
                        $inclusion = ProductInclusionsExclusions::find($req->inclusion_id);
                    }
                    else{
                        $inclusion = new ProductInclusionsExclusions;
                    }
                    $inclusion->inclusion_package = @$req->inclusion_package;
                    $inclusion->inclusion_category = @$req->inclusion_category;
                    $inclusion->inclusion_sub_category = serialize(@$req->inclusion_sub_category);
                    $inclusion->inclusion_description = @$req->inclusion_description;
                    $inclusion->exclusion_package = @$req->exclusion_package;
                    $inclusion->exclusion_category = @$req->exclusion_category;
                    $inclusion->exclusion_sub_category = serialize(@$req->exclusion_sub_category);
                    $inclusion->exclusion_description = @$req->exclusion_description;
                    $inclusion->product_id=$product->id;
                    $inclusion->save();
                }

                if($req->holiday_start_date){
                    if($req->tour_duration_id){
                        $timeslot = ProductTimeDurationSlot::find($req->tour_duration_id);
                    }
                    else{
                        $timeslot = new ProductTimeDurationSlot;
                    }
                    $timeslot->holiday_start_date = @$req->holiday_start_date;
                    $timeslot->holiday_end_date = @$req->holiday_end_date;
                    $timeslot->holiday_start_tour_time = @$req->holiday_start_tour_time;
                    $timeslot->holiday_end_tour_time = @$req->holiday_end_tour_time;
                    $timeslot->additional_date_time_slot = @$req->additional_date_time_slot;
                    $timeslot->min_people = @$req->min_people;
                    $timeslot->max_people = @$req->max_people;
                    $timeslot->hour_before_tour_time = @$req->hour_before_tour_time;
                    $timeslot->product_id=$product->id;
                    $timeslot->save();
                }
                

                if($req->currency){
                    if($req->price_id){
                        $pricing = ProductPricing::find($req->price_id);
                    }
                    else{
                        $pricing = new ProductPricing;
                    }
                    $pricing->currency = $req->currency;
                    $pricing->min_age_adult = $req->min_age_adult;
                    $pricing->max_age_adult = $req->max_age_adult;
                    $pricing->min_age_child = $req->min_age_child;
                    $pricing->max_age_child = $req->max_age_child;
                    $pricing->min_age_youth = $req->min_age_youth;
                    $pricing->max_age_youth = $req->max_age_youth;
                    $pricing->pricing_type = $req->pricing_type;
                    $pricing->adult_price = $req->adult_price;
                    $pricing->child_price = $req->child_price;
                    $pricing->youth_price = $req->youth_price;
                    $pricing->min_age_adult_price = $req->min_age_adult_price;
                    $pricing->max_age_adult_price = $req->max_age_adult_price;
                    $pricing->min_age_child_price = $req->min_age_child_price;
                    $pricing->max_age_child_price = $req->max_age_child_price;
                    $pricing->min_age_youth_price = $req->min_age_youth_price;
                    $pricing->max_age_youth_price = $req->max_age_youth_price;
                    $pricing->tax_name = $req->tax_name;
                    $pricing->tax_rate = $req->tax_rate;
                    $pricing->product_id=$product->id;
                    $pricing->save();
                }


                return redirect('admin/create-holiday')->with('success', 'New Holiday '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/create-holiday')->with('warning', 'failed!');
            }
            
    }

    public function getcitybycountry(Request $req)
    {
        $cities=Destination::where('status',1)->where('country_id',$req->id)->get();
        $html='';

        foreach ($cities as $key => $city) {
            $html .='<option value="'.$city->id.'">'.$city->name.'</option>';
        }

        echo json_encode($html);
    }
    public function getrentcategory(Request $req)
    {
        $rents=ProductCategory::where('status',1)->where('parent',$req->id)->get();
        $html='';

        foreach ($rents as $key => $rent) {
            $html .='<option value="'.$rent->id.'">'.$rent->name.'</option>';
        }

        echo json_encode($html);
    }
    
    public function getinclusionsubcate(Request $req)
    {
        $inclusionscats=ProductCategory::where('status',1)->where('type',7)->where('parent',$req->id)->get();
        $html='';

        foreach ($inclusionscats as $key => $inclusionscat) {
            $html .='<option value="'.$inclusionscat->id.'">'.$inclusionscat->name.'</option>';
        }

        echo json_encode($html);
    }
    public function deleteproductgalleryimage(Request $req)
    {
        $img=ProductImage::where('id',$req->id)->first();
        @unlink("upload\images\/".$img->image);
        ProductImage::where('id',$req->id)->delete();
        echo 1;
    }
    public function removeitinary(Request $req)
    {
        ProductItinerary::where('id',$req->id)->delete();
        echo json_encode(1);
    }
    public function removetourprice(Request $req)
    {
        ProductPricing::where('id',$req->id)->delete();
        echo json_encode(1);
    }
    public function removetourtime(Request $req)
    {
        ProductTourTime::where('id',$req->id)->delete();
        echo json_encode(1);
    }
}
