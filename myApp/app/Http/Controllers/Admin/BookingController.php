<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Paytype;
use App\Models\Booking;
use App\Models\Channel;
use App\Models\Tourtype;
use App\Models\Destination;
use App\Models\User;
use App\Models\Tour;
use App\Models\Storyteller;
use App\Models\ProductCategory;
use App\Models\TourLanguage;
use App\Models\Country;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Theme;


class BookingController extends Controller
{
    public function index()
    {
        $upcomingbookingcount=Booking::where('date','>=',date('Y-m-d'))->where('is_allocate',1)->count();
        $unallocatebookingcount=Booking::where('date','>=',date('Y-m-d'))->where('is_allocate',0)->count();
        $upcomingbookings=Booking::where('date','>=',date('Y-m-d'))->where('is_allocate',1)->groupBy('date')->get();
        $pastbookingcount=Booking::where('date','<',date('Y-m-d'))->count();
        return view('admin.all_booking')
        ->with('upcomingbookings',$upcomingbookings)
        ->with('upcomingbookingcount',$upcomingbookingcount)
        ->with('unallocatebookingcount',$unallocatebookingcount)
        ->with('pastbookingcount',$pastbookingcount);
    }
    public function add_booking($id=0)
    {
        if($id>0){
            $data=Booking::where('id',$id)->first();
        }else{
            $data=array();
        }
        $channels=Channel::where('status',1)->get();
        $tourtypes=Tourtype::where('status',1)->get();
        $destinations=Destination::where('status',1)->get();
        $paytypes=Paytype::where('status',1)->get();
        
        return view('admin.add_booking')
        ->with('channels',$channels)
        ->with('tourtypes',$tourtypes)
        ->with('destinations',$destinations)
        ->with('paytypes',$paytypes)
        ->with('edit',$data);
    }
    public function submitbooking(Request $req)
    {
        $req->validate([
            'channel' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'tourtype' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'city' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'tour_name' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'number_of_people' => 'required',
            'date' => 'required',
            'time' => 'required',
            'booking_amount' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'booking_amt_paytype' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'customer' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'booking_reference' => (isset($req->id) and $req->id > 0) ? '' : 'required',
        ]);
        if($req->id){
            $booking = Booking::find($req->id);
        }
        else{
            $booking = new Booking;
        }
        $date = date('Y-m-d', strtotime($req['date']));

        if(!isset($req->id)){
            $booking->channel_id = $req->channel;
            $booking->tour_type_id = $req->tourtype;
            $booking->destination_id = $req->city;
            $booking->tour_id = $req->tour_name;
            $booking->booking_amount = $req->booking_amount;
            $booking->booking_amt_paytype = $req->booking_amt_paytype;
            $booking->customer_id = $req->customer;
            $booking->booking_reference = $req->booking_reference;
        }

        if($req->reschedule){
            $booking->rescheduleMsg = $req->reschedule;
        }
           
            $booking->number_of_people = $req->number_of_people;
            $booking->date = $date;
            $booking->time = $req->time;
           
            $booking->discount = $req->discount;
            $booking->additional_amount = $req->additional_amount;
            $booking->additional_amt_paytupe = $req->additional_amt_paytupe;
            $booking->reason_additional_amount = $req->reason_additional_amount;
            
           


            if($booking->save())
            {
                if($req->submit=='allocate'){
                    return redirect('admin/allocate/'.$booking->id);
                }else{
                    return redirect('admin/add-booking')->with('success', 'Booking '. (($req->id) ? 'Reschedule' : 'Created'). ' successfully.');
                }
                
            }else{
               
                return redirect('admin/add-booking')->with('warning', 'failed!');
            }
    }
    public function import_booking($id=0)
    {
        if($id>0){
            $data=Booking::where('id',$id)->first();
        }else{
            $data=array();
        }
        $channels=Channel::where('status',1)->get();
        $tourtypes=Tourtype::where('status',1)->get();
        $destinations=Destination::where('status',1)->get();
        $paytypes=Paytype::where('status',1)->get();
        
        return view('admin.import_booking')
        ->with('channels',$channels)
        ->with('tourtypes',$tourtypes)
        ->with('destinations',$destinations)
        ->with('paytypes',$paytypes)
        ->with('edit',$data);
    }
    public function submitimportbooking(Request $req)
    {
        $req->validate([
            'channel' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'city' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'tour_name' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'number_of_people' => 'required',
            'date' => 'required',
            'time' => 'required',
            'booking_amount' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'booking_amt_paytype' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'customer' => (isset($req->id) and $req->id > 0) ? '' : 'required',
            'booking_reference' => (isset($req->id) and $req->id > 0) ? '' : 'required',
        ]);
        if($req->id){
            $booking = Booking::find($req->id);
        }
        else{
            $booking = new Booking;
        }
        $date = date('Y-m-d', strtotime($req['date']));

        if(!isset($req->id)){
            $booking->channel_id = $req->channel;
            $booking->tour_type_id = $req->producttype;
            $booking->destination_id = $req->city;
            $booking->tour_id = $req->tour_name;
            $booking->booking_amount = $req->booking_amount;
            $booking->booking_amt_paytype = $req->booking_amt_paytype;
            $booking->customer_id = $req->customer;
            $booking->booking_reference = $req->booking_reference;
        }

        if($req->reschedule){
            $booking->rescheduleMsg = $req->reschedule;
        }
           
            $booking->number_of_people = $req->number_of_people;
            $booking->date = $date;
            $booking->time = $req->time;

            if($booking->save())
            {
                if($req->submit=='allocate'){
                    return redirect('admin/allocate/'.$booking->id);
                }else{
                    return redirect('admin/import-booking')->with('success', 'Booking '. (($req->id) ? 'Reschedule' : 'Created'). ' successfully.');
                }
                
            }else{
               
                return redirect('admin/import-booking')->with('warning', 'failed!');
            }
    }
    public function allocate($id=0)
    {
        $storytellers=Storyteller::where('status',1)->get();
        return view('admin.allocate')->with('storytellers',$storytellers);
    }
    public function allocatesubmit(Request $req)
    {
        $allocate = Booking::find($req->id);
        $allocate->is_allocate=1;
        $allocate->storyteller=$req->storyteller;
        $allocate->save();
        echo 1;
    }
    public function cancelbooking(Request $req)
    {
        $allocate = Booking::find($req->id);
        $allocate->status=2;
        $allocate->cancelMsg=$req->cancelMsg;
        $allocate->save();
        return redirect('admin/all-booking');
    }
    public function changebookingstatus(Request $req)
    {
        $name='status'.$req->bid;
        $allocate = Booking::find($req->bid);
        $allocate->status=$req->$name;
        $allocate->save();
        return redirect('admin/upcoming-booking')->with('success', 'Booking Status Changed successfully.');
    }
    
    


    public function upcoming_booking()
    {
        $bookings=Booking::where('date','>=',date('Y-m-d'))->where('is_allocate',1)->get();
        return view('admin.all_upcoming_booking')->with('results',$bookings);
    }
    public function unallocated_booking()
    {
        $bookings=Booking::where('date','>=',date('Y-m-d'))->where('is_allocate',0)->get();
        return view('admin.all_unallocated_booking')->with('results',$bookings);
    }
    public function past_booking()
    {
        $bookings=Booking::where('date','<',date('Y-m-d'))->get();
        return view('admin.all_past_booking')->with('results',$bookings);
    }
    public function getcustomers(Request $req)
    {
        $users=User::where('channel_id',$req->id)->get();
        $html='<select class="form-control  select-select2" name="customer" id="customer">';
        $html.='<option value="">Select</option> ';
        foreach ($users as $key => $user) {
            $html.='<option value="'.$user->id.'">'.$user->name.'</option> ';
        }
        $html.='</select>';
        echo $html;
    }
    public function gettours(Request $req)
    {
        $tours=Product::where('product_type',$req->tid)->where('city',$req->cid)->get();
        $html='<select class="form-control  select-select2" name="tour_name" id="tour_name">';
        $html.='<option value="">Select</option> ';
        foreach ($tours as $key => $tour) {
            $html.='<option value="'.$tour->id.'">'.$tour->product_name.'</option> ';
        }
        $html.='</select>';
        echo $html;
    }
    
    public function all_paytype()
    {
        $results=Paytype::get();
        return view('admin.all_paytype')->with('results',$results);
    }
    public function add_paytype($id=0)
    {
        if($id>0){
            $data=Paytype::where('id',$id)->first();
        }else{
            $data=array();
        }
        
        return view('admin.add_paytype')->with('edit',$data);
    }
    public function submitpaytype(Request $req)
    {
        $req->validate([
            'name' => 'required|unique:paytypes,name,'.$req->id.',id',
        ]);
        if($req->id){
            $paytype = Paytype::find($req->id);
        }
        else{
            $paytype = new Paytype;
        }
        

            $paytype->name = $req->name;
            $paytype->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($paytype->save())
            {
                return redirect('admin/all-paytype')->with('success', 'Payment type '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-paytype')->with('warning', 'failed!');
            }
    }

    public function status_paytype($id=0)
    {
        $data=Paytype::where('id',$id)->first();
        
        if($data->status==1){
            Paytype::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Paytype::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Payment type status changed successfully.');
    }
    public function delete_paytype($id=0)
    {
        $data=Paytype::where('id',$id)->first();
        Paytype::where('id',$id)->delete();
        return back()->with('success', 'Payment type Deleted Successfully.');
        
    }

    //themes

    public function all_theme()
    {
        $results=Theme::get();
        return view('admin.all_theme')->with('results',$results);
    }
    public function add_theme($id=0)
    {
        if($id>0){
            $data=Theme::where('id',$id)->first();
        }else{
            $data=array();
        }
        
        return view('admin.add_theme')->with('edit',$data);
    }
    public function submittheme(Request $req)
    {
        $req->validate([
            'name' => 'required|unique:themes,name,'.$req->id.',id',
        ]);
        if($req->id){
            $theme = Theme::find($req->id);
        }
        else{
            $theme = new Theme;
        }
        

            $theme->name = $req->name;
            $theme->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($theme->save())
            {
                return redirect('admin/all-themes')->with('success', 'Theme '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-theme')->with('warning', 'failed!');
            }
    }

    public function status_theme($id=0)
    {
        $data=Theme::where('id',$id)->first();
        
        if($data->status==1){
            Theme::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Theme::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Theme status changed successfully.');
    }
    public function delete_theme($id=0)
    {
        $data=Theme::where('id',$id)->first();
        Theme::where('id',$id)->delete();
        return back()->with('success', 'Theme Deleted Successfully.');
        
    }

    //product category

    public function all_category()
    {
        $results=ProductCategory::get();
        return view('admin.all_category')->with('results',$results);
    }
    public function add_category($id=0)
    {
        if($id>0){
            $data=ProductCategory::where('id',$id)->first();
        }else{
            $data=array();
        }
        $results=ProductCategory::where('status',1)->where('parent',0)->get();

        return view('admin.add_category')->with('edit',$data)->with('results',$results);
    }
    public function submitcategory(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'type' => 'required',
        ]);
        if($req->id){
            $category = ProductCategory::find($req->id);
        }
        else{
            $category = new ProductCategory;
        }
        
        $slug = str_slug($req->name, "-"); 
            $category->name = $req->name;
            $category->slug = $slug;
            $category->type = $req->type;
            if($req->parent){
                $category->parent = $req->parent;
            }else{
                $category->parent = 0;
            }
            
            $category->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($category->save())
            {
                return redirect('admin/all-category')->with('success', 'Category '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-category')->with('warning', 'failed!');
            }
    }

    public function status_category($id=0)
    {
        $data=ProductCategory::where('id',$id)->first();
        
        if($data->status==1){
            ProductCategory::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            ProductCategory::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Category type status changed successfully.');
    }
    public function delete_category($id=0)
    {
        $data=ProductCategory::where('id',$id)->first();
        ProductCategory::where('id',$id)->delete();
        return back()->with('success', 'Category type Deleted Successfully.');
        
    }

    //Languages

    public function all_language()
    {
        $results=TourLanguage::get();
        return view('admin.all_languages')->with('results',$results);
    }
    public function add_language($id=0)
    {
        if($id>0){
            $data=TourLanguage::where('id',$id)->first();
        }else{
            $data=array();
        }
        
        return view('admin.add_language')->with('edit',$data);
    }
    public function submitlanguage(Request $req)
    {
        $req->validate([
            'name' => 'required|unique:tour_languages,name,'.$req->id.',id',
        ]);
        if($req->id){
            $language = TourLanguage::find($req->id);
        }
        else{
            $language = new TourLanguage;
        }
        if($req->icon)
        {
            @unlink("upload\images\/".$language->icon);
            $icon = rand().time().'.'.$req->icon->getClientOriginalExtension();
            $req->icon->move('upload/images', $icon);
            $language->icon = $icon;
        }
        $slug = str_slug($req->name, "-");

            $language->name = $req->name;
            $language->slug = $slug;
            $language->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($language->save())
            {
                return redirect('admin/all-languages')->with('success', 'Language '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-language')->with('warning', 'failed!');
            }
    }

    public function status_language($id=0)
    {
        $data=TourLanguage::where('id',$id)->first();
        
        if($data->status==1){
            TourLanguage::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            TourLanguage::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Language status changed successfully.');
    }
    public function delete_language($id=0)
    {
        $data=TourLanguage::where('id',$id)->first();
        TourLanguage::where('id',$id)->delete();
        return back()->with('success', 'Language Deleted Successfully.');
        
    }
}
