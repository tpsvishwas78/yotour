<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Channel;
use App\Models\User;
Use Auth;

class ChannelController extends Controller
{
    public function index()
    {
        $results=Channel::get();
        return view('admin.all_channel')->with('results',$results);
    }
    public function add_channel($id=0)
    {
        if($id>0){
            $data=Channel::where('id',$id)->first();
        }else{
            $data=array();
        }
        
        return view('admin.add_channel')->with('edit',$data);
    }
    public function submitchannel(Request $req)
    {
        $req->validate([
            'name' => 'required|unique:channels,name,'.$req->id.',id',
        ]);
        
        if($req->id){
            $channel = Channel::find($req->id);
        }
        else{
            $channel = new Channel;
        }
        if($req->logo)
            {
                @unlink("upload\images\/".$channel->logo);
                $logo = rand().time().'.'.$req->logo->getClientOriginalExtension();
                $req->logo->move('upload/images', $logo);
                $channel->logo = $logo;
            }
            

            $channel->name = $req->name;
            $channel->status = (isset($req->status) and $req->status > 0) ? 1 : 0;
            $channel->website = $req->website;

            if($channel->save())
            {
                return redirect('admin/all-channel')->with('success', 'Channel '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/addchannel')->with('warning', 'failed!');
            }
    }

    public function status_channel($id=0)
    {
        $data=Channel::where('id',$id)->first();
        
        if($data->status==1){
            Channel::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Channel::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Channel status changed successfully.');
    }
    public function delete_channel($id=0)
    {
        $data=Channel::where('id',$id)->first();
        @unlink("upload\images\/".$data->logo);
        Channel::where('id',$id)->delete();
        return back()->with('success', 'Channel Deleted Successfully.');
        
    }
}
