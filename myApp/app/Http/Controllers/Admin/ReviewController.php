<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Review;
use App\Models\Product;

class ReviewController extends Controller
{
    public function index()
    {
        $results=Review::where('is_admin',1)->get();
        return view('admin.all_admin_reviews')->with('results',$results);
    }
    public function all_customer_review()
    {
        $results=Review::where('is_admin',0)->get();
        return view('admin.all_customer_review')->with('results',$results);
    }
    
    public function add_admin_review($id=0)
    {
        if($id>0){
            $data=Review::where('id',$id)->first();
        }else{
            $data=array();
        }
        $products=Product::where('status',1)->get();
        
        return view('admin.add_admin_review')->with('edit',$data)->with('products',$products);
    }
    public function submitadminreview(Request $req)
    {
        $req->validate([
            'product' => 'required',
            'customer_name' => 'required',
            'rating' => 'required',
            'comment' => 'required',
        ]);
        if($req->id){
            $review = Review::find($req->id);
        }
        else{
            $review = new Review;
        }
        if($req->profile_img)
            {
                @unlink("upload\profileimages\/".$review->profile_img);
                $profile_img = rand().time().'.'.$req->profile_img->getClientOriginalExtension();
                $req->profile_img->move('upload/profileimages', $profile_img);
                $review->customer_profile_img = $profile_img;
            }
            
            $review->product_id = $req->product;
            $review->customer_name = $req->customer_name;
            $review->rating = $req->rating;
            $review->comment = $req->comment;
            $review->is_admin = 1;
            
            $review->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($review->save())
            {
                return redirect('admin/all-admin-review')->with('success', 'Review '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-admin-review')->with('warning', 'failed!');
            }
    }

    public function status_review($id=0)
    {
        $data=Review::where('id',$id)->first();
        
        if($data->status==1){
            Review::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Review::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Review status changed successfully.');
    }
    public function delete_review($id=0)
    {
        $data=Review::where('id',$id)->first();
        @unlink("upload\images\/".$data->profile_img);
        Review::where('id',$id)->delete();
        return back()->with('success', 'Review Deleted Successfully.');
        
    }
}
