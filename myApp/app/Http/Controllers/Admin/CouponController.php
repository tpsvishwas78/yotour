<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Coupon;
use App\Models\User;
Use Auth;

class CouponController extends Controller
{
    public function index()
    {
        $results=Coupon::get();
        return view('admin.all_coupon')->with('results',$results);
    }
    public function add_coupon($id=0)
    {
        if($id>0){
            $data=Coupon::where('id',$id)->first();
        }else{
            $data=array();
        }
        
        return view('admin.add_coupon')->with('edit',$data);
    }
    public function submitcoupon(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'coupon_code' => 'required',
            'start_date' => 'required',
            'expire_date' => 'required',
            'min_amount' => 'required',
            'type' => 'required',
            'discount_amt_per' => 'required',
        ]);
        if($req->id){
            $coupon = Coupon::find($req->id);
        }
        else{
            $coupon = new Coupon;
        }
        

            $coupon->name = $req->name;
            $coupon->coupon_code = $req->coupon_code;
            $coupon->start_date = $req->start_date;
            $coupon->expire_date = $req->expire_date;
            $coupon->min_amount = $req->min_amount;
            $coupon->type = $req->type;
            $coupon->discount_amt_per = $req->discount_amt_per;
            $coupon->max_discount_amount = $req->max_discount_amount;
            $coupon->status = (isset($req->status) and $req->status > 0) ? 1 : 0;
            $coupon->description = $req->description;

            if($coupon->save())
            {
                return redirect('admin/all-coupon')->with('success', 'Coupon '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/addcoupon')->with('warning', 'failed!');
            }
    }

    public function status_coupon($id=0)
    {
        $data=Coupon::where('id',$id)->first();
        
        if($data->status==1){
            Coupon::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Coupon::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Coupon status changed successfully.');
    }
    public function delete_coupon($id=0)
    {
        $data=Coupon::where('id',$id)->first();
        Coupon::where('id',$id)->delete();
        return back()->with('success', 'Coupon Deleted Successfully.');
        
    }
}
