<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tour;
use App\Models\Tourtype;
use App\Models\Tourimage;
use App\Models\Destination;
use App\Models\User;
Use Auth;

class TourController extends Controller
{
    public function index()
    {
        $results=Tour::get();
        return view('admin.all_tour')->with('results',$results);
    }
    public function add_tour($id=0)
    {
        if($id>0){
            $data=Tour::where('id',$id)->first();
        }else{
            $data=array();
        }
        $destinations=Destination::where('status',1)->get();
        $tourtypes=Tourtype::where('status',1)->get();
        
        return view('admin.add_tour')
        ->with('edit',$data)
        ->with('destinations',$destinations)
        ->with('tourtypes',$tourtypes);
    }
    public function submittour(Request $req)
    {
        $req->validate([
            'title' => 'required|unique:tours,title,'.$req->id.',id',
            'destination' => 'required',
            'type' => 'required',
        ]);
        if($req->id){
            $tour = Tour::find($req->id);
        }
        else{
            $tour = new Tour;
        }
        if($req->featured_img)
            {
                @unlink("upload\images\/".$tour->featured_img);
                $featured_img = rand().time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $featured_img);
                $tour->featured_img = $featured_img;
            }
           
            $slug = str_slug($req->title, "-"); 

            $tour->title = $req->title;
            $tour->slug = $slug;
            $tour->type = $req->type;
            $tour->destination_id = $req->destination;
            $tour->price = $req->price;
            $tour->location = $req->location;
            $tour->short_description = $req->short_description;
            $tour->status = (isset($req->status) and $req->status > 0) ? 1 : 0;
            $tour->description = $req->description;

            if($tour->save())
            {
                if($req->hasfile('gallary'))
                {
                    if($req->id){
                        foreach($req->file('gallary') as $image)
                    {
                    $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new Tourimage;
                            $galery->tour_id=$req->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                    }else{
                        foreach($req->file('gallary') as $image)
                    {
                        $name=time() . uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move('upload/images', $name);
                    $galery = new Tourimage;
                            $galery->tour_id=$tour->id;
                            $galery->image=$name;
                            $galery->save();
                    }
                     }
                    
                }
                return redirect('admin/all-tour')->with('success', 'Tour '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-tour')->with('warning', 'failed!');
            }
    }

    public function status_tour($id=0)
    {
        $data=Tour::where('id',$id)->first();
        
        if($data->status==1){
            Tour::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Tour::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Tour status changed successfully.');
    }
    public function delete_tour($id=0)
    {
        $data=Tour::where('id',$id)->first();
        @unlink("upload\images\/".$data->featured_img);
        Tour::where('id',$id)->delete();
        return back()->with('success', 'Tour Deleted Successfully.');
        
    }
    public function deletegalleryimage(Request $req)
    {
        $img=Tourimage::where('id',$req->id)->first();
        @unlink("upload\images\/".$img->image);
        Tourimage::where('id',$req->id)->delete();
        echo 1;
    }

    //type

    public function all_tour_type()
    {
        $results=Tourtype::get();
        return view('admin.all_tour_type')->with('results',$results);
    }
    public function add_tour_type($id=0)
    {
        if($id>0){
            $data=Tourtype::where('id',$id)->first();
        }else{
            $data=array();
        }
        
        return view('admin.add_tour_type')->with('edit',$data);
    }
    public function submitTourtype(Request $req)
    {
        $req->validate([
            'name' => 'required|unique:Tourtypes,name,'.$req->id.',id',
        ]);
        if($req->id){
            $tour = Tourtype::find($req->id);
        }
        else{
            $tour = new Tourtype;
        }
        if($req->featured_img)
            {
                @unlink("upload\images\/".$tour->featured_img);
                $featured_img = rand().time().'.'.$req->featured_img->getClientOriginalExtension();
                $req->featured_img->move('upload/images', $featured_img);
                $tour->featured_img = $featured_img;
            }
            if($req->banner_img)
            {
                @unlink("upload\images\/".$tour->banner_img);
                $banner_img = time().'.'.$req->banner_img->getClientOriginalExtension();
                $req->banner_img->move('upload/images', $banner_img);
                $tour->banner_img = $banner_img;
            }
            $slug = str_slug($req->name, "-"); 

            $tour->name = $req->name;
            $tour->slug = $slug;
            $tour->status = (isset($req->status) and $req->status > 0) ? 1 : 0;
            $tour->description = $req->description;

            if($tour->save())
            {
                return redirect('admin/all-tour-type')->with('success', 'Tour Type '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-tour-type')->with('warning', 'failed!');
            }
    }

    public function status_tour_type($id=0)
    {
        $data=Tourtype::where('id',$id)->first();
        
        if($data->status==1){
            Tourtype::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Tourtype::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Tour Type status changed successfully.');
    }
    public function delete_tour_type($id=0)
    {
        $data=Tourtype::where('id',$id)->first();
        @unlink("upload\images\/".$data->featured_img);
        @unlink("upload\images\/".$data->banner_img);
        Tourtype::where('id',$id)->delete();
        return back()->with('success', 'Tour Type Deleted Successfully.');
        
    }
}
