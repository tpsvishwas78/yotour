<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Channel;
Use Auth;
use Hash;

class CustomerController extends Controller
{
    public function index()
    {
        $results=User::where('user_role',0)->get();
        return view('admin.all_customer')->with('results',$results);
    }
    public function add_customer($id=0)
    {
        if($id>0){
            $data=User::where('id',$id)->first();
        }else{
            $data=array();
        }
        $channels=Channel::where('status',1)->get();
        
        return view('admin.add_customer')->with('edit',$data)->with('channels',$channels);
    }
    public function submitcustomer(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'channel' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'gender' => 'required',
        ]);
        if($req->id){
            $customer = User::find($req->id);
        }
        else{
            $customer = new User;
        }
        if($req->profile_img)
            {
                @unlink("upload\profileimages\/".$customer->profile_img);
                $profile_img = rand().time().'.'.$req->profile_img->getClientOriginalExtension();
                $req->profile_img->move('upload/profileimages', $profile_img);
                $customer->profile_img = $profile_img;
            }
            
            $customer->name = $req->name;
            $customer->email = $req->email;
            $customer->phone = $req->phone;
            $customer->channel_id = $req->channel;
            $customer->age = $req->age;
            $customer->dob = $req->dob;
            $customer->gender = $req->gender;
            $customer->address = $req->address;
            $customer->user_role = 0;
            $customer->password = Hash::make($req->phone);
            
            $customer->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($customer->save())
            {
                return redirect('admin/all-customer')->with('success', 'Customer '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-customer')->with('warning', 'failed!');
            }
    }

    public function status_customer($id=0)
    {
        $data=User::where('id',$id)->first();
        
        if($data->status==1){
            User::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            User::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Customer status changed successfully.');
    }
    public function delete_customer($id=0)
    {
        $data=User::where('id',$id)->first();
        @unlink("upload\images\/".$data->profile_img);
        User::where('id',$id)->delete();
        return back()->with('success', 'Customer Deleted Successfully.');
        
    }
}
