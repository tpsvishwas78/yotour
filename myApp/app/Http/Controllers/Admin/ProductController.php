<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\ProductCategory;
use App\Models\TourLanguage;
use App\Models\Destination;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Theme;
use App\Models\Combo;
use App\Models\ComboProduct;

class ProductController extends Controller
{
    public function index()
    {
        $experience=Product::where('product_type',1)->count();
        $holiday=Product::where('product_type',2)->count();
        $combo=Combo::count();
        return view('admin.all_products')->with('experience',$experience)->with('holiday',$holiday)->with('combo',$combo);
    }
    public function all_holiday_products()
    {
        $products=Product::where('product_type',2)->get();
        return view('admin.all_holiday_products')->with('products',$products);
    }
    public function all_experience_products()
    {
        $products=Product::where('product_type',1)->get();
        return view('admin.all_experience_products')->with('products',$products);
    }
   
    public function status_exp_product($id=0)
    {
        $data=Product::where('id',$id)->first();
        
        if($data->status==1){
            Product::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Product::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Product status changed successfully.');
    }
    public function delete_exp_product($id=0)
    {
        $data=Product::where('id',$id)->first();
        Product::where('id',$id)->delete();
        return back()->with('success', 'Product Deleted Successfully.');
        
    }
    public function all_combo_product()
    {
        $results=Combo::get();
        return view('admin.all_combo_products')->with('results',$results);
    }
    public function add_combo_product($id=0)
    {
        if($id>0){
            $data=Combo::where('id',$id)->first();
        }else{
            $data=array();
        }
        $products=Product::where('status',1)->get();
        
        return view('admin.add_combo_product')->with('edit',$data)->with('products',$products);
    }
    public function submitcomboproduct(Request $req)
    {
        $req->validate([
            'product' => 'required',
            'title' => 'required',
            'original_price' => 'required',
            'combo_price' => 'required',
        ]);
        if($req->id){
            $combo = Combo::find($req->id);
        }
        else{
            $combo = new Combo;
        }
            $slug = str_slug($req->title, "-"); 
            $combo->title = $req->title;
            $combo->slug = $slug;
            $combo->original_price = $req->original_price;
            $combo->combo_price = $req->combo_price;
            $combo->description = $req->description;
            $combo->status = (isset($req->status) and $req->status > 0) ? 1 : 0;

            if($combo->save())
            {
                if($req->has('product')){
                    ComboProduct::where('combo_id',$combo->id)->delete();
                    foreach ($req->product as $key => $value) {
                        $product = new ComboProduct;
                        $product->product_id = $value;
                        $product->combo_id=$combo->id;
                        $product->save();
                    }
                }
                return redirect('admin/all-combo-product')->with('success', 'Combo '. (($req->id) ? 'Updated' : 'Created'). ' successfully.');
            }else{
                return redirect('admin/add-combo-product')->with('warning', 'failed!');
            }
    }

    public function status_combo_product($id=0)
    {
        $data=Combo::where('id',$id)->first();
        
        if($data->status==1){
            Combo::where('id',$id)->update([
                'status' => 0,
            ]);
        }else{
            Combo::where('id',$id)->update([
                'status' => 1,
            ]);
        }
        return back()->with('success', 'Combo status changed successfully.');
    }
    public function delete_combo_product($id=0)
    {
        $data=Combo::where('id',$id)->first();
        Combo::where('id',$id)->delete();
        ComboProduct::where('combo_id',$id)->delete();
        return back()->with('success', 'Combo Deleted Successfully.');
        
    }
}
