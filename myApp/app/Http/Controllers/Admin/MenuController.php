<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{
    //
    
  public function index($id=0)
  { $data=array();
      $parentmenu = Menu:: get();//Menu::where('parent_id', '=', 0)->get();
      $menulist = Menu::get();
      @$data = array();
      if($id>0){
          $data=Menu::where('id',$id)->first();
          return view('admin.menu')          
          ->with('edit',@$data)
          ->with('parentmenu', $parentmenu)
          ->with('menulist', $menulist);  
      }else{
      return view('admin.menu')
      ->with('parentmenu', $parentmenu)
      ->with('menulist', $menulist);      
      }
  }

  public function submitMenu(Request $req)
  { 
    //   $req->validate([
    //       'menu_name' => 'required',
    //   ]);
      
      Menu::create([
        'menu_name' => $req['menu_name'],
        'parent_id' => $req['parent_id'],
        'menu_order' => $req['menu_order'],
        'menu_link' => $this->createSlug($req['menu_name']),
        'menu_location' => $req['menu_location'],
      ]);
      return back()->with('success', 'Menu created successfully.');
      if(isset($req->id) and $req->id != ''){
        Menu::where('id',$req->id)->update([
              'menu_name' => $req['menu_name'],
              'parent_id' => $req['parent_id'],
              'menu_order' => $req['menu_order'],
              'menu_link' => $this->createSlug($req['menu_name']),
              'menu_location' => $req['menu_location'],
          ]);
          return redirect('admin/menu')->with('success', 'Menu updated successfully.');
      }
      
  }
  public function menudelete($id=0)
  {
    Menu::where('id',$id)->delete();
      return back()->with('success', 'Menu Deleted Successfully.');
  }
  public function menustatus($id=0)
  {
      
    if($id>0)
    {
        $slider=Menu::where('id',$id)->get()->first();
        if(isset($slider->status))
        {
         $status = ($slider->status == 'active') ? 'deactive' : 'active'; 
        $updatestatus= Menu::find($id)  ;
        $updatestatus->status = $status;
        $updatestatus->save();
                }     
    } 
   
     return back()->with('success', 'Menu status changed successfully.');
  }
//--------------- Create Unique Slug ----------------//
public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('menu_link', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('menu_link', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Menu::select('menu_link')->where('menu_link', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
