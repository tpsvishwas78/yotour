<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_paid_tour_price extends Model
{
    //

    protected $table = 'product_paid_tour_pricing';
    public $timestamps = false;
}
