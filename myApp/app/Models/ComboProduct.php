<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComboProduct extends Model
{
    public $timestamps = false;
}
