<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model {

    public function channel()
    {
        return $this->hasOne('App\Models\Channel', 'id', 'channel_id')->select('id','name')->where('status', 1);
    }

    public function tourtype()
    {
        return $this->hasOne('App\Models\Tourtype', 'id', 'tour_type_id')->select('id','name')->where('status', 1);
    }

    public function destination()
    {
        return $this->hasOne('App\Models\Destination', 'id', 'destination_id')->select('id','name')->where('status', 1);
    }

    public function tour()
    {
        return $this->hasOne('App\Models\Tour', 'id', 'tour_id')->select('id','title')->where('status', 1);
    }

    public function paytype()
    {
        return $this->hasOne('App\Models\Paytype', 'id', 'booking_amt_paytype')->select('id','name')->where('status', 1);
    }

    public function storyteller()
    {
        return $this->hasOne('App\Models\Storyteller', 'id', 'storyteller')->select('id','name')->where('status', 1);
    }

}
