<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Storyteller extends Model {

    public  function booking()
    {
        return $this->hasMany('App\Models\Booking','storyteller', 'id')->where('date', '>=', date("Y-m-d"));
    }

}
