<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTourTime extends Model
{
    protected $table = 'product_tour_time';
    public $timestamps = false;
}
