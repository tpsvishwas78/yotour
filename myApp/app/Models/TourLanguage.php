<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourLanguage extends Model
{
    public $timestamps = false;
}
