<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductInclusionsExclusions extends Model
{
    protected $table = 'product_inclusions_exclusions';
    public $timestamps = false;
}
