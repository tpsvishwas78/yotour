<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTimeDurationSlot extends Model
{
    protected $table = 'product_time_duration_slot';
    public $timestamps = false;
}
