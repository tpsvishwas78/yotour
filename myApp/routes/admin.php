<?php

Route::group(['middleware' => 'AdminLoginCheck'], function(){
    Route::get('/admin/login', 'Admin\LoginController@index')->name('admin/login');
    Route::post('/admin/loginSubmit', 'Admin\LoginController@loginSubmit')->name('admin/loginSubmit');
});

Route::group(['middleware'=>['AdminAuth', 'PreventBackHistory']], function(){ 
    Route::get('/admin/logout', 'Admin\LoginController@logout')->name('admin/logout');
    Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('admin/dashboard');
    Route::get('/admin/profile', 'Admin\DashboardController@profile')->name('admin/profile');
    Route::post('/admin/updateprofile', 'Admin\DashboardController@updateprofile')->name('admin/updateprofile');
    Route::get('/admin/changepassword', 'Admin\DashboardController@changepassword')->name('admin/changepassword');
    Route::post('/admin/changepasswordsubmit', 'Admin\DashboardController@changepasswordsubmit')->name('admin/changepasswordsubmit');

    //Start DestinationController
    //city
    Route::get('/admin/add-destination/{id?}', 'Admin\DestinationController@add_destination')->name('admin/add_destination');
    Route::post('/admin/submitdestination', 'Admin\DestinationController@submitdestination')->name('admin/submitdestination');
    Route::get('/admin/all-destination', 'Admin\DestinationController@index')->name('admin/all_destination');
    Route::get('/admin/status-destination/{id?}', 'Admin\DestinationController@status_destination')->name('admin/status_destination');
    Route::get('/admin/delete-destination/{id?}', 'Admin\DestinationController@delete_destination')->name('admin/delete_destination');
    //country
    Route::get('/admin/add-country/{id?}', 'Admin\DestinationController@add_country')->name('admin/add_country');
    Route::post('/admin/submitcountry', 'Admin\DestinationController@submitcountry')->name('admin/submitcountry');
    Route::get('/admin/all-country', 'Admin\DestinationController@all_country')->name('admin/all_country');
    Route::get('/admin/status-country/{id?}', 'Admin\DestinationController@status_country')->name('admin/status_country');
    Route::get('/admin/delete-country/{id?}', 'Admin\DestinationController@delete_country')->name('admin/delete_country');
    //End DestinationController

    //Start ChannelController
    Route::get('/admin/add-channel/{id?}', 'Admin\ChannelController@add_channel')->name('admin/add_channel');
    Route::post('/admin/submitchannel', 'Admin\ChannelController@submitchannel')->name('admin/submitchannel');
    Route::get('/admin/all-channel', 'Admin\ChannelController@index')->name('admin/all_channel');
    Route::get('/admin/status-channel/{id?}', 'Admin\ChannelController@status_channel')->name('admin/status_channel');
    Route::get('/admin/delete-channel/{id?}', 'Admin\ChannelController@delete_channel')->name('admin/delete_channel');
    //End ChannelController

    //Start CouponController
    Route::get('/admin/add-coupon/{id?}', 'Admin\CouponController@add_coupon')->name('admin/add_coupon');
    Route::post('/admin/submitcoupon', 'Admin\CouponController@submitcoupon')->name('admin/submitcoupon');
    Route::get('/admin/all-coupon', 'Admin\CouponController@index')->name('admin/all_coupon');
    Route::get('/admin/status-coupon/{id?}', 'Admin\CouponController@status_coupon')->name('admin/status_coupon');
    Route::get('/admin/delete-coupon/{id?}', 'Admin\CouponController@delete_coupon')->name('admin/delete_coupon');
    //End CouponController
    
    //Start TourController
    Route::get('/admin/add-tour/{id?}', 'Admin\TourController@add_tour')->name('admin/add_tour');
    Route::post('/admin/submittour', 'Admin\TourController@submittour')->name('admin/submittour');
    Route::get('/admin/all-tour', 'Admin\TourController@index')->name('admin/all_tour');
    Route::get('/admin/status-tour/{id?}', 'Admin\TourController@status_tour')->name('admin/status_tour');
    Route::get('/admin/delete-tour/{id?}', 'Admin\TourController@delete_tour')->name('admin/delete_tour');
    //type
    Route::get('/admin/add-tour-type/{id?}', 'Admin\TourController@add_tour_type')->name('admin/add_tour_type');
    Route::post('/admin/submittourtype', 'Admin\TourController@submittourtype')->name('admin/submittour');
    Route::get('/admin/all-tour-type', 'Admin\TourController@all_tour_type')->name('admin/all_tour_type');
    Route::get('/admin/status-tour-type/{id?}', 'Admin\TourController@status_tour_type')->name('admin/status_tour_type');
    Route::get('/admin/delete-tour-type/{id?}', 'Admin\TourController@delete_tour_type')->name('admin/delete_tour_type');
    Route::post('/admin/deletegalleryimage', 'Admin\TourController@deletegalleryimage')->name('admin/deletegalleryimage');
    
    //End TourController
    
    //Start CustomerController
    Route::get('/admin/add-customer/{id?}', 'Admin\CustomerController@add_customer')->name('admin/add_customer');
    Route::post('/admin/submitcustomer', 'Admin\CustomerController@submitcustomer')->name('admin/submitcustomer');
    Route::get('/admin/all-customer', 'Admin\CustomerController@index')->name('admin/all_customer');
    Route::get('/admin/status-customer/{id?}', 'Admin\CustomerController@status_customer')->name('admin/status_customer');
    Route::get('/admin/delete-customer/{id?}', 'Admin\CustomerController@delete_customer')->name('admin/delete_customer');
    //End CustomerController

    //Start StorytellerController
    Route::get('/admin/add-storyteller/{id?}', 'Admin\StorytellerController@add_storyteller')->name('admin/add_storyteller');
    Route::post('/admin/submitstoryteller', 'Admin\StorytellerController@submitstoryteller')->name('admin/submitstoryteller');
    Route::get('/admin/all-storyteller', 'Admin\StorytellerController@index')->name('admin/all_storyteller');
    Route::get('/admin/status-storyteller/{id?}', 'Admin\StorytellerController@status_storyteller')->name('admin/status_storyteller');
    Route::get('/admin/delete-storyteller/{id?}', 'Admin\StorytellerController@delete_storyteller')->name('admin/delete_storyteller');
    //End StorytellerController
    
    //Start BookingController
    Route::get('/admin/import-booking/{id?}', 'Admin\BookingController@import_booking')->name('admin/import_booking');
    Route::post('/admin/submitimportbooking', 'Admin\BookingController@submitimportbooking')->name('admin/submitimportbooking');

    Route::get('/admin/add-booking/{id?}', 'Admin\BookingController@add_booking')->name('admin/add_booking');
    Route::post('/admin/submitbooking', 'Admin\BookingController@submitbooking')->name('admin/submitbooking');
    Route::get('/admin/all-booking', 'Admin\BookingController@index')->name('admin/all_booking');
    Route::get('/admin/status-booking/{id?}', 'Admin\BookingController@status_booking')->name('admin/status_booking');
    Route::get('/admin/delete-booking/{id?}', 'Admin\BookingController@delete_booking')->name('admin/delete_booking');
    Route::post('/admin/getcustomers', 'Admin\BookingController@getcustomers')->name('admin/getcustomers');
    Route::post('/admin/gettours', 'Admin\BookingController@gettours')->name('admin/gettours');
    Route::get('/admin/upcoming-booking', 'Admin\BookingController@upcoming_booking')->name('admin/upcoming_booking');
    Route::get('/admin/unallocated-booking', 'Admin\BookingController@unallocated_booking')->name('admin/unallocated_booking');
    Route::get('/admin/past-booking', 'Admin\BookingController@past_booking')->name('admin/past_booking');
    Route::get('/admin/allocate/{id?}', 'Admin\BookingController@allocate')->name('admin/allocate');
    Route::post('/admin/allocatesubmit', 'Admin\BookingController@allocatesubmit')->name('admin/allocatesubmit');
    Route::post('/admin/cancelbooking', 'Admin\BookingController@cancelbooking')->name('admin/cancelbooking');
    Route::post('/admin/changebookingstatus', 'Admin\BookingController@changebookingstatus')->name('admin/changebookingstatus');
    
    //paytype
    Route::get('/admin/add-paytype/{id?}', 'Admin\BookingController@add_paytype')->name('admin/add_paytype');
    Route::post('/admin/submitpaytype', 'Admin\BookingController@submitpaytype')->name('admin/submitpaytype');
    Route::get('/admin/all-paytype', 'Admin\BookingController@all_paytype')->name('admin/all_paytype');
    Route::get('/admin/status-paytype/{id?}', 'Admin\BookingController@status_paytype')->name('admin/status_paytype');
    Route::get('/admin/delete-paytype/{id?}', 'Admin\BookingController@delete_paytype')->name('admin/delete_paytype');

    //all-themes
    Route::get('/admin/add-theme/{id?}', 'Admin\BookingController@add_theme')->name('admin/add_theme');
    Route::post('/admin/submittheme', 'Admin\BookingController@submittheme')->name('admin/submittheme');
    Route::get('/admin/all-themes', 'Admin\BookingController@all_theme')->name('admin/all_theme');
    Route::get('/admin/status-theme/{id?}', 'Admin\BookingController@status_theme')->name('admin/status_theme');
    Route::get('/admin/delete-theme/{id?}', 'Admin\BookingController@delete_theme')->name('admin/delete_theme');

    //languages
    Route::get('/admin/add-language/{id?}', 'Admin\BookingController@add_language')->name('admin/add_language');
    Route::post('/admin/submitlanguage', 'Admin\BookingController@submitlanguage')->name('admin/submitlanguage');
    Route::get('/admin/all-languages', 'Admin\BookingController@all_language')->name('admin/all_language');
    Route::get('/admin/status-language/{id?}', 'Admin\BookingController@status_language')->name('admin/status_language');
    Route::get('/admin/delete-language/{id?}', 'Admin\BookingController@delete_language')->name('admin/delete_language');

    //product category
    Route::get('/admin/add-category/{id?}', 'Admin\BookingController@add_category')->name('admin/add_category');
    Route::post('/admin/submitcategory', 'Admin\BookingController@submitcategory')->name('admin/submitcategory');
    Route::get('/admin/all-category', 'Admin\BookingController@all_category')->name('admin/all_category');
    Route::get('/admin/status-category/{id?}', 'Admin\BookingController@status_category')->name('admin/status_category');
    Route::get('/admin/delete-category/{id?}', 'Admin\BookingController@delete_category')->name('admin/delete_category');
    
    //page menu
    Route::get('/admin/menu/{id?}','Admin\MenuController@index');   
    Route::post('/admin/submitMenu/', 'Admin\MenuController@submitMenu')->name('admin/submitMenu');
    Route::get('/admin/menu/menustatus/{id?}', 'Admin\MenuController@menustatus')->name('admin/menustatus');
    Route::get('/admin/menu/menudelete/{id?}', 'Admin\MenuController@menudelete')->name('admin/menudelete');
    //End BookingController
    
    //Start ExperienceController
    Route::get('/admin/experience/add-tour-activity/{id?}', 'Admin\ExperienceController@add_tour_activity')->name('admin/add_tour_activity');
    Route::post('/admin/submittouractivity', 'Admin\ExperienceController@submittouractivity')->name('admin/submittouractivity');

    Route::get('/admin/experience/add-tickets/{id?}', 'Admin\ExperienceController@add_tickets')->name('admin/add_tickets');
    Route::post('/admin/submittickets', 'Admin\ExperienceController@submittickets')->name('admin/submittickets');

    Route::get('/admin/experience/add-transport-rental/{id?}', 'Admin\ExperienceController@add_transport_rental')->name('admin/add_transport_rental');
    Route::post('/admin/submittransportrental', 'Admin\ExperienceController@submittransportrental')->name('admin/submittransportrental');
    Route::post('/admin/getrentcategory', 'Admin\ExperienceController@getrentcategory')->name('admin/getrentcategory');

    Route::get('/admin/create-holiday/{id?}', 'Admin\ExperienceController@create_holiday')->name('admin/create_holiday');
    Route::post('/admin/submitholiday', 'Admin\ExperienceController@submitholiday')->name('admin/submitholiday');
    
    
    Route::post('/admin/getcitybycountry', 'Admin\ExperienceController@getcitybycountry')->name('admin/getcitybycountry');
    Route::post('/admin/getinclusionsubcate', 'Admin\ExperienceController@getinclusionsubcate')->name('admin/getinclusionsubcate');  
    Route::post('/admin/deleteproductgalleryimage', 'Admin\ExperienceController@deleteproductgalleryimage')->name('admin/deleteproductgalleryimage');
    Route::post('/admin/removeitinary', 'Admin\ExperienceController@removeitinary')->name('admin/removeitinary');
    Route::post('/admin/removetourtime', 'Admin\ExperienceController@removetourtime')->name('admin/removetourtime');

    
    
    Route::get('/admin/allproducts', 'Admin\ProductController@index')->name('admin/allproducts');
    Route::get('/admin/all-experience-products/{id?}', 'Admin\ProductController@all_experience_products')->name('admin/all_experience_products');
    Route::get('/admin/all-holiday-products/{id?}', 'Admin\ProductController@all_holiday_products')->name('admin/all_holiday_products');
    

    Route::get('/admin/status-exp-product/{id?}', 'Admin\ProductController@status_exp_product')->name('admin/status_exp_product');
    Route::get('/admin/delete-exp-product/{id?}', 'Admin\ProductController@delete_exp_product')->name('admin/delete_exp_product');
    

    //ReviewController
    Route::get('/admin/add-admin-review/{id?}', 'Admin\ReviewController@add_admin_review')->name('admin/add_admin_review');
    Route::post('/admin/submitadminreview', 'Admin\ReviewController@submitadminreview')->name('admin/submitadminreview');
    Route::get('/admin/all-admin-review', 'Admin\ReviewController@index')->name('admin/index');
    Route::get('/admin/all-customer-review', 'Admin\ReviewController@all_customer_review')->name('admin/all_customer_review');
    Route::get('/admin/status-review/{id?}', 'Admin\ReviewController@status_review')->name('admin/status_review');
    Route::get('/admin/delete-review/{id?}', 'Admin\ReviewController@delete_review')->name('admin/delete_review');
    //End ReviewController

    //ProductController
    Route::get('/admin/add-combo-product/{id?}', 'Admin\ProductController@add_combo_product')->name('admin/add_combo_product');
    Route::post('/admin/submitcomboproduct', 'Admin\ProductController@submitcomboproduct')->name('admin/submitcomboproduct');
    Route::get('/admin/all-combo-product', 'Admin\ProductController@all_combo_product')->name('admin/all_combo_product');
    Route::get('/admin/status-combo-product/{id?}', 'Admin\ProductController@status_combo_product')->name('admin/status_combo_product');
    Route::get('/admin/delete-combo-product/{id?}', 'Admin\ProductController@delete_combo_product')->name('admin/delete_combo_product');
    //End ProductController
});






?>