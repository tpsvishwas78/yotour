<?php

Route::post('login', 'storyteller\UserController@login');
Route::get('getbooking/{accessToken?}', 'storyteller\BookingController@allbooking');
Route::get('getavailability/{accessToken?}', 'storyteller\AvailabilityController@myavailability');


//Route::prefix('booking')->group(function () {
//   Route::get('/{accessToken}', 'storyteller\BookingController@allbooking');
//});