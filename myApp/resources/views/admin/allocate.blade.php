@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Booking Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2>Allocate Booking</h2>
</div>


<table class="table table-bordered table-striped table-vcenter">
    <thead>
            <tr>
                    <th class="text-center">&nbsp;</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Availability</th>
                    <th class="text-center">No. Of People</th>
                </tr>
    </thead>
    <tbody>
            <?php
            @$booking=\DB::table('bookings')->where('id',Request::segment(3))->first();
            ?>
       @foreach ($storytellers as $storyteller)
          
       
        <tr>
            <td class="text-center">
                    @csrf
                <input @if (@$booking->storyteller==$storyteller->id)
                    checked
                @endif type="radio" name="storyteller" value="{{ $storyteller->id }}">
                <input type="hidden" name="booking_id" value="{{ Request::segment(3) }}">
            </td>
            <td class="text-center"><strong>{{ $storyteller->name }}</strong></td>
            <td class="text-center"><span class="label label-success">&nbsp;&nbsp;&nbsp;</span></td>
            <td class="text-center">5</td>
        </tr>

        @endforeach
        
        
    </tbody>
</table>    
<div class="form-group form-actions" style="margin-top:50px;">
            <button type="submit" value="allocate" id="allocateBtn" name="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> ALLOCATE</button>
    </div>     


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
<script>
$(document).on('click', '#allocateBtn', function(e)
    {
        var storyteller=$('input[name=storyteller]:checked').val();
        var id=$('input[name=booking_id]').val();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/allocatesubmit",
                 method:"POST",
                 data:{id:id,storyteller:storyteller,_token:_token},
                 success:function(data){
                    window.location.href = "{{ url('') }}/admin/all-booking";
                 }
                });
        
    });
</script>
@endsection