@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-log_book"></i>Product Management
</h1>
</div>
</div>


<div class="row text-center">
        <div class="col-sm-6 col-lg-4">
                <a href="{{ url('') }}/admin/all-experience-products" class="widget widget-hover-effect2">
                    <div class="widget-extra upcoming-booking">
                        <h4 class="widget-content-light"><strong>Experience </strong> Products</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 animation-expandOpen">{{ @$experience }}</span></div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-4">
                    <a href="{{ url('') }}/admin/all-holiday-products" class="widget widget-hover-effect2">
                        <div class="widget-extra unallocated-booking">
                            <h4 class="widget-content-light"><strong>Holiday </strong> Products</h4>
                        </div>
                        <div class="widget-extra-full"><span class="h2 animation-expandOpen">{{ @$holiday }}</span></div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                        <a href="{{ url('') }}/admin/all-combo-product" class="widget widget-hover-effect2">
                            <div class="widget-extra past-booking">
                                <h4 class="widget-content-light"><strong>Combos </strong> Producs</h4>
                            </div>
                            <div class="widget-extra-full"><span class="h2 animation-expandOpen">{{ @$combo }}</span></div>
                        </a>
                    </div>

                   
</div>


</div>

@endsection