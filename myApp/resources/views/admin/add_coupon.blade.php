@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Coupons Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Coupon</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submitcoupon')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Coupon Name <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                <input type="text" id="name" name="name" class="form-control" value="{{ @$edit->name }}">
                                
                            @if ($errors->has('name'))
                                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div> 
                <div class="form-group">
                    <label class="col-md-4 control-label" for="coupon_code">Coupon Code <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="coupon_code" name="coupon_code" class="form-control" value="{{ @$edit->coupon_code ? @$edit->coupon_code : substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 6) }}">
                        @if ($errors->has('coupon_code'))
                                            <span class="text-danger">{{ $errors->first('coupon_code') }}</span>
                        @endif
                    </div>
                </div>                     
                <div class="form-group">
                        <label class="col-md-4 control-label" for="start_date">Start Date <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <input type="text" id="example-datepicker5" name="start_date" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$edit->start_date }}">
                        @if ($errors->has('start_date'))
                            <span class="text-danger">{{ $errors->first('start_date') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                        <label class="col-md-4 control-label" for="expire_date">Expire Date <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <input type="text" id="example-datepicker5" name="expire_date" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd"  value="{{ @$edit->expire_date }}">
                        @if ($errors->has('expire_date'))
                            <span class="text-danger">{{ $errors->first('expire_date') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                        <label class="col-md-4 control-label" for="min_amount">Discount Type <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                
                                <select name="type" id="type" class="form-control">
                                    <option value="">Select</option>
                                    <option @if (@$edit->type==1)
                                        selected
                                    @endif value="1">Percentage</option>
                                    <option @if (@$edit->type==2)
                                            selected
                                        @endif value="2">Amount</option>
                                </select>
                                
                            @if ($errors->has('type'))
                                                <span class="text-danger">{{ $errors->first('type') }}</span>
                            @endif
                        </div>
                    </div>  
                <div class="form-group">
                        <label class="col-md-4 control-label" for="min_amount">Minimum Amount <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                <input type="text" id="min_amount" name="min_amount" class="form-control" value="{{ @$edit->min_amount }}">
                                
                            @if ($errors->has('min_amount'))
                                                <span class="text-danger">{{ $errors->first('min_amount') }}</span>
                            @endif
                            <span class="help-block">Minimum amount required for apply discount</span>
                        </div>
                </div>
                <div class="form-group">
                        <label class="col-md-4 control-label" for="discount_amt_per">Discount (Amount/Percentage) <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                <input type="text" id="discount_amt_per" name="discount_amt_per" class="form-control" value="{{ @$edit->discount_amt_per }}">
                                
                            @if ($errors->has('discount_amt_per'))
                                                <span class="text-danger">{{ $errors->first('discount_amt_per') }}</span>
                            @endif
                        </div>
                </div> 
                <div class="form-group">
                        <label class="col-md-4 control-label" for="max_discount_amount">Maximum Discount Amount</label>
                        <div class="col-md-6">
                                <input type="text" id="max_discount_amount" name="max_discount_amount" class="form-control" value="{{ @$edit->max_discount_amount }}">
                                
                            
                        </div>
                </div>  
        
        
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Description </label>
            <div class="col-md-8">
                <textarea id="textarea-ckeditor" name="description" class="ckeditor">{{ @$edit->description }}</textarea>
                    
            </div>
        </div>
        
        <div class="form-group">
                <label class="col-md-4 control-label" for="val_username">Status</label>
                <div class="col-md-6">
                    <label class="switch switch-primary">
                        <input type="checkbox" @if (@$edit->status==1)
                        checked                        
                        @endif  name="status" value="1"><span></span>
                    </label>
                        
                </div>
            </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection