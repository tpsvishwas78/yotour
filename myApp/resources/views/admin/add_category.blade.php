@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Setting
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Category</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submitcategory')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Select Type <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <select name="type" id="type" class="form-control select-select2">
                                <option value="">Select</option>
                                <option @if (@$edit->type==1)
                                    selected
                                @endif value="1">Tour</option>
                                <option @if (@$edit->type==2)
                                    selected
                                @endif value="2">Activity</option>
                                <option @if (@$edit->type==3)
                                    selected
                                @endif value="3">Tickets</option>
                                <option @if (@$edit->type==4)
                                    selected
                                @endif value="4">Transport </option>
                                <option @if (@$edit->type==5)
                                    selected
                                @endif value="5">Rental </option>
                                <option @if (@$edit->type==6)
                                    selected
                                @endif value="6">Services </option>
                                <option @if (@$edit->type==7)
                                    selected
                                @endif value="7">Inclusions/Exclusions </option>
                               
                                <option @if (@$edit->type==8)
                                    selected
                                @endif value="9">Holiday</option>
                            </select>
                        @if ($errors->has('type'))
                            <span class="text-danger">{{ $errors->first('type') }}</span>
                        @endif
                    </div>
            </div>  
            
            <div class="form-group">
                <label class="col-md-4 control-label" for="parent">Select Category</label>
                <div class="col-md-6">
                        <select name="parent" id="parent" class="form-control select-select2">
                            <option value="">Select</option>
                            @foreach ($results as $result)
                            <option @if (@$edit->parent==$result->id)
                                selected
                            @endif value="{{ $result->id }}">{{ $result->name }}</option>
                            @endforeach
                           
                        </select>
                    
                </div>
        </div>  
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="name" name="name" class="form-control" value="{{ @$edit->name }}">
                            
                        @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                    </div>
                </div>
                                   
        
        
       
        <div class="form-group">
                <label class="col-md-4 control-label" for="val_username">Status </label>
                <div class="col-md-6">
                    <label class="switch switch-primary">
                        <input type="checkbox" @if (@$edit->status==1)
                        checked                        
                        @endif  name="status" value="1"><span></span>
                    </label>
                        
                </div>
            </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection