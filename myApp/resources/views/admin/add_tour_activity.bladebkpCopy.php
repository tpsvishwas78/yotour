@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Product Management
</h1>
</div>
</div>
<style>
#pickupmap {
    height: 300px;
    display: none;
}
#dropmap {
    height: 300px;
    display: none;
}
.btn-default.active,.btn-default.active.focus,.btn-default.active:hover{
    background-color: #00BCD4;
    border-color: #00BCD4;
    color: #fff;
}

.btn-default,.btn-default:focus{
    background-color: #eaedf1;
    border-color: #eaedf1;
    color: #000;
}
.btn-group .btn+.btn{
    margin-left: 5px;
}
</style>
<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submittouractivity')}}" enctype="multipart/form-data" class="form-horizontal form-bordered-1">
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Tour/Activity</h2>
    <div class="pull-right" style="margin: 1px 10px;">
        <button type="submit"  data-toggle="tooltip"  class="btn btn-primary" data-original-title="Save"><i class="fa fa-save"></i> Save</button>
       
</div>
</div>
<!-- END Form Validation Example Title -->

    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="block-title">
                <ul class="nav nav-tabs" data-toggle="tabs">
                    <li class="active"><a href="#tab1">General</a></li>
                    <li class=""><a href="#tab2">Itinerary</a></li>
                    <li class=""><a href="#tab3">Inclusions</a></li>
                    <li class=""><a href="#tab4">Duration & Time Slot</a></li>
                    <li class=""><a href="#tab6">Cut Off</a></li>
                    <li class=""><a href="#tab7">Language</a></li>
                    <li class=""><a href="#tab8">Accessibility</a></li>
                    <li class=""><a href="#tab9">Pricing</a></li>
                    <li class=""><a href="#tab10">Policy & Info</a></li>
                </ul>
            </div>
                 <!-- Tabs Content -->
                 <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Destination </label>
                            <div class="col-md-3">
                                    <select name="country" id="country" class="form-control select-select2" style="width:100% !important;">
                                        <option value="">Select Country</option>
                                        @foreach ($countries as $country)
                                        <option @if (@$edit->country==$country->id)
                                            selected
                                        @endif value="{{ $country->id }}">{{ $country->name }}</option> 
                                        @endforeach
                                        
                                    </select>
                                    
                                @if ($errors->has('country'))
                                      <span class="text-danger">{{ $errors->first('country') }}</span>
                                 @endif
                            </div>
        <?php
        @$cities=\DB::table('destinations')->where('country_id',@$edit->country)->get();
        ?>
                            <div class="col-md-3">
                                <select name="city" id="city" class="form-control select-select2" style="width:100% !important;">
                                    <option value="">Select City</option>
                                    @foreach ($cities as $city)
                                    <option @if ($edit->city==$city->id)
                                        selected
                                    @endif value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                    
                                </select>
                                
                            @if ($errors->has('city'))
                                <span class="text-danger">{{ $errors->first('city') }}</span>
                             @endif
                        </div>
                        </div> 
    
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="product_name">Product Name </label>
                            <div class="col-md-6">
                                    <input type="text" id="product_name" name="product_name" class="form-control" value="{{ @$edit->product_name }}">
                                    
                                @if ($errors->has('product_name'))
                                    <span class="text-danger">{{ $errors->first('product_name') }}</span>
                                @endif
                            </div>
                        </div> 
    
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="tour_category">Select Category</label>
                                
                                <div class="col-md-3">
                                    <span>For Tour</span>
                                        <select name="tour_category" id="tour_category" class="form-control select-select2" style="width:100% !important;">
                                            <option value="">Select</option>
                                            @foreach ($tourcats as $tourcat)
                                        <option @if (@$edit->tour_category==$tourcat->id)
                                            selected
                                        @endif value="{{ $tourcat->id }}">{{ $tourcat->name }}</option> 
                                        @endforeach
                                           
                                        </select>
                                    @if ($errors->has('tour_category'))
                                        <span class="text-danger">{{ $errors->first('tour_category') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <span>For Activity</span>
                                    <select name="activity_category" id="activity_category" class="form-control select-select2" style="width:100% !important;">
                                        <option value="">Select</option>
                                        @foreach ($activitycats as $activitycat)
                                        <option @if (@$edit->activity_category==$activitycat->id)
                                            selected
                                        @endif value="{{ $activitycat->id }}">{{ $activitycat->name }}</option> 
                                        @endforeach
                                       
                                    </select>
                                @if ($errors->has('activity_category'))
                                    <span class="text-danger">{{ $errors->first('activity_category') }}</span>
                                @endif
                            </div>
                        </div>  
    
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Select Theme</label>
                            <div class="col-md-6">
                                    <select name="theme" id="theme" class="form-control select-select2" style="width:100% !important;">
                                        <option value="">Select</option>
                                       @foreach ($themes as $theme)
                                       <option @if (@$edit->theme==$theme->id)
                                           selected
                                       @endif value="{{ $theme->id }}"> {{ $theme->name }} </option>  
                                       @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="short_description">Short Description </label>
                            <div class="col-md-6">
                                
                                 <textarea name="short_description" id="" cols="30" rows="4" style="width:100%;">{{ @$edit->short_description }}</textarea>   
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="short_description">Highlights of Tour</label>
                            <div class="col-md-6">
                                <input type="text" id="highlight_tour" name="highlight_tour" class="form-control" value="{{ @$edit->highlight_tour }}">
                                
                            @if ($errors->has('highlight_tour'))
                                <span class="text-danger">{{ $errors->first('highlight_tour') }}</span>
                            @endif
                        </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="product_description">Product Full Description/What you can expect	 </label>
                            <div class="col-md-8">
                                <textarea id="textarea-ckeditor" name="product_description" class="ckeditor">{{ @$edit->product_description }}</textarea>
                            </div>
                        </div>
                        <?php
                        @$images=\DB::table('product_images')->where('product_id',@$edit->id)->get();
                        ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="gallary">Select Photos & Video </label>
                            <div class="col-md-6">
                                <div class="controls">
                                    <div class="entry input-group col-xs-3">
                                      <input class="btn btn-default" name="gallary[]" type="file">
                                      <span class="input-group-btn">
                                    <button class="btn btn-success btn-add" type="button">
                                            <span class="gi gi-plus"></span>
                                      </button>
                                      </span>
                                    </div>
                                </div>
                                @if (@$images)
                @foreach (@$images as $image)
                        <p>
                        <img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
                        <a href="javascript:void(0);"><i id="{{ $image->id }}" class="gi gi-delete removeimg"></i></a> 
                        </p>
                @endforeach
                @endif
                              <input  style="margin-top: 20px;" type="text" name="video_link" placeholder="Youtube video link" class="form-control" value="{{ @$edit->video_link }}">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="featured_img">Featured Image </label>
                            <div class="col-md-6">
                                <input type="file" id="featured_img" name="featured_img" class="form-control">
                                @if (@$edit->featured_img)
                                <p>
                                <img width="100" src="{{ url('') }}/upload/images/{{ @$edit->featured_img }}" alt="">
                                </p> 
                                @endif
                            </div>
                        </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="pickup">Meeting Point, Pick up & Drop Off </label>
                        <div class="col-md-6">
                            <input type="text" id="pickup" name="pickup" class="form-control" value="{{ @$edit->pickup }}" placeholder="Pick Up Location">
                            <input type="hidden" name="pickup_latitude" id="pickup_latitude" value="{{ @$edit->pickup_latitude }}">
                            <input type="hidden" name="pickup_longitude" id="pickup_longitude" value="{{ @$edit->pickup_longitude }}">
                            <div class="ground-map">
                                <div id="pickupmap"></div>
                            </div>
                        </div>
                        <label class="col-md-4 control-label" for="dropoff"></label>
                        <div class="col-md-6" style="margin-top: 30px;">
                            <input type="text" id="dropoff" name="dropoff" class="form-control" value="{{ @$edit->dropoff }}" placeholder="Drop Off Location">
                            <input type="hidden" name="drop_latitude" id="drop_latitude" value="{{ @$edit->drop_latitude }}">
                            <input type="hidden" name="drop_longitude" id="drop_longitude" value="{{ @$edit->drop_longitude }}">
                            <div class="ground-map">
                                <div id="dropmap"></div>
                            </div>
                        </div>
                    </div>    
                    </div>
                    <div class="tab-pane " id="tab2">
    
                        <?php
                        @$itineraries=\DB::table('product_itinerary')->where('product_id',@$edit->id)->get();
                        ?>
<!-- Start Itinerary Block --> 
        <div class="block">           
            <div class="block-title">
                <h2>Itinerary</h2>
            </div>
        <fieldset>
            <div class="itinary-controls">
                <?php
            $mykey=0;
                ?>
            @foreach (@$itineraries as $key => $itinerary)
            <?php $mykey=$key;
            if($mykey>0){
                $mykey++;
            }
            ?>
            <div class="itinary-entry" style="border-bottom: 1px solid #ddd;
            margin-bottom: 20px;">
            <div class="form-group">
                <label class="col-md-4 control-label">Name of the place in Itinerary</label>
                <div class="col-md-6">
                        <input type="text" id="itinerary_title" name="itinerary_title[]" class="form-control" value="{{ @$itinerary->itinerary_title }}">
                </div>
                <button class="btn btn-remove-itinary btn-danger" type="button" id="{{ @$itinerary->id }}">
                    <span class="gi gi-minus"></span>
              </button>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Photo </label>
                <div class="col-md-6">
                    <input type="file" id="itinerary_img" name="itinerary_img[]" class="form-control">
                    @if (@$itinerary->itinerary_img)
                    <p>
                    <img width="100" src="{{ url('') }}/upload/images/{{ @$itinerary->itinerary_img }}" alt="">
                    </p> 
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Type of Place</label>
                <div class="col-md-6">
                        <input type="text" id="example-tags" name="place_type[]" class="form-control input-tags1" placeholder="Monument,Museum..etc" value="{{ @$itinerary->place_type }}">
                    <span>Monument,Museum..etc</span>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-4 control-label">Describe what travelers will see and do here if they book the experience:</label>
                <div class="col-md-6">
                        <input type="text" id="describe_experience" name="describe_experience[]" class="form-control" value="{{ @$itinerary->describe_experience }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">How much time do travelers typically spend here?
                </label>
                <div class="col-md-6">
                    <?php @$spenttimeArray = unserialize(@$itinerary->spent_time);?>
                    <label class="checkbox-inline">
                        <input @if (@in_array('Pass by without stopping',@$spenttimeArray)) 
                         checked 
                         @endif type="checkbox"  name="spent_time[{{ $key }}][0]" value="Pass by without stopping">Pass by without stopping
                    </label>
                    <label class="checkbox-inline">
                        <input @if (@in_array('Minutes',@$spenttimeArray)) checked @endif type="checkbox" name="spent_time[{{ $key }}][1]" value="Minutes">Minutes
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Is admission to this place included in the price of your tour?
                </label>
                <div class="col-md-6">
                    <label class="radio-inline">
                        <input type="radio" @if (@$itinerary->included_price_tour=='Yes')
                            checked
                        @endif name="included_price_tour[{{ $key }}]" value="Yes">Yes
                    </label>
                    <label class="radio-inline">
                        <input @if (@$itinerary->included_price_tour=='No')
                        checked
                    @endif type="radio" name="included_price_tour[{{ $key }}]" value="No">No
                    </label>
                    <label class="radio-inline">
                        <input @if (@$itinerary->included_price_tour=='N/A (Admission is free)')
                        checked
                    @endif type="radio"  name="included_price_tour[{{ $key }}]" value="N/A (Admission is free)">N/A (Admission is free)
                    </label>
                </div>
            </div> 
        </div>
    <input type="hidden" name="itinerary_id[{{ $key }}]" value="{{ @$itinerary->id }}">
        @endforeach

        <div class="itinary-entry" style="border-bottom: 1px solid #ddd;
            margin-bottom: 20px;">
            <div class="form-group">
                <label class="col-md-4 control-label">Name of the place in Itinerary</label>
                <div class="col-md-6">
                        <input type="text" id="itinerary_title" name="itinerary_title[]" class="form-control" >
                </div>
                <button class="btn btn-success btn-add-itinary" type="button">
                    <span class="gi gi-plus"></span>
              </button>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Photo </label>
                <div class="col-md-6">
                    <input type="file" id="itinerary_img" name="itinerary_img[]" class="form-control">
                   
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Type of Place</label>
                <div class="col-md-6">
                        <input type="text" id="example-tags" name="place_type[]" class="form-control input-tags1" placeholder="Monument,Museum..etc">
                    <span>Monument,Museum..etc</span>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-4 control-label">Describe what travelers will see and do here if they book the experience:</label>
                <div class="col-md-6">
                        <input type="text" id="describe_experience" name="describe_experience[]" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">How much time do travelers typically spend here?
                </label>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                        <input type="checkbox"  name="spent_time[{{ $mykey }}][0]" value="Pass by without stopping">Pass by without stopping
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="spent_time[{{ $mykey }}][1]" value="Minutes">Minutes
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Is admission to this place included in the price of your tour?
                </label>
                <div class="col-md-6">
                    <label class="radio-inline">
                        <input type="radio" name="included_price_tour[{{ $mykey }}]" value="Yes">Yes
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="included_price_tour[{{ $mykey }}]" value="No">No
                    </label>
                    <label class="radio-inline">
                        <input type="radio"  name="included_price_tour[{{ $mykey }}]" value="N/A (Admission is free)">N/A (Admission is free)
                    </label>
                </div>
            </div> 
        </div>
    </div>
    <input type="hidden" name="itinerary_id[]" value="">
        </fieldset>

        </div>
        <!-- END Itinerary Block -->  
                    </div>
                    <div class="tab-pane " id="tab3">
                        <?php
                        @$inclusions=\DB::table('product_inclusions_exclusions')->where('product_id',@$edit->id)->first();
                        ?>                      
<!-- Start Inclusions & Exclusions Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Inclusions & Exclusions</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="add_inclusion">Add an inclusion</label>
        <div class="col-md-8">
            <label class="radio-inline" for="for_basic_tour">
                <input type="radio" @if (@$inclusions->inclusion_package=='For Basic Tour')
                checked
            @endif id="for_basic_tour" name="inclusion_package" value="For Basic Tour">For Basic Tour
            </label>
            <label class="radio-inline" for="for_budget_tour">
                <input type="radio" @if (@$inclusions->inclusion_package=='For Budget Tour')
                checked
            @endif id="for_budget_tour" name="inclusion_package" value="For Budget Tour">For Budget Tour
            </label>
            <label class="radio-inline" for="for_premium_tour">
                <input @if (@$inclusions->inclusion_package=='For Premium Tour')
                checked
            @endif type="radio" id="for_premium_tour" name="inclusion_package" value="For Premium Tour">For Premium Tour
            </label>
            <label class="radio-inline" for="for_vip_tour">
                <input @if (@$inclusions->inclusion_package=='For VIP Tour')
                checked
            @endif type="radio" id="for_vip_tour" name="inclusion_package" value="For VIP Tour">For VIP Tour
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="inclusion_category">Category</label>
        <div class="col-md-6">
                <select name="inclusion_category" id="inclusion_category" class="form-control select-select2" style="width:100% !important;">
                    <option value="">Select</option>
                    @foreach ($inclusionscats as $inclusionscat)
                        <option @if (@$inclusions->inclusion_category==$inclusionscat->id)
                            selected
                        @endif value="{{ $inclusionscat->id }}">{{ $inclusionscat->name }}</option> 
                    @endforeach
                </select>
        </div>
    </div>
    <?php
        @$inclusionsubcategorys=\DB::table('product_categories')->where('parent',$inclusions->inclusion_category)->get();
    ?>
    <div class="form-group">
        <label class="col-md-4 control-label" for="inclusion_sub_category">Sub Category</label>
        <div class="col-md-6">
                <select name="inclusion_sub_category[]" id="inclusion_sub_category" class="form-control select-select2" multiple style="width:100% !important;">
                    <?php @$inclusionsubArray = unserialize(@$inclusions->inclusion_sub_category);?>

                    @foreach ($inclusionsubcategorys as $inclusionsubcategory)
                        <option @if (@in_array($inclusionsubcategory->id,@$inclusionsubArray))
                            selected
                        @endif value="{{ $inclusionsubcategory->id }}">{{ $inclusionsubcategory->name }}</option>
                    @endforeach
                   
                </select>
           
        </div>
    </div> 
    <div class="form-group">
        <label class="col-md-4 control-label" for="inclusion_description">Description </label>
        <div class="col-md-6">
                <input type="text" id="inclusion_description" name="inclusion_description" class="form-control" value="{{ @$inclusions->inclusion_description }}">
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label class="col-md-4 control-label" for="exclusion_for_basic_tour">Add an exclusion</label>
        <div class="col-md-8">
            <label class="radio-inline" for="exclusion_for_basic_tour">
                <input @if (@$inclusions->exclusion_package=='For Basic Tour')
                    checked
                @endif type="radio" id="exclusion_for_basic_tour" name="exclusion_package" value="For Basic Tour">For Basic Tour
            </label>
            <label class="radio-inline" for="exclusion_for_budget_tour">
                <input type="radio" @if (@$inclusions->exclusion_package=='For Budget Tour')
                checked
            @endif id="exclusion_for_budget_tour" name="exclusion_package" value="For Budget Tour">For Budget Tour
            </label>
            <label class="radio-inline" for="exclusion_for_premium_tour">
                <input @if (@$inclusions->exclusion_package=='For Premium Tour')
                checked
            @endif type="radio" id="exclusion_for_premium_tour" name="exclusion_package" value="For Premium Tour">For Premium Tour
            </label>
            <label class="radio-inline" for="exclusion_for_vip_tour">
                <input @if (@$inclusions->exclusion_package=='For VIP Tour')
                checked
            @endif type="radio" id="exclusion_for_vip_tour" name="exclusion_package" value="For VIP Tour">For VIP Tour
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="exclusion_category">Category</label>
        <div class="col-md-6">
                <select name="exclusion_category" id="exclusion_category" class="form-control select-select2" style="width:100% !important;">
                    <option value="">Select</option>
                    @foreach ($inclusionscats as $inclusionscat)
                    <option @if (@$inclusions->exclusion_category==$inclusionscat->id)
                        selected
                    @endif value="{{ $inclusionscat->id }}">{{ $inclusionscat->name }}</option> 
                    @endforeach
                </select>
        </div>
    </div> 
    <?php
        @$exclusionsubcategorys=\DB::table('product_categories')->where('parent',$inclusions->exclusion_category)->get();
    ?>
    <div class="form-group">
        <label class="col-md-4 control-label" for="exclusion_sub_category">Sub Category</label>
        <div class="col-md-6">
                <select name="exclusion_sub_category[]" id="exclusion_sub_category" class="form-control select-select2" multiple style="width:100% !important;">
                    <?php @$exclusionsubArray = unserialize(@$inclusions->exclusion_sub_category);?>

                    @foreach ($exclusionsubcategorys as $exclusionsubcategory)
                    <option @if (@in_array($exclusionsubcategory->id,@$exclusionsubArray))
                        selected
                    @endif value="{{ $exclusionsubcategory->id }}">{{ $exclusionsubcategory->name }}</option>
                @endforeach
                </select>
           
        </div>
    </div>  
    <div class="form-group">
        <label class="col-md-4 control-label" for="exclusion_description">Description </label>
        <div class="col-md-6">
                <input type="text" id="exclusion_description" name="exclusion_description" class="form-control" value="{{ @$inclusions->exclusion_description }}">
        </div>
    </div>
<input type="hidden" name="inclusion_id" value="{{ @$inclusions->id }}">
    
</fieldset>

</div>
<!-- END Inclusions & Exclusions Block -->  
                    </div>
                    <div class="tab-pane " id="tab4">
                        <?php
                        @$duration=\DB::table('product_time_duration_slot')->where('product_id',@$edit->id)->first();
                        ?>                              
<!-- Start Tour Duration Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Tour Duration</h2>
    </div>
<fieldset>
    <input type="hidden" name="tour_duration_id" value="{{ @$duration->id }}">
        <div class="form-group">
            <label class="col-md-4 control-label" for="tour_duration">Tour Duration</label>
            <div class="col-md-6">
                <label class="radio-inline" for="tour_duration_fixed">
                    <input type="radio" class="tour_duration"  id="tour_duration_fixed" name="tour_duration" value="Fixed" @if (@$duration->tour_duration=='Fixed' || @$duration->tour_duration=='')
                        checked
                    @endif>Fixed
                </label>
                <label class="radio-inline" for="tour_duration_flexible">
                    <input type="radio" class="tour_duration" id="tour_duration_flexible" name="tour_duration" value="Flexible" @if (@$duration->tour_duration=='Flexible')
                    checked
                @endif>Flexible
                </label>
            </div>
        </div>
    <div id="fixed_timing" @if (@$duration->tour_duration=='Fixed' || @$duration->tour_duration=='')
        style="display:block;" 
        @else style="display:none;" 
    @endif>
        <div class="form-group">
            <label class="col-md-4 control-label" for="fixed_duration">Fixed (Minutes/Hour)</label>
            <div class="col-md-6">
                <input type="text" id="fixed_duration" name="fixed_duration" class="form-control" value="{{ @$duration->fixed_duration }}">
            </div>
        </div>
    </div>
    <div id="flexible_timing" @if (@$duration->tour_duration=='Flexible')
        style="display:block;" 
        @else style="display:none;" 
    @endif>
        <div class="form-group">
            <label class="col-md-4 control-label" for="flexible_duration_minus">Flexible (Minutes)</label>
            <div class="col-md-6">
                <input type="text" id="flexible_duration_minus" name="flexible_duration_minus" class="form-control" value="{{ @$duration->flexible_duration_minus }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="flexible_duration_hour">Flexible (Hour)</label>
            <div class="col-md-6">
                <input type="text" id="flexible_duration_hour" name="flexible_duration_hour" class="form-control" value="{{ @$duration->flexible_duration_hour }}">
            </div>
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Tour Duration Block --> 

<!-- Start Tour Time Slot Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Tour Time Slot</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="start_booking_date">From when you want to start taking bookings?        </label>
        <div class="col-md-6">
            <input type="text" id="example-datepicker1" name="start_booking_date" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$duration->start_booking_date }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="time_slot_days">Days when this tour is bookable</label>
        <div class="col-md-6">
            <?php @$days = unserialize(@$duration->time_slot_days);?>
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default @if (@in_array(1,@$days)) active @endif">
                    <input @if (@in_array(1,@$days))
                        checked
                    @endif type="checkbox" name="time_slot_days[]" value="1">Mon
                </label>
                <label class="btn btn-default @if (@in_array(2,@$days)) active @endif">
                    <input @if (@in_array(2,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="2"> Tue
                </label>
                <label class="btn btn-default @if (@in_array(3,@$days)) active @endif">
                    <input @if (@in_array(3,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="3"> Wed
                </label>
                <label class="btn btn-default @if (@in_array(4,@$days)) active @endif">
                    <input @if (@in_array(4,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="4"> Thu
                </label>
                <label class="btn btn-default @if (@in_array(5,@$days)) active @endif">
                    <input @if (@in_array(5,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="5"> Fri
                </label>
                <label class="btn btn-default @if (@in_array(6,@$days)) active @endif">
                    <input @if (@in_array(6,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="6"> Sat
                </label>
                <label class="btn btn-default @if (@in_array(7,@$days)) active @endif">
                    <input @if (@in_array(7,@$days))
                    checked
                @endif  type="checkbox" name="time_slot_days[]" value="7"> Sun
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label" for="specific_booking_date">Do you want to end taking bookings on a specific date? (optional)
        </label>
        <div class="col-md-6">
            <input type="text" id="example-datepicker2" name="specific_booking_date" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$duration->specific_booking_date }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="specific_block_booking_date">Do you want to block specific dates for booking tours?  (optional)
        </label>
        <div class="col-md-6">
            <input type="text" id="example-datepicker3" name="specific_block_booking_date" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$duration->specific_block_booking_date }}">
        </div>
    </div>
    <?php
    @$timings=\DB::table('product_tour_time')->where('product_id',@$edit->id)->get();
    ?>
    <div class="form-group">
        <label class="col-md-4 control-label" for="example-timepicker">Add a tour time (Start Time To End Time)
        </label>
        <div class="col-md-6 timecontrols">

            @foreach ($timings as $key => $timing)
            <input type="hidden" name="time_id[{{ $key }}]" value="{{ @$timing->id }}">
        <div class="timeentry">
            <div class="col-md-5" style="padding:0px;">
                <div class="input-group bootstrap-timepicker">
                    <input type="text" id="example-timepicker" name="start_tour_time[]" class="form-control input-timepicker" value="{{ @$timing->start_tour_time }}">
                    <span class="input-group-btn">
                        <a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a>
                    </span>
                </div>
            </div>
            <div class="col-md-5" style="padding:0px;">
                <div class="input-group bootstrap-timepicker">
                    <input type="text" id="example-timepicker" name="end_tour_time[]" class="form-control input-timepicker" value="{{ @$timing->end_tour_time }}">
                    <span class="input-group-btn">
                        <a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a>
                    </span>
                </div>
            </div>
            <button class="btn btn-remove-time btn-danger" type="button" id="{{ @$timing->id }}">
                <span class="gi gi-minus"></span>
          </button>
        </div>
        @endforeach
        <div class="timeentry">
            <input type="hidden" name="time_id[]">
            <div class="col-md-5" style="padding:0px;">
                <div class="input-group bootstrap-timepicker">
                    <input type="text" name="start_tour_time[]" class="form-control input-timepicker" >
                    <span class="input-group-btn">
                        <a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a>
                    </span>
                </div>
            </div>
            <div class="col-md-5" style="padding:0px;">
                <div class="input-group bootstrap-timepicker">
                    <input type="text"  name="end_tour_time[]" class="form-control input-timepicker" >
                    <span class="input-group-btn">
                        <a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a>
                    </span>
                </div>
            </div>
            <button class="btn btn-success btn-add-time" type="button">
                <span class="gi gi-plus"></span>
          </button>
        </div>
            
        </div>
        
    </div>
    
</fieldset>

</div>
<!-- END Tour Time Slot Block --> 

                    </div>
                   
                    <div class="tab-pane " id="tab6">
                        

<!-- Start Cut Off Time Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Cut Off Time</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="cut_off_time">How would you like to set your cut-off time?
            
        </label>
        <div class="col-md-6">
            <span>Hours before the tour start time</span>
            <input type="text" id="hour_before_tour_time" name="hour_before_tour_time" class="form-control" value="{{ @$duration->hour_before_tour_time }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="advance_notice_time">How much advance notice do you need for additional guest bookings when someone has already booked your experience?</label>
        <div class="col-md-6">
            <input type="text" id="advance_notice_time" name="advance_notice_time" class="form-control" value="{{ @$duration->advance_notice_time }}">
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Cut Off Time Block -->
                    </div>
                    <div class="tab-pane " id="tab7">
                        
<!-- Start Tour Language Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Tour Language</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="tour_language">Select Tour Language</label>
        <div class="col-md-6">
            <?php @$languageArray = unserialize(@$edit->tour_language);?>
                <select name="tour_language[]" id="tour_language" class="form-control select-select2" multiple style="width:100% !important;">
                   @foreach ($languages as $language)
                   <option @if (@in_array($language->id,@$languageArray))
                       selected
                   @endif value="{{ $language->id }}">{{ $language->name }}</option>
                   @endforeach
                </select>
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Tour Language Block -->
                    </div>
                    <div class="tab-pane " id="tab8">
                        
<!-- Start Accessibility Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Accessibility</h2>
    </div>
<fieldset>
    <label style="text-align:left;color: #000; font-size: 16px;" class="col-md-12 control-label" >General</label>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_wheelchair">Is it Wheelchair accessible?</label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_wheelchair_yes">
                <input @if (@$edit->is_wheelchair=='Yes')
                    checked
                @endif type="radio" id="is_wheelchair_yes" name="is_wheelchair" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_wheelchair_no">
                <input @if (@$edit->is_wheelchair=='No')
                checked
            @endif type="radio" id="is_wheelchair_no" name="is_wheelchair" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_stroller">Is it stroller accessible?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_stroller_yes">
                <input @if (@$edit->is_stroller=='Yes')
                checked
            @endif type="radio" id="is_stroller_yes" name="is_stroller" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_stroller_no">
                <input @if (@$edit->is_stroller=='No')
                checked
            @endif type="radio" id="is_stroller_no" name="is_stroller" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_animals_allowed">Are animals allowed?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_animals_allowed_yes">
                <input @if (@$edit->is_animals_allowed=='Yes')
                checked
            @endif type="radio" id="is_animals_allowed_yes" name="is_animals_allowed" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_animals_allowed_no">
                <input @if (@$edit->is_animals_allowed=='No')
                checked
            @endif type="radio" id="is_animals_allowed_no" name="is_animals_allowed" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_travelers_easily">Can travelers easily arrive/depart on public transportation?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_travelers_easily_yes">
                <input @if (@$edit->is_travelers_easily=='Yes')
                checked
            @endif type="radio" id="is_travelers_easily_yes" name="is_travelers_easily" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_travelers_easily_no">
                <input @if (@$edit->is_travelers_easily=='No')
                checked
            @endif type="radio" id="is_travelers_easily_no" name="is_travelers_easily" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_infants_required">Are infants required to sit on laps?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_infants_required_yes">
                <input @if (@$edit->is_infants_required=='Yes')
                checked
            @endif type="radio" id="is_infants_required_yes" name="is_infants_required" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_infants_required_no">
                <input @if (@$edit->is_infants_required=='No')
                checked
            @endif type="radio" id="is_infants_required_no" name="is_infants_required" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_infant_seats_available">Are infant seats available?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_infant_seats_available_yes">
                <input @if (@$edit->is_infant_seats_available=='Yes')
                checked
            @endif type="radio" id="is_infant_seats_available_yes" name="is_infant_seats_available" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_infant_seats_available_no">
                <input @if (@$edit->is_infant_seats_available=='No')
                checked
            @endif type="radio" id="is_infant_seats_available_no" name="is_infant_seats_available" value="No">No
            </label>
        </div>
    </div>
    <label style="text-align:left;color: #000; font-size: 16px;" class="col-md-12 control-label" for="future_options">Give options to add more such options in future</label>
    <div class="form-group">
        <label class="col-md-4 control-label" for="health_restrictions">Health Restrictions
        </label>
        <div class="col-md-6">
            <label class="checkbox-inline" for="travelers_with_back_problems" style="margin-left: 10px;">
                <input @if (@$edit->travelers_with_back_problems=='1')
                checked
            @endif type="checkbox" id="travelers_with_back_problems" name="travelers_with_back_problems" value="1">Not recommended for travelers with back problems
            </label>
            <label class="checkbox-inline" for="pregnant_travelers">
                <input @if (@$edit->pregnant_travelers=='1')
                checked
            @endif type="checkbox" id="pregnant_travelers" name="pregnant_travelers" value="1">Not recommended for pregnant travelers
            </label>
            <label class="checkbox-inline" for="serious_medical">
                <input @if (@$edit->serious_medical=='1')
                checked
            @endif type="checkbox" id="serious_medical" name="serious_medical" value="1">Not recommended for travelers with heart problems or other serious medical conditions             
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="physical_difficulty">Select the physical difficulty level
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="physical_difficulty_easy">
                <input @if (@$edit->physical_difficulty=='Easy')
                checked
            @endif type="radio" id="physical_difficulty_easy" name="physical_difficulty" value="Easy">Easy
            </label>
            <label class="radio-inline" for="physical_difficulty_moderate">
                <input @if (@$edit->physical_difficulty=='Moderate')
                checked
            @endif type="radio" id="physical_difficulty_moderate" name="physical_difficulty" value="Moderate">Moderate
            </label>
            <label class="radio-inline" for="physical_difficulty_challenging">
                <input  @if (@$edit->physical_difficulty=='Challenging')
                checked
            @endif type="radio" id="physical_difficulty_challenging" name="physical_difficulty" value="Challenging">Challenging
            </label>
        </div>
    </div>
</fieldset>

</div>
<!-- END Accessibility Block -->
                    </div>
                    <div class="tab-pane " id="tab9">
                        <?php
                        @$pricing=\DB::table('product_pricing')->where('product_id',@$edit->id)->first();
                        ?>                    
<!-- Start Pricing Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Pricing</h2>
    </div>
<fieldset>
<input type="hidden" name="price_id" value="{{ @$pricing->id }}">
    <div class="form-group">
        <label class="col-md-4 control-label" for="currency">Select Currency</label>
        <div class="col-md-6">
                <select name="currency" id="currency" class="form-control select-select2" style="width:100% !important;">
                    <option @if (@$pricing->currency=='INR')
                        selected
                    @endif value="INR">INR</option>
                    <option @if (@$pricing->currency=='EUR')
                        selected
                    @endif value="EUR">EUR</option>
                </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="age_group_pricing">Define Age Group for Pricing</label>
        <div class="col-md-6">
            <div class="col-md-12">
            <label class="col-md-2 control-label" for="adult_age">Adult</label>
            <div class="col-md-3">
                <span>Min. Age</span>
            <input type="text" id="min_age_adult" name="min_age_adult" class="form-control" value="{{ @$pricing->min_age_adult }}">
            </div>
            <div class="col-md-3">
                <span>Max. Age</span>
                <input type="text" id="max_age_adult" name="max_age_adult" class="form-control" value="{{ @$pricing->max_age_adult }}">
                </div>
            </div>
            <div class="col-md-12">
                <label class="col-md-2 control-label" for="child_age">Child</label>
                <div class="col-md-3">
                    <span>Min. Age</span>
                <input type="text" id="min_age_child" name="min_age_child" class="form-control" value="{{ @$pricing->min_age_child }}">
                </div>
                <div class="col-md-3">
                    <span>Max. Age</span>
                    <input type="text" id="max_age_child" name="max_age_child" class="form-control" value="{{ @$pricing->max_age_child }}">
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="col-md-2 control-label" for="youth_age">Youth</label>
                    <div class="col-md-3">
                        <span>Min. Age</span>
                    <input type="text" id="min_age_youth" name="min_age_youth" class="form-control" value="{{ @$pricing->min_age_youth }}">
                    </div>
                    <div class="col-md-3">
                        <span>Max. Age</span>
                        <input type="text" id="max_age_youth" name="max_age_youth" class="form-control" value="{{ @$pricing->max_age_youth }}">
                        </div>
                    </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="max_no_allow_single_booking">Enter the maximum number of travelers allowed in a single booking</label>
        <div id="travelallow"></div>
        <div class="col-md-6">
            <input type="number" min="1" max="6" id="max_no_allow_single_booking" name="max_no_allow_single_booking" class="form-control" value="{{ @$pricing->max_no_allow_single_booking }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="max_no_allow_travelers">Enter the maximum number of travelers allowed in a Tour</label>
        <div id="maxtravelallow"></div>
        <div class="col-md-6">
            <input type="number" min="1" max="6" id="max_no_allow_travelers" name="max_no_allow_travelers" class="form-control" value="{{ @$pricing->max_no_allow_travelers }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="pricing_type">Select Pricing</label>
        <div class="col-md-6">
            <label class="radio-inline" for="pricing_type_person">
                <input @if (@$pricing->pricing_type=='Per Person')
                checked
                @endif type="radio" id="pricing_type_person" name="pricing_type" value="Per Person">Per Person
            </label>
            <label class="radio-inline" for="pricing_type_vehicle">
                <input @if (@$pricing->pricing_type=='Per Vehicle')
                checked
            @endif type="radio" id="pricing_type_vehicle" name="pricing_type" value="Per Vehicle">Per Vehicle
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="pricing_options">How many pricing options do you have for this product?
</label>
        <div class="col-md-6">
            <label class="radio-inline" for="pricing_options_one">
                <input @if (@$pricing->pricing_options=='One')
                checked
            @endif type="radio" id="pricing_options_one" name="pricing_options" value="One">One </label>
            <label class="radio-inline" for="pricing_options_more">
                <input @if (@$pricing->pricing_options=='More than one')
                checked
            @endif type="radio" id="pricing_options_more" name="pricing_options" value="More than one">More than one
            </label>
        </div>
    </div>
    <div class="form-group" id="hideattri">
        <label class="col-md-4 control-label" for="add_attributes">Add attributes that apply to this option
           (Name this Option) </label>
        <div class="col-md-6">
            <input type="text" id="add_attributes" name="add_attributes" class="form-control" value="{{ @$pricing->add_attributes }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="price_for_tour">Price for the Tour
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="price_for_tour_free">
                <input @if (@$pricing->price_for_tour_type=='Free')
                checked
            @endif type="radio" id="price_for_tour_free" name="price_for_tour_type" value="Free">Free (Contribution based)
            </label>
            <label class="radio-inline" for="price_for_tour_paid">
                <input @if (@$pricing->price_for_tour_type=='Paid')
                checked
            @endif type="radio" id="price_for_tour_paid" name="price_for_tour_type" value="Paid">Paid Tour
            </label>
            <br><br>
            
            <div id="showfreetour">
            How many people can book for free?
            <div id="freetour"></div>
            <input type="number" id="no_people_book_for_free" min="1" max="2" name="no_people_book_for_free" class="form-control" value="{{ @$pricing->no_people_book_for_free }}">
            <br>
            In case the above limit cross then each person will pay this price
            <input type="text" id="limit_cross_price" name="limit_cross_price" class="form-control" value="{{ @$pricing->limit_cross_price }}">
          
            </div>
                <div id="showpaidtoru">
                Minimum person required?
                <div id="paidtour"></div>
                <input type="number" min="1" max="6" id="min_person_required" name="min_person_required" class="form-control" value="{{ @$pricing->min_person_required }}">
                <br>

                <?php @$tourtype = unserialize(@$pricing->pricing_tour_type);?>

                <label class="checkbox-inline" for="pricing_tour_type_basic">
                    <input @if (@in_array('Basic',@$tourtype))
                        checked
                    @endif type="checkbox" id="pricing_tour_type_basic" name="pricing_tour_type[]" value="Basic">Basic
                </label>
                <label class="checkbox-inline" for="pricing_tour_type_budget">
                    <input @if (@in_array('Budget',@$tourtype))
                    checked
                @endif type="checkbox" id="pricing_tour_type_budget" name="pricing_tour_type[]" value="Budget">Budget
                </label>
                <label class="checkbox-inline" for="pricing_tour_type_premium">
                    <input @if (@in_array('Premium',@$tourtype))
                    checked
                @endif type="checkbox" id="pricing_tour_type_premium" name="pricing_tour_type[]" value="Premium">Premium
                </label>
                <label class="checkbox-inline" for="pricing_tour_type_vip">
                    <input @if (@in_array('VIP',@$tourtype))
                    checked
                @endif type="checkbox" id="pricing_tour_type_vip" name="pricing_tour_type[]" value="VIP">VIP
                </label>
                <label class="checkbox-inline" for="pricing_tour_type_private">
                    <input @if (@in_array('Private',@$tourtype))
                    checked
                @endif type="checkbox" id="pricing_tour_type_private" name="pricing_tour_type[]" value="Private">Private
                </label>
<br><br>
                <label class="col-md-4 control-label" for="adult_price">Adult Price</label>
                 <div class="col-md-6">
                     <input type="text" id="adult_price" name="adult_price" class="form-control" value="{{ @$pricing->adult_price }}">
                 </div>
                 <label class="col-md-4 control-label" for="child_price">Child Price</label>
                 <div class="col-md-6">
                     <input type="text" id="child_price" name="child_price" class="form-control" value="{{ @$pricing->child_price }}">
                 </div>
                 <label class="col-md-4 control-label" for="youth_price">Youth Price</label>
                 <div class="col-md-6">
                     <input type="text" id="youth_price" name="youth_price" class="form-control" value="{{ @$pricing->youth_price }}">
                 </div>

                 <br><br>
                 <label class="checkbox-inline" for="group_pricing">
                    <input @if (@$pricing->group_pricing=='1')
                        checked
                    @endif type="checkbox" id="group_pricing" name="group_pricing" value="1">Group Pricing
                </label>
                <br><br>
                <div class="col-md-12">
                    <label class="col-md-4 control-label" for="no_of_people_in_group">No. of people in group</label>
                    <div class="col-md-6">
                        <input type="text" id="no_of_people_in_group" name="no_of_people_in_group" class="form-control" value="{{ @$pricing->no_of_people_in_group }}">
                    </div>
                </div>
                <div class="col-md-12">
                 <label class="col-md-4 control-label" for="group_pricing_min">Min</label>
                 <div class="col-md-6">
                     <input type="text" id="group_pricing_min" name="group_pricing_min" class="form-control" value="{{ @$pricing->group_pricing_min }}">
                 </div>
                </div>
                <div class="col-md-12">
                    <label class="col-md-4 control-label" for="group_pricing_max">Max</label>
                    <div class="col-md-6">
                        <input type="text" id="group_pricing_max" name="group_pricing_max" class="form-control" value="{{ @$pricing->group_pricing_max }}">
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="col-md-4 control-label" for="group_pricing_price">Price</label>
                    <div class="col-md-6">
                        <input type="text" id="group_pricing_price" name="group_pricing_price" class="form-control" value="{{ @$pricing->group_pricing_price }}">
                    </div>
                   </div>
                   <label class="checkbox-inline" for="tiered_pricing">
                    <input @if (@$pricing->tiered_pricing=='1')
                        checked
                    @endif type="checkbox" id="tiered_pricing" name="tiered_pricing" value="1">Tiered Pricing (applicable only for adults)
                </label>
                <br><br>
                <div class="col-md-12">
                    <label class="col-md-4 control-label" for="tiered_pricing_from">From</label>
                    <div class="col-md-6">
                        <input type="text" id="tiered_pricing_from" name="tiered_pricing_from" class="form-control" value="{{ @$pricing->tiered_pricing_from }}">
                    </div>
                   </div>
                   <div class="col-md-12">
                    <label class="col-md-4 control-label" for="tiered_pricing_to">To</label>
                    <div class="col-md-6">
                        <input type="text" id="tiered_pricing_to" name="tiered_pricing_to" class="form-control" value="{{ @$pricing->tiered_pricing_to }}">
                    </div>
                   </div>
                   <div class="col-md-12">
                    <label class="col-md-4 control-label" for="tiered_pricing_per_person_price">Price per person</label>
                    <div class="col-md-6">
                        <input type="text" id="tiered_pricing_per_person_price" name="tiered_pricing_per_person_price" class="form-control" value="{{ @$pricing->tiered_pricing_per_person_price }}">
                    </div>
                   </div>
                
                <div class="col-md-12">
                    <label class="col-md-4 control-label" for="tax_on_pricing">Tax on Pricing</label>
                    <div class="col-md-6">
                        <div class="col-md-12">
                        <span>Name tax </span>
                        <input type="text" id="tax_name" name="tax_name" class="form-control" value="{{ @$pricing->tax_name }}">
                    </div>
                    <div class="col-md-12">
                        <span>Tax Rate</span>
                        <input type="text" id="tax_rate" name="tax_rate" class="form-control" value="{{ @$pricing->tax_rate }}">
                    </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="col-md-4 control-label" for="pricing_days">Days when these pricing are applicable
                    </label>
                    <div class="col-md-6">
                        <?php @$pricingdays = unserialize(@$pricing->pricing_days);?>
                        
            
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default @if (@in_array(1,@$pricingdays)) active @endif">
                                <input @if (@in_array(1,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="1">Mon
                            </label>
                            <label class="btn btn-default @if (@in_array(2,@$pricingdays)) active @endif">
                                <input @if (@in_array(2,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="2"> Tue
                            </label>
                            <label class="btn btn-default @if (@in_array(3,@$pricingdays)) active @endif">
                                <input @if (@in_array(3,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="3"> Wed
                            </label>
                            <label class="btn btn-default @if (@in_array(4,@$pricingdays)) active @endif">
                                <input @if (@in_array(4,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="4"> Thu
                            </label>
                            <label class="btn btn-default @if (@in_array(5,@$pricingdays)) active @endif">
                                <input @if (@in_array(5,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="5"> Fri
                            </label>
                            <label class="btn btn-default @if (@in_array(6,@$pricingdays)) active @endif">
                                <input @if (@in_array(6,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="6"> Sat
                            </label>
                            <label class="btn btn-default @if (@in_array(7,@$pricingdays)) active @endif">
                                <input @if (@in_array(7,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="7"> Sun
                            </label>
                        </div>
                    </div>
                
                </div>
            
            </div>
            </div>
    </div>
    {{-- <div class="form-group">
        <label class="col-md-4 control-label" for="tax_on_pricing">Tax on Pricing</label>
        <div class="col-md-6">
            <div class="col-md-12">
            <span>Name tax </span>
            <input type="text" id="tax_name" name="tax_name" class="form-control" value="{{ @$pricing->tax_name }}">
        </div>
        <div class="col-md-12">
            <span>Tax Rate</span>
            <input type="text" id="tax_rate" name="tax_rate" class="form-control" value="{{ @$pricing->tax_rate }}">
        </div>
        </div>
    </div> --}}
    
</fieldset>

</div>
<!-- END Pricing Block -->

                    </div>
                    <div class="tab-pane " id="tab10">
                        
<!-- Start Cancellation Policy Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Cancellation Policy</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="cancellation_policy">Select your cancellation policy</label>
        <div class="col-md-6">
            <label class="radio-inline" for="cancellation_policy_24">
                <input @if (@$edit->cancellation_policy=='1') checked @endif type="radio" id="cancellation_policy_24" name="cancellation_policy" value="1">24 Hours (Recommended)
            </label>
            <label class="radio-inline" for="cancellation_policy_non">
                <input @if (@$edit->cancellation_policy=='0') checked @endif type="radio" id="cancellation_policy_non" name="cancellation_policy" value="0">Non-refundable
            </label>
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Cancellation Policy Block -->

<!-- Start Additional Info Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Additional Info</h2>
    </div>
<fieldset>
   
    <div class="form-group">
        <label class="col-md-4 control-label" for="additional_info">Additional Info
        </label>
        <div class="col-md-6">
            <input type="text" id="additional_info" name="additional_info" class="form-control" value="{{ @$edit->additional_info }}">
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Additional Info Block -->
                    </div>
                </div>
                <!-- END Tabs Content -->
    
</fieldset>

</div>
<!-- END Cancellation Policy Block -->

        <input type="hidden" name="id" value="{{ @$edit->id }}">
   
</form>
<!-- END Form Validation Example Content -->


</div>

</div>
</div>
<script>
  $("#hideattri") .hide(); 
  $("#showfreetour") .hide(); 
  $("#showpaidtoru") .hide(); 
$('#max_no_allow_single_booking').keyup(function(){
  if ($(this).val() > 6){
   $("#travelallow").html('<span style="color:red">maximum 6 travelers allowed</span>')
    $(this).val('6');
  }
});
$('#max_no_allow_travelers').keyup(function(){
  if ($(this).val() > 6){
   $("#maxtravelallow").html('<span style="color:red">maximum 6 travelers allowed</span>')
    $(this).val('6');
  }
});
$('#no_people_book_for_free').keyup(function(){
  if ($(this).val() > 2){
   $("#freetour").html('<span style="color:red">maximum 2 people book for free</span>')
    $(this).val('2');
  }
});
//--------------show hide option on redio check ------------//  

$('input[type="radio"]').change(function(){
      
   var inputValue = $(this).attr("value");
    if(inputValue=='One')
    {
        $("#hideattri").hide();
    }
    else if(inputValue=='More than one')
    {
        $("#hideattri").show();    
    }
    else if(inputValue=='Free')
    {
        $("#showfreetour").show();   
        $("#showpaidtoru").hide();   
    }else if(inputValue=='Paid')
    {
        $("#showpaidtoru").show(); 
        $("#showfreetour").hide(); 
    }

  });
//-------------end show hide section------------------//

//---------- create multiple price for paid tour ---------//

$(function()
{
    var i=0;
    $(document).on('click', '.btn-add-itinary', function(e)
    {
       
        e.preventDefault();
        
        var controlForm = $('.itinary-controls:first'),
            currentEntry = $(this).parents('.itinary-entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        //newEntry.find('input').val('');

        
        
        controlForm.find('.itinary-entry:not(:last) .btn-add-itinary')
            .removeClass('btn-add-itinary').addClass('btn-remove-itinary')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="gi gi-minus"></span>');

            var j=parseInt(i)+parseInt(1);
            // console.log('input[name="spent_time['+i+'][0]"]');
            // console.log('input[name="spent_time['+i+'][1]"]');

            newEntry.find('input[name="included_price_tour['+i+']"]').attr('name', 'included_price_tour['+j+']');
            newEntry.find('input[name="spent_time['+i+'][0]"]').attr('name', 'spent_time['+j+'][0]');
            newEntry.find('input[name="spent_time['+i+'][1]"]').attr('name', 'spent_time['+j+'][1]');
        i++;

    }).on('click', '.btn-remove-itinary', function(e)
    {
        var c = confirm('Are you sure you want to remove?');
        if(c){
            $(this).parents('.itinary-entry:first').remove();
            // e.preventDefault();
            // return false;
            var id=$(this).attr('id');
            if(id){
            
                var _token = $('input[name="_token"]').val();
                $.ajax({
                             url:"{{ url('') }}/admin/removeitinary",
                             method:"POST",
                             dataType:'Json',
                             data:{id:id, _token:_token},
                             success:function(data){
                                
                             }
                });
            }
        }
     
	});
});


//------------ end multiple tour price -------------//

$(function()
{
    var i=0;
    $(document).on('click', '.btn-add-itinary', function(e)
    {
       
        e.preventDefault();
        
        var controlForm = $('.itinary-controls:first'),
            currentEntry = $(this).parents('.itinary-entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        //newEntry.find('input').val('');

        
        
        controlForm.find('.itinary-entry:not(:last) .btn-add-itinary')
            .removeClass('btn-add-itinary').addClass('btn-remove-itinary')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="gi gi-minus"></span>');

            var j=parseInt(i)+parseInt(1);
            // console.log('input[name="spent_time['+i+'][0]"]');
            // console.log('input[name="spent_time['+i+'][1]"]');

            newEntry.find('input[name="included_price_tour['+i+']"]').attr('name', 'included_price_tour['+j+']');
            newEntry.find('input[name="spent_time['+i+'][0]"]').attr('name', 'spent_time['+j+'][0]');
            newEntry.find('input[name="spent_time['+i+'][1]"]').attr('name', 'spent_time['+j+'][1]');
        i++;

    }).on('click', '.btn-remove-itinary', function(e)
    {
        var c = confirm('Are you sure you want to remove?');
        if(c){
            $(this).parents('.itinary-entry:first').remove();
            // e.preventDefault();
            // return false;
            var id=$(this).attr('id');
            if(id){
            
                var _token = $('input[name="_token"]').val();
                $.ajax({
                             url:"{{ url('') }}/admin/removeitinary",
                             method:"POST",
                             dataType:'Json',
                             data:{id:id, _token:_token},
                             success:function(data){
                                
                             }
                });
            }
        }
     
	});
});

$(function()
{
    $(document).on('click', '.btn-add-time', function(e)
    {
        e.preventDefault();

        var controlForm = $('.timecontrols:first'),
            currentEntry = $(this).parents('.timeentry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        
        controlForm.find('.timeentry:not(:last) .btn-add-time')
            .removeClass('btn-add-time').addClass('btn-remove-time')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="gi gi-minus"></span>');

            $('.input-timepicker').timepicker({minuteStep: 1,showSeconds: false,showMeridian: true,defaultTime: ''});
    }).on('click', '.btn-remove-time', function(e)
    {
      var c = confirm('Are you sure you want to remove?');
        if(c){
            $(this).parents('.timeentry:first').remove();
            // e.preventDefault();
            // return false;
            var id=$(this).attr('id');
            if(id){
            
                var _token = $('input[name="_token"]').val();
                $.ajax({
                             url:"{{ url('') }}/admin/removetourtime",
                             method:"POST",
                             dataType:'Json',
                             data:{id:id, _token:_token},
                             success:function(data){
                                
                             }
                });
            }
        }
	});
});
$(document).on('change', '.tour_duration', function(e)
{
    var ductionType=$(this).val();
    if(ductionType=='Fixed'){
        $('#flexible_timing').hide();
        $('#fixed_timing').show();
    }else{
        $('#fixed_timing').hide();
        $('#flexible_timing').show();
    }

});
$(document).on('change', '#country', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getcitybycountry",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#city').html(data);
                 }
                });
    
});
$(document).on('change', '#inclusion_category', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getinclusionsubcate",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#inclusion_sub_category').html(data);
                 }
    });
    
});

$(document).on('change', '#exclusion_category', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getinclusionsubcate",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#exclusion_sub_category').html(data);
                 }
    });
    
});

    $(document).on('click', '.removeimg', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('p').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deleteproductgalleryimage",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
 $(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="gi gi-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});


google.maps.event.addDomListener(window, 'load', function () {
          var options = {
			//types: ['(cities)'],
			};
            var places = new google.maps.places.Autocomplete(document.getElementById('pickup'),options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var lat = places.getPlace().geometry.location.lat();
                var lng = places.getPlace().geometry.location.lng();
                    $('#pickup_latitude').val(lat);
                    $('#pickup_longitude').val(lng);

                    var map = new google.maps.Map(document.getElementById('pickupmap'), {
            center: {lat: lat, lng: lng},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var marker = new google.maps.Marker({map: map,draggable:true, position: {lat: lat, lng: lng}});
          google.maps.event.addListener(marker, 'dragend', function() 
            {
                //console.log(marker.getPosition().lat());
                    $('#pickup_latitude').val(marker.getPosition().lat());
                    $('#pickup_longitude').val(marker.getPosition().lng());
            });

          if(lat && lng){
                $('#pickupmap').show();
            }
            });
            
            

            var dropplaces = new google.maps.places.Autocomplete(document.getElementById('dropoff'),options);
            google.maps.event.addListener(dropplaces, 'place_changed', function () {
                var droplat = dropplaces.getPlace().geometry.location.lat();
                var droplng = dropplaces.getPlace().geometry.location.lng();
                    $('#drop_latitude').val(droplat);
                    $('#drop_longitude').val(droplng);

            var dropmap = new google.maps.Map(document.getElementById('dropmap'), {
            center: {lat: droplat, lng: droplng},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var dropmarker=new google.maps.Marker({map: dropmap,draggable:true, position: {lat: droplat, lng: droplng}});
          google.maps.event.addListener(dropmarker, 'dragend', function() 
            {
                //console.log(marker.getPosition().lat());
                    $('#drop_latitude').val(dropmarker.getPosition().lat());
                    $('#drop_longitude').val(dropmarker.getPosition().lng());
            });
          if(droplat && droplng){
                $('#dropmap').show();
            }
            });
        });
</script>
@endsection