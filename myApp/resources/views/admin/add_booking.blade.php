@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-log_book"></i>Booking Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Booking</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation-booking"  method="POST" action="{{url('admin/submitbooking')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">BOOKING CHANNEL <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            
                          <select class="form-control  select-select2" name="channel" id="channel" @if (@$edit)
                          disabled
                          @endif >
                        <option value="">Select</option> 
                        @foreach ($channels as $channel)
                        <option @if (@$edit->channel_id==$channel->id)
                            selected
                        @endif value="{{ $channel->id }}">{{ $channel->name }}</option> 
                        @endforeach
                        </select>  
                        @if ($errors->has('channel'))
                                            <span class="text-danger">{{ $errors->first('channel') }}</span>
                                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">TOUR TYPE <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            
                          <select class="form-control  select-select2 getmytour" name="tourtype" id="tourtype" @if (@$edit)
                          disabled
                          @endif>
                        <option value="">Select</option> 
                        @foreach ($tourtypes as $tourtype)
                        <option @if (@$edit->tour_type_id==$tourtype->id)
                            selected
                        @endif value="{{ $tourtype->id }}">{{ $tourtype->name }}</option> 
                        @endforeach
                        </select>  
                        @if ($errors->has('tourtype'))
                                            <span class="text-danger">{{ $errors->first('tourtype') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                        <label class="col-md-4 control-label" for="name">CITY<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                
                              <select class="form-control  select-select2 getmytour" name="city" id="destination" @if (@$edit)
                              disabled
                              @endif>
                            <option value="">Select</option> 
                            @foreach ($destinations as $destination)
                            <option @if (@$edit->destination_id==$destination->id)
                                selected
                            @endif value="{{ $destination->id }}">{{ $destination->name }}</option> 
                            @endforeach
                            </select>  
                            @if ($errors->has('city'))
                                                <span class="text-danger">{{ $errors->first('city') }}</span>
                                            @endif
                        </div>
                    </div>
                    <?php
                        @$tours=\DB::table('tours')->where('destination_id',@$edit->destination_id)->where('type',@$edit->tour_type_id)->get();
                    ?>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">TOUR NAME <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                <div id="gettours">
                                    <select  class="form-control  select-select2" name="tour_name" id="tour_name" @if (@$edit)
                                    disabled
                                    @endif>
                                        <option value="">Select</option> 
                                        @foreach ($tours as $tour)
                                        <option @if ($tour->id==@$edit->tour_id)
                                            selected
                                        @endif value="{{ $tour->id }}">{{ $tour->title }}</option>  
                                        @endforeach
                                      
                                        </select>  
                                </div>
                              
                            @if ($errors->has('tour_name'))
                                                <span class="text-danger">{{ $errors->first('tour_name') }}</span>
                                            @endif
                        </div>
                    </div>
                   
                <div class="form-group">
                    <label class="col-md-4 control-label" for="number_of_people">NUMBER OF PEOPLE  <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="number_of_people" name="number_of_people" class="form-control" value="{{ @$edit->number_of_people }}" >
                            
                        @if ($errors->has('number_of_people'))
                                            <span class="text-danger">{{ $errors->first('number_of_people') }}</span>
                                        @endif
                    </div>
                </div>  
                
                <div class="form-group">
                    <label class="col-md-4 control-label" for="date">DATE <span class="text-danger">*</span>                                   </label>
                    <div class="col-md-6">
                            
                            <input type="text" id="example-datepicker5" name="date" class="form-control input-datepicker-close" data-date-format="M dd,yyyy" value="{{ @$edit->date }}" autocomplete="off">
                            @if ($errors->has('date'))
                                        <span class="text-danger">{{ $errors->first('date') }}</span>
                                    @endif
                        
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="date">TIME <span class="text-danger">*</span>                                   </label>
                <div class="col-md-6">
                        
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" id="example-timepicker24" name="time" class="form-control input-timepicker24"  value="{{ @$edit->time }}" autocomplete="off">
                            <span class="input-group-btn">
                                <a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a>
                            </span>
                        </div>
                        @if ($errors->has('time'))
                                    <span class="text-danger">{{ $errors->first('time') }}</span>
                                @endif
                    
                </div>
        </div>
                    <div class="form-group">
                            <label class="col-md-4 control-label" for="booking_amount">BOOKING AMOUNT  <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                    <input @if (@$edit)
                                    disabled
                                    @endif type="text" id="booking_amount" name="booking_amount" class="form-control" value="{{ @$edit->booking_amount }}">
                                    
                                @if ($errors->has('booking_amount'))
                                                    <span class="text-danger">{{ $errors->first('booking_amount') }}</span>
                                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="booking_amt_paytype">BOOKING AMOUNT PAYMENT TYPE  <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <select @if (@$edit)
                                    disabled
                                    @endif class="form-control  select-select2" name="booking_amt_paytype" id="booking_amt_paytype">
                                        <option value="">Select</option> 
                                        @foreach ($paytypes as $paytype)
                                        <option @if (@$edit->booking_amt_paytype==$paytype->id)
                                            selected
                                        @endif value="{{ $paytype->id }}">{{ $paytype->name }}</option> 
                                        @endforeach
                                        </select>  
                                        
                                    @if ($errors->has('booking_amt_paytype'))
                                                        <span class="text-danger">{{ $errors->first('booking_amt_paytype') }}</span>
                                                    @endif
                                </div>
                            </div>
                        
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="discount">DISCOUNT </label>
                                <div class="col-md-6">
                                        <input type="text" id="discount" name="discount" class="form-control" value="{{ @$edit->discount }}">
                                        @if ($errors->has('discount'))
                                        <span class="text-danger">{{ $errors->first('discount') }}</span>
                                    @endif
                                </div>
                        </div> 
                        
                        
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="additional_amount">ADDITIONAL AMOUNT 
                                    </label>
                                <div class="col-md-6">
                                        <input type="text" id="additional_amount" name="additional_amount" class="form-control" value="{{ @$edit->additional_amount }}">
                                        @if ($errors->has('additional_amount'))
                                        <span class="text-danger">{{ $errors->first('additional_amount') }}</span>
                                    @endif
                                </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="additional_amt_paytupe">ADDITIONAL AMOUNT PAYMENT TYPE </label>
                            <div class="col-md-6">
                                <select class="form-control  select-select2" name="additional_amt_paytupe" id="additional_amt_paytupe">
                                    <option value="">Select</option> 
                                    @foreach ($paytypes as $paytype)
                                    <option @if (@$edit->additional_amt_paytupe==$paytype->id)
                                        selected
                                    @endif value="{{ $paytype->id }}">{{ $paytype->name }}</option> 
                                    @endforeach
                                    </select>  
                                    
                                @if ($errors->has('additional_amt_paytupe'))
                                                    <span class="text-danger">{{ $errors->first('additional_amt_paytupe') }}</span>
                                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="reason_additional_amount">REASON FOR ADDITIONAL AMOUNT </label>
                                <div class="col-md-6">
                                        <input type="text" id="reason_additional_amount" name="reason_additional_amount" class="form-control" value="{{ @$edit->reason_additional_amount }}">
                                        @if ($errors->has('reason_additional_amount'))
                                        <span class="text-danger">{{ $errors->first('reason_additional_amount') }}</span>
                                    @endif
                                </div>
                        </div>
                        <?php
                        @$customers=\DB::table('users')->where('channel_id',@$edit->channel_id)->get();
                    ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">CUSTOMER <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                    <div id="getcustomers">
                                        <select @if (@$edit)
                                        disabled
                                        @endif class="form-control  select-select2" name="customer" id="customer">
                                            <option value="">Select</option> 
                                          @foreach ($customers as $customer)
                                          <option @if ($customer->id==@$edit->customer_id)
                                              selected
                                          @endif value="{{ $customer->id }}">{{ $customer->name }}</option>
                                          @endforeach
                                            </select>  
                                    </div>
                                  
                                @if ($errors->has('customer'))
                                                    <span class="text-danger">{{ $errors->first('customer') }}</span>
                                                @endif
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="booking_reference">BOOKING REFERENCE 
                                <span class="text-danger">*</span> </label>
                            <div class="col-md-6">
                                    <input @if (@$edit)
                                    disabled
                                    @endif type="text" id="booking_reference" name="booking_reference" class="form-control" value="{{ @$edit->booking_reference }}">
                                    @if ($errors->has('booking_reference'))
                                    <span class="text-danger">{{ $errors->first('booking_reference') }}</span>
                                @endif
                            </div>
                    </div>
                        
                    </fieldset>
                
    <input type="hidden" name="id" value="{{ @$edit->id }}">
    <div class="form-group form-actions">
        <div class="col-md-6 col-md-offset-4">
            @if ((@$edit->id))
            <button data-toggle="modal" data-target="#reschedule" type="button" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> CONFIRM RESCHEDULE</button>
            @else
            <button type="submit" value="save" name="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> SAVE</button>
            <button type="submit" value="allocate" name="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> ALLOCATE</button>
            @endif
            
        </div>
    </div>
    <div id="reschedule" class="modal fade" role="dialog">
            <div class="modal-dialog">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">CONFIRM RESCHEDULE</h4>
                </div>
                <div class="modal-body">
                  <textarea name="reschedule" id="" cols="30" rows="10" style="width:100%;"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
              </div>
          
            </div>
          </div>
</form>


<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
<script>
    
$(document).on('change', '#channel', function(e)
    {
        var id=$(this).val();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/getcustomers",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#getcustomers').html(data);
                    $('.select-select2').select2();
                 }
                });
        
    });
    $(document).on('change', '.getmytour', function(e)
    {
         var cid=$('#destination').val();
         var tid=$('#tourtype').val();
         var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/gettours",
                 method:"POST",
                 data:{cid:cid,tid:tid, _token:_token},
                 success:function(data){
                    $('#gettours').html(data);
                    $('.select-select2').select2();
                 }
                });
        
    });
</script>

@endsection

