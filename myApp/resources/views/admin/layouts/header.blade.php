<?php
$userdata=Auth::guard('admin')->user();
?>
<!-- Header -->
<header class="navbar navbar-default">
                <!-- Left Header Navigation -->
                <ul class="nav navbar-nav-custom">
                    <!-- Main Sidebar Toggle Button -->
                    <li>
                        <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                            <i class="fa fa-bars fa-fw"></i>
                        </a>
                    </li>
                    <!-- END Main Sidebar Toggle Button -->

                   
                </ul>
                <!-- END Left Header Navigation -->

                

                <!-- Right Header Navigation -->
                <ul class="nav navbar-nav-custom pull-right">
                    
                    <!-- User Dropdown -->
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                             
                            @if ($userdata->profile_img)
                            <img src="{{ url('') }}/upload/profileimages/{{$userdata->profile_img}}" alt="{{$userdata->first_name}}">
                            @else
                            <img src="{{ url('') }}/assets/admin/img/placeholders/avatars/avatar2.jpg"alt="{{$userdata->first_name}}">
                            @endif
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                            
                            <li>
                                <a href="{{ url('admin/profile') }}">
                                    <i class="fa fa-user fa-fw pull-right"></i>
                                    Profile
                                </a>
                               
                            </li>
                            <li class="divider"></li>
                            <li>
                                        <a href="{{ url('admin/changepassword') }}">
                                            <i class="fa fa-lock fa-fw pull-right"></i>
                                            Change Password
                                        </a>
                                       
                                    </li>
                                    <li class="divider"></li>
                            <li>
                               
                                <a href="{{ url('admin/logout') }}"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                            </li>
                            
                        </ul>
                    </li>
                    <!-- END User Dropdown -->
                </ul>
                <!-- END Right Header Navigation -->
            </header>
            <!-- END Header -->