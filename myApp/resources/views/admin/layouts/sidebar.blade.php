<?php
$user=Auth::guard('admin')->user();
?>
<!-- Main Sidebar -->
<div id="sidebar">
<!-- Wrapper for scrolling functionality -->
<div id="sidebar-scroll">
<!-- Sidebar Content -->
<div class="sidebar-content">
<!-- Brand -->
<a href="{{ url('') }}/admin/dashboard" class="sidebar-brand">
<i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Yo</strong>Tour</span>
</a>
<!-- END Brand -->

<!-- User Info -->
<div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
<div class="sidebar-user-avatar">
<a href="{{ url('admin/profile') }}">

@if ($user->profile_img)
<img src="{{ url('') }}/upload/profileimages/{{$user->profile_img}}" class="w-50 rounded-circle" alt="{{$user->name}}">
@else
<img src="{{ url('') }}/assets/admin/img/placeholders/avatars/avatar2.jpg" alt="{{$user->name}}">
@endif
</a>
</div>
<div class="sidebar-user-name">{{$user->name}}</div>
<div class="sidebar-user-links">
<a href="{{ url('admin/profile') }}" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>


<a href="{{ url('admin/logout') }}" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
</div>
</div>
<!-- END User Info -->



<!-- Sidebar Navigation -->
<ul class="sidebar-nav">
<li>
<a href="{{ url('') }}/admin/dashboard"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard Website</span></a>
</li>
<li>
    <a href="{{ url('') }}/admin/dashboard"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Website Backend</span></a>
    </li>
    <li>
        <a href="{{ url('') }}/admin/dashboard"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard Business</span></a>
        </li>

        <li>
            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-circle_plus sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Operations</span></a>
            <ul>
                    <li>
                        <a class="sub-menu" href="{{ url('') }}/admin/all-booking">Manage Bookings</a>
                        </li>
                        <li>
                                <a class="sub-menu" href="{{ url('') }}/admin/unallocated-booking">Allocate Tours</a>
                            </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/import-booking">Import Bookings</a>
                </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/add-booking">Create New Booking</a>
                </li>
                <li>
                    <a class="sub-menu" href="#">Storyteller Availability</a>
                </li>
                <li>
                    <a class="sub-menu" href="#">Today's Tour Status</a>
                </li>
                
            </ul>
            </li>
            <li>
                <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-product-hunt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Products </span></a>
                <ul>
        <li>
            <a href="#" class="sidebar-nav-submenu "><i class="fa fa-angle-left sidebar-nav-indicator"></i>Create Experience</a>
            <ul>
                <li>
                    <a href="{{ url('') }}/admin/experience/add-tour-activity">Tour/Activity</a>
                </li>
                <li>
                    <a href="{{ url('') }}/admin/experience/add-tickets">Tickets</a>
                </li>
                <li>
                    <a href="{{ url('') }}/admin/experience/add-transport-rental">Transport/Rental</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
            </ul>
            </li>
            <li>
                <a class="sub-menu" href="{{ url('') }}/admin/create-holiday">Create Holiday</a>
                                </li>
                    <li>
                        <a class="sub-menu" href="{{ url('') }}/admin/all-combo-product">Create Combos</a>
                    </li>
                    <li>
                        <a class="sub-menu" href="{{ url('') }}/admin/allproducts">View All Product</a>
                    </li>
                    
                </ul>
                </li>

                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-chat sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Reviews</span></a>
                    <ul>
                        <li>
                            <a class="sub-menu" href="{{ url('') }}/admin/all-admin-review">Add Review</a>
                        </li>
                        <li>
                            <a class="sub-menu" href="{{ url('') }}/admin/all-customer-review">View Customer Review</a>
                        </li>
                    </ul>
                    </li>

                    <li>
                        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user  sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Storytellers</span></a>
                        <ul>
                            <li>
                                <a class="sub-menu" href="#">View Open Hiring Requests</a>
                            </li>
                            <li>
                                <a class="sub-menu" href="#">Send MOA</a>
                            </li>
                            <li>
                                <a class="sub-menu" href="#">Onboard Storyteller</a>
                            </li>
                            <li>
                                <a class="sub-menu" href="{{ url('') }}/admin/all-storyteller">Manage Storyteller</a>
                            </li>
                            <li>
                                <a class="sub-menu" href="#">Payment Settlement</a>
                            </li>
                            <li>
                                <a class="sub-menu" href="#">Performance Report</a>
                            </li>
                        </ul>
                        </li>

                        <li>
                            <a href="{{ url('') }}/admin/all-customer"><i class="gi gi-group sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Customers</span></a>
                            </li>
                            
    <li>
        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-gift sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Promotions & Discounts</span></a>
        <ul>
            <li>
                <a class="sub-menu" href="#">Yo Cashback</a>
            </li>
            <li>
                <a class="sub-menu" href="{{ url('') }}/admin/all-coupon">Coupons</a>
            </li>
            <li>
                <a class="sub-menu" href="#">Flash Deals</a>
            </li>
            <li>
                <a class="sub-menu" href="#">Limited Time Deals</a>
            </li>
            <li>
                <a class="sub-menu" href="#">Games/Quiz</a>
            </li>
        </ul>
        </li>

        <li>
            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-settings sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Settings</span></a>
            <ul>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/all-category">Product Category</a>
                </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/all-themes">Themes</a>
                </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/menu">Menu</a>
                </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/all-paytype">Payment Type</a>
                </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/all-languages">Tour Languages</a>
                </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/all-destination">City</a>
                </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/all-country">Country</a>
                </li>
                <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/all-channel">Booking Channel</a>
                </li>
            </ul>
            </li>

            <li>
                <a href="#"><i class="gi gi-book sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Reports</span></a>
                </li>


</ul>
<!-- END Sidebar Navigation -->


</div>
<!-- END Sidebar Content -->
</div>
<!-- END Wrapper for scrolling functionality -->
</div>
<!-- END Main Sidebar -->