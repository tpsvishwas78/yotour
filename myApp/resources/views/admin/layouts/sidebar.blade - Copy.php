<?php
$user=Auth::guard('admin')->user();
?>
<!-- Main Sidebar -->
<div id="sidebar">
<!-- Wrapper for scrolling functionality -->
<div id="sidebar-scroll">
<!-- Sidebar Content -->
<div class="sidebar-content">
<!-- Brand -->
<a href="{{ url('') }}/admin/dashboard" class="sidebar-brand">
<i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Yo</strong>Tour</span>
</a>
<!-- END Brand -->

<!-- User Info -->
<div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
<div class="sidebar-user-avatar">
<a href="{{ url('admin/profile') }}">

@if ($user->profile_img)
<img src="{{ url('') }}/upload/profileimages/{{$user->profile_img}}" class="w-50 rounded-circle" alt="{{$user->name}}">
@else
<img src="{{ url('') }}/assets/admin/img/placeholders/avatars/avatar2.jpg" alt="{{$user->name}}">
@endif
</a>
</div>
<div class="sidebar-user-name">{{$user->name}}</div>
<div class="sidebar-user-links">
<a href="{{ url('admin/profile') }}" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>


<a href="{{ url('admin/logout') }}" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
</div>
</div>
<!-- END User Info -->



<!-- Sidebar Navigation -->
<ul class="sidebar-nav">
<li>
<a href="{{ url('') }}/admin/dashboard"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard Website</span></a>
</li>
<li>
    <a href="{{ url('') }}/admin/dashboard"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Website Backend</span></a>
    </li>
    <li>
        <a href="{{ url('') }}/admin/dashboard"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard Business</span></a>
        </li>
<li>
<a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-log_book sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Booking management</span></a>
<ul>
        <li>
            <a class="sub-menu" href="{{ url('') }}/admin/add-paytype">Add New Payment Type</a>
            </li>
            <li>
                    <a class="sub-menu" href="{{ url('') }}/admin/all-paytype">All Payment Type</a>
                </li>
    <li>
        <a class="sub-menu" href="{{ url('') }}/admin/add-booking">Add New Booking</a>
    </li>
    <li>
        <a class="sub-menu" href="{{ url('') }}/admin/all-booking">All Booking</a>
    </li>
    
</ul>
</li>
<li>
<a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-globe sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Destination Management</span></a>
<ul>
<li>
    <a class="sub-menu" href="{{ url('') }}/admin/add-destination">Add New Destination</a>
</li>
<li>
    <a class="sub-menu" href="{{ url('') }}/admin/all-destination">All Destinations</a>
</li>

</ul>
</li>
<li>
<a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-google_maps sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Tour Management</span></a>
<ul>
        <li>
                <a class="sub-menu" href="{{ url('') }}/admin/add-tour-type">Add New Tour Type</a>
            </li>
            <li>
                <a class="sub-menu" href="{{ url('') }}/admin/all-tour-type">All Tour Type</a>
            </li>
    <li>
        <a class="sub-menu" href="{{ url('') }}/admin/add-tour">Add New Tour</a>
    </li>
    <li>
        <a class="sub-menu" href="{{ url('') }}/admin/all-tour">All Tours</a>
    </li>
    
</ul>
</li>
<li>
<a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-link  sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Booking Channel Management</span></a>
<ul>
<li>
    <a class="sub-menu" href="{{ url('') }}/admin/add-channel">Add New Channel</a>
</li>
<li>
    <a class="sub-menu" href="{{ url('') }}/admin/all-channel">All Channels</a>
</li>

</ul>
</li>
<li>
<a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-money  sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Coupons Management</span></a>
<ul>
<li>
    <a class="sub-menu" href="{{ url('') }}/admin/add-coupon">Add New Coupon</a>
</li>
<li>
    <a class="sub-menu" href="{{ url('') }}/admin/all-coupon">All Coupons</a>
</li>

</ul>
</li>
<li>
<a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-group  sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Customers Management</span></a>
<ul>
    <li>
        <a class="sub-menu" href="{{ url('') }}/admin/add-customer">Add New Customer</a>
    </li>
    <li>
        <a class="sub-menu" href="{{ url('') }}/admin/all-customer">All Customers</a>
    </li>
    
</ul>
</li>
<li>
    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user  sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Storyteller Management</span></a>
    <ul>
        <li>
            <a class="sub-menu" href="{{ url('') }}/admin/add-storyteller">Add New Storyteller</a>
        </li>
        <li>
            <a class="sub-menu" href="{{ url('') }}/admin/all-storyteller">All Storyteller</a>
        </li>
        
    </ul>
</li>

</ul>
<!-- END Sidebar Navigation -->


</div>
<!-- END Sidebar Content -->
</div>
<!-- END Wrapper for scrolling functionality -->
</div>
<!-- END Main Sidebar -->