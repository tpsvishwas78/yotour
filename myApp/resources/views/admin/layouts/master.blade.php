<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Admin Panel</title>

        <meta name="description" content="">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{ url('') }}/assets/admin/css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{ url('') }}/assets/admin/css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{ url('') }}/assets/admin/css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{ url('') }}/assets/admin/css/themes.css">
        <!-- END Stylesheets -->
<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="{{ url('') }}/assets/admin/js/vendor/jquery.min.js"></script>
        <!-- Modernizr (browser feature detection library) -->
        <script src="{{ url('') }}/assets/admin/js/vendor/modernizr.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEWv2phNEucxoajL9_UuBpSG7OLVVlUww&libraries=places"></script>
    </head>
    <body>
        <!-- Page Wrapper -->
        <!-- In the PHP version you can set the following options from inc/config file -->
        <!--
            Available classes:

            'page-loading'      enables page preloader
        -->
        <div id="page-wrapper">
            <!-- Preloader -->
            <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
            <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>Yo</strong>Tour</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie10"></div>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            
            <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
                

                @include('admin.layouts.sidebar')
                <!-- Main Container -->
<div id="main-container">
                @include('admin.layouts.header')
                @yield('content')
              </div>
              <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        

        
        <script src="{{ url('') }}/assets/admin/js/vendor/bootstrap.min.js"></script>
        <script src="{{ url('') }}/assets/admin/js/plugins.js"></script>
        <script src="{{ url('') }}/assets/admin/js/app.js"></script>


       
        <!-- Load and execute javascript code used only in this page -->
<script src="{{ url('') }}/assets/admin/js/pages/formsValidation.js"></script>
<script>$(function() { FormsValidation.init(); });</script>
<script src="{{ url('') }}/assets/admin/js/helpers/ckeditor/ckeditor.js"></script>
<script src="{{ url('') }}/assets/admin/js/pages/tablesDatatables.js"></script>

        <script>$(function(){ TablesDatatables.init(); });</script>

        <script>
        var str = window.location.href,
    delimiter = '/',
    start = 5,
    tokens = str.split(delimiter, 6);//.slice(start);
    //console.log('tokens',tokens);
    URLs = tokens.join(delimiter);

    //console.log('URLs',URLs);

  $(".sub-menu").each(function(idx, val) {    
    if($(this).attr('href') == URLs)
    {
      $(this).parent().parent().addClass('in');
      $(this).parent().parent().parent().addClass('active');
      $(this).parent().parent().parent().parent().className;
      $(this).parent().addClass('active'); 
    }
  });
        </script>
    </body>
</html>