@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-user"></i>Profile
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><strong>Update</strong> Profile</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/updateprofile')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        <legend><i class="fa fa-angle-right"></i> Basic Info</legend>
        @if(Session::has('success'))

                <div class="alert alert-success">
        
                    {{ Session::get('success') }}
        
                    @php
        
                        Session::forget('success');
        
                    @endphp
        
                </div>
        
                @endif

                @if(Session::has('warning'))

                <div class="alert alert-warning">
        
                    {{ Session::get('warning') }}
        
                    @php
        
                        Session::forget('warning');
        
                    @endphp
        
                </div>
        
                @endif
                @csrf
                                    
        <div class="form-group">
            <label class="col-md-4 control-label" for="val_username">Profile Photo </label>
            <div class="col-md-6">
                @if ($userdata->profile_img)
                <img src="{{ url('') }}/upload/profileimages/{{$userdata->profile_img}}" class="w-50 rounded-circle" alt="{{$userdata->first_name}}">
                @else
                <img src="{{ url('') }}/assets/admin/img/placeholders/avatars/avatar2.jpg" class="w-50 rounded-circle" alt="{{$userdata->first_name}}">
                @endif
                
                <div class="file-upload">
                    <label for="upload" class="btn btn-primary m-b-0 m-l-5 m-r-5">Upload a new picture</label>
                    <input id="upload" style="display: none;" class="file-upload__input" type="file" name="image">
                    
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name <span class="text-danger">*</span></label>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" id="name" name="name" class="form-control" value="{{ $userdata->name }}">
                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                </div>
                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email <span class="text-danger">*</span></label>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" id="email" name="email" class="form-control"  value="{{ $userdata->email }}">
                    <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                </div>
                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
            </div>
        </div>
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection