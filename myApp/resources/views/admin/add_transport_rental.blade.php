@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Product Management
</h1>
</div>
</div>
<style>
#pickupmap {
    height: 300px;
    display: none;
}
#dropmap {
    height: 300px;
    display: none;
}
.btn-default.active,.btn-default.active.focus,.btn-default.active:hover{
    background-color: #00BCD4;
    border-color: #00BCD4;
    color: #fff;
}

.btn-default,.btn-default:focus{
    background-color: #eaedf1;
    border-color: #eaedf1;
    color: #000;
}
.btn-group .btn+.btn{
    margin-left: 5px;
}
</style>
<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submittransportrental')}}" enctype="multipart/form-data" class="form-horizontal form-bordered-1">
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Transport/Rental</h2>
    <div class="pull-right" style="margin: 1px 10px;">
        <button type="submit"  data-toggle="tooltip"  class="btn btn-primary" data-original-title="Save"><i class="fa fa-save"></i> Save</button>
       
</div>
</div>
<!-- END Form Validation Example Title -->

    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                
    <!-- Block Tabs Title -->
    <div class="block-title">
        <ul class="nav nav-tabs" data-toggle="tabs">
            <li class="active"><a href="#tabs1">General</a></li>
            <li><a href="#tabs2">Inclusions</a></li>
            <li><a href="#tabs3">Duration</a></li>
            <li><a href="#tabs4">Time Slot</a></li>
            <li><a href="#tabs5">Cut Off</a></li>
            <li><a href="#tabs6">Pricing</a></li>
            <li><a href="#tabs7">Policy</a></li>
            <li><a href="#tabs8">Info</a></li>
        </ul>
    </div>
    <!-- END Block Tabs Title -->

    <div class="tab-content">
        <div class="tab-pane active" id="tabs1">
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Destination </label>
                <div class="col-md-3">
                        <select name="country" id="country" class="form-control select-select2" style="width:100% !important;">
                            <option value="">Select Country</option>
                            @foreach ($countries as $country)
                            <option @if (@$edit->country==$country->id)
                                selected
                            @endif value="{{ $country->id }}">{{ $country->name }}</option> 
                            @endforeach
                            
                        </select>
                        
                    @if ($errors->has('country'))
                          <span class="text-danger">{{ $errors->first('country') }}</span>
                     @endif
                </div>
<?php
@$cities=\DB::table('destinations')->where('country_id',@$edit->country)->get();
?>
                <div class="col-md-3">
                    <select name="city" id="city" class="form-control select-select2" style="width:100% !important;">
                        <option value="">Select City</option>
                        @foreach ($cities as $city)
                        <option @if ($edit->city==$city->id)
                            selected
                        @endif value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                        
                    </select>
                    
                @if ($errors->has('city'))
                    <span class="text-danger">{{ $errors->first('city') }}</span>
                 @endif
            </div>
            </div> 

            

            <div class="form-group">
                    <label class="col-md-4 control-label" for="transport_category">Select Category</label>
                    
                    <div class="col-md-3">
                        <span>For Transport</span>
                            <select name="transport_category" id="transport_category" class="form-control select-select2" style="width:100% !important;">
                                <option value="">Select</option>
                                @foreach ($transportcats as $transportcat)
                            <option @if (@$edit->transport_category==$transportcat->id)
                                selected
                            @endif value="{{ $transportcat->id }}">{{ $transportcat->name }}</option> 
                            @endforeach
                               
                            </select>
                        @if ($errors->has('transport_category'))
                            <span class="text-danger">{{ $errors->first('transport_category') }}</span>
                        @endif
                    </div>
<?php
@$rentcats=\DB::table('product_categories')->where('parent',@$edit->transport_category)->get();
?>
                    <div class="col-md-3">
                        <span>For Rental</span>
                        <select name="rent_category" id="rent_category" class="form-control select-select2" style="width:100% !important;">
                            <option value="">Select</option>
                            @foreach ($rentcats as $rentcat)
                            <option @if (@$edit->rent_category==$rentcat->id)
                                selected
                            @endif value="{{ $rentcat->id }}">{{ $rentcat->name }}</option> 
                            @endforeach
                           
                        </select>
                    @if ($errors->has('rent_category'))
                        <span class="text-danger">{{ $errors->first('rent_category') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="rent_type">Type</label>
                <div class="col-md-6">
                        <input type="text" id="rent_type" name="rent_type" class="form-control" value="{{ @$edit->rent_type }}">
                        <span>Ex:- hatchback,sedan etc.</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="minimum_age">Minimum Age</label>
                <div class="col-md-6">
                        <input type="text" id="minimum_age" name="minimum_age" class="form-control" value="{{ @$edit->minimum_age }}">
                        @if ($errors->has('minimum_age'))
                        <span class="text-danger">{{ $errors->first('minimum_age') }}</span>
                    @endif
                </div>
            </div> 
            <div class="form-group">
                <label class="col-md-4 control-label" for="product_title">Product Title </label>
                <div class="col-md-6">
                        <input type="text" id="product_title" name="product_title" class="form-control" value="{{ @$edit->product_name }}">
                        
                    @if ($errors->has('product_title'))
                        <span class="text-danger">{{ $errors->first('product_title') }}</span>
                    @endif
                </div>
            </div> 

            <?php
            @$images=\DB::table('product_images')->where('product_id',@$edit->id)->get();
            ?>
            <div class="form-group">
                <label class="col-md-4 control-label" for="gallary">Select Photos & Video </label>
                <div class="col-md-6">
                    <div class="controls">
                        <div class="entry input-group col-xs-3">
                          <input class="btn btn-default" name="gallary[]" type="file">
                          <span class="input-group-btn">
                        <button class="btn btn-success btn-add" type="button">
                                <span class="gi gi-plus"></span>
                          </button>
                          </span>
                        </div>
                    </div>
                    @if (@$images)
    @foreach (@$images as $image)
            <p>
            <img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
            <a href="javascript:void(0);"><i id="{{ $image->id }}" class="gi gi-delete removeimg"></i></a> 
            </p>
    @endforeach
    @endif
                  <input  style="margin-top: 20px;" type="text" name="video_link" placeholder="Youtube video link" class="form-control" value="{{ @$edit->video_link }}">      
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="featured_img">Featured Image </label>
                <div class="col-md-6">
                    <input type="file" id="featured_img" name="featured_img" class="form-control">
                    @if (@$edit->featured_img)
                    <p>
                    <img width="100" src="{{ url('') }}/upload/images/{{ @$edit->featured_img }}" alt="">
                    </p> 
                    @endif
                </div>
            </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="pickup">Meeting Point, Pick up & Drop Off </label>
            <div class="col-md-6">
                <input type="text" id="pickup" name="pickup" class="form-control" value="{{ @$edit->pickup }}" placeholder="Pick Up Location">
                <input type="hidden" name="pickup_latitude" id="pickup_latitude" value="{{ @$edit->pickup_latitude }}">
                <input type="hidden" name="pickup_longitude" id="pickup_longitude" value="{{ @$edit->pickup_longitude }}">
                <div class="ground-map">
                    <div id="pickupmap"></div>
                </div>
            </div>
            
            <label class="col-md-4 control-label" for="dropoff"></label>
            <div class="col-md-6" style="margin-top: 30px;">
                <input type="text" id="dropoff" name="dropoff" class="form-control" value="{{ @$edit->dropoff }}" placeholder="Drop Off Location">
                <input type="hidden" name="drop_latitude" id="drop_latitude" value="{{ @$edit->drop_latitude }}">
                <input type="hidden" name="drop_longitude" id="drop_longitude" value="{{ @$edit->drop_longitude }}">
                <div class="ground-map">
                    <div id="dropmap"></div>
                </div>
            </div>

            
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="home_delivery">Home Delivery</label>
            <div class="col-md-6">
                <input type="text" id="home_delivery" name="home_delivery" class="form-control" value="{{ @$edit->home_delivery }}">
            </div>
        </div>
        </div>
        <div class="tab-pane" id="tabs2">
            <?php
            @$inclusions=\DB::table('product_inclusions_exclusions')->where('product_id',@$edit->id)->first();
            ?>                      
            <!-- Start Inclusions & Exclusions Block --> 
            <div class="block">           
                <div class="block-title">
                    <h2>Inclusions & Exclusions</h2>
                </div>
            <fieldset>
                
                <div class="form-group">
                    <label class="col-md-4 control-label" for="inclusion_category">Category</label>
                    <div class="col-md-6">
                            <select name="inclusion_category" id="inclusion_category" class="form-control select-select2" style="width:100% !important;">
                                <option value="">Select</option>
                                @foreach ($inclusionscats as $inclusionscat)
                                    <option @if (@$inclusions->inclusion_category==$inclusionscat->id)
                                        selected
                                    @endif value="{{ $inclusionscat->id }}">{{ $inclusionscat->name }}</option> 
                                @endforeach
                            </select>
                    </div>
                </div>
                <?php
                    @$inclusionsubcategorys=\DB::table('product_categories')->where('parent',$inclusions->inclusion_category)->get();
                ?>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="inclusion_sub_category">Sub Category</label>
                    <div class="col-md-6">
                            <select name="inclusion_sub_category[]" id="inclusion_sub_category" class="form-control select-select2" multiple style="width:100% !important;">
                                <?php @$inclusionsubArray = unserialize(@$inclusions->inclusion_sub_category);?>
            
                                @foreach ($inclusionsubcategorys as $inclusionsubcategory)
                                    <option @if (@in_array($inclusionsubcategory->id,@$inclusionsubArray))
                                        selected
                                    @endif value="{{ $inclusionsubcategory->id }}">{{ $inclusionsubcategory->name }}</option>
                                @endforeach
                               
                            </select>
                       
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-md-4 control-label" for="inclusion_description">Description </label>
                    <div class="col-md-6">
                            <input type="text" id="inclusion_description" name="inclusion_description" class="form-control" value="{{ @$inclusions->inclusion_description }}">
                    </div>
                </div>
                <hr>
                
                <div class="form-group">
                    <label class="col-md-4 control-label" for="exclusion_category">Category</label>
                    <div class="col-md-6">
                            <select name="exclusion_category" id="exclusion_category" class="form-control select-select2" style="width:100% !important;">
                                <option value="">Select</option>
                                @foreach ($inclusionscats as $inclusionscat)
                                <option @if (@$inclusions->exclusion_category==$inclusionscat->id)
                                    selected
                                @endif value="{{ $inclusionscat->id }}">{{ $inclusionscat->name }}</option> 
                                @endforeach
                            </select>
                    </div>
                </div> 
                <?php
                    @$exclusionsubcategorys=\DB::table('product_categories')->where('parent',$inclusions->exclusion_category)->get();
                ?>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="exclusion_sub_category">Sub Category</label>
                    <div class="col-md-6">
                            <select name="exclusion_sub_category[]" id="exclusion_sub_category" class="form-control select-select2" multiple style="width:100% !important;">
                                <?php @$exclusionsubArray = unserialize(@$inclusions->exclusion_sub_category);?>
            
                                @foreach ($exclusionsubcategorys as $exclusionsubcategory)
                                <option @if (@in_array($exclusionsubcategory->id,@$exclusionsubArray))
                                    selected
                                @endif value="{{ $exclusionsubcategory->id }}">{{ $exclusionsubcategory->name }}</option>
                            @endforeach
                            </select>
                       
                    </div>
                </div>  
                <div class="form-group">
                    <label class="col-md-4 control-label" for="exclusion_description">Description </label>
                    <div class="col-md-6">
                            <input type="text" id="exclusion_description" name="exclusion_description" class="form-control" value="{{ @$inclusions->exclusion_description }}">
                    </div>
                </div>
            <input type="hidden" name="inclusion_id" value="{{ @$inclusions->id }}">
                
            </fieldset>
        
</div>
<!-- END Inclusions & Exclusions Block -->  
        </div>
        <div class="tab-pane" id="tabs3">
            <?php
            @$duration=\DB::table('product_time_duration_slot')->where('product_id',@$edit->id)->first();
            ?>   
<!-- Start Tour Duration Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Rental Duration</h2>
    </div>
    <fieldset>
        <input type="hidden" name="tour_duration_id" value="{{ @$duration->id }}">
            <div class="form-group">
                <label class="col-md-4 control-label" for="tour_duration">Tour Duration</label>
                <div class="col-md-6">
                    <label class="radio-inline" for="tour_duration_fixed">
                        <input type="radio" class="tour_duration"  id="tour_duration_fixed" name="tour_duration" value="Fixed" @if (@$duration->tour_duration=='Fixed' || @$duration->tour_duration=='')
                            checked
                        @endif>Fixed
                    </label>
                    <label class="radio-inline" for="tour_duration_flexible">
                        <input type="radio" class="tour_duration" id="tour_duration_flexible" name="tour_duration" value="Flexible" @if (@$duration->tour_duration=='Flexible')
                        checked
                    @endif>Flexible
                    </label>
                </div>
            </div>
        <div id="fixed_timing" @if (@$duration->tour_duration=='Fixed' || @$duration->tour_duration=='')
            style="display:block;" 
            @else style="display:none;" 
        @endif>
            <div class="form-group">
                <label class="col-md-4 control-label" for="fixed_duration">Fixed (Minutes/Hour)</label>
                <div class="col-md-6">
                    <input type="text" id="fixed_duration" name="fixed_duration" class="form-control" value="{{ @$duration->fixed_duration }}">
                </div>
            </div>
        </div>
        <div id="flexible_timing" @if (@$duration->tour_duration=='Flexible')
            style="display:block;" 
            @else style="display:none;" 
        @endif>
            <div class="form-group">
                <label class="col-md-4 control-label" for="flexible_duration_minus">Flexible (Minutes)</label>
                <div class="col-md-6">
                    <input type="text" id="flexible_duration_minus" name="flexible_duration_minus" class="form-control" value="{{ @$duration->flexible_duration_minus }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="flexible_duration_hour">Flexible (Hour)</label>
                <div class="col-md-6">
                    <input type="text" id="flexible_duration_hour" name="flexible_duration_hour" class="form-control" value="{{ @$duration->flexible_duration_hour }}">
                </div>
            </div>
        </div>
        
    </fieldset>

</div>
<!-- END Tour Duration Block --> 
        </div>
        <div class="tab-pane" id="tabs4">
<!-- Start Tour Time Slot Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Tour Time Slot</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="start_booking_date">From when you want to start taking bookings?        </label>
        <div class="col-md-6">
            <input type="text" id="example-datepicker1" name="start_booking_date" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$duration->start_booking_date }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="time_slot_days">Days when this tour is bookable</label>
        <div class="col-md-6">
            <?php @$days = unserialize(@$duration->time_slot_days);?>
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default @if (@in_array(1,@$days)) active @endif">
                    <input @if (@in_array(1,@$days))
                        checked
                    @endif type="checkbox" name="time_slot_days[]" value="1">Mon
                </label>
                <label class="btn btn-default @if (@in_array(2,@$days)) active @endif">
                    <input @if (@in_array(2,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="2"> Tue
                </label>
                <label class="btn btn-default @if (@in_array(3,@$days)) active @endif">
                    <input @if (@in_array(3,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="3"> Wed
                </label>
                <label class="btn btn-default @if (@in_array(4,@$days)) active @endif">
                    <input @if (@in_array(4,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="4"> Thu
                </label>
                <label class="btn btn-default @if (@in_array(5,@$days)) active @endif">
                    <input @if (@in_array(5,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="5"> Fri
                </label>
                <label class="btn btn-default @if (@in_array(6,@$days)) active @endif">
                    <input @if (@in_array(6,@$days))
                    checked
                @endif type="checkbox" name="time_slot_days[]" value="6"> Sat
                </label>
                <label class="btn btn-default @if (@in_array(7,@$days)) active @endif">
                    <input @if (@in_array(7,@$days))
                    checked
                @endif  type="checkbox" name="time_slot_days[]" value="7"> Sun
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label" for="specific_booking_date">Do you want to end taking bookings on a specific date? (optional)
        </label>
        <div class="col-md-6">
            <input type="text" id="example-datepicker2" name="specific_booking_date" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$duration->specific_booking_date }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="specific_block_booking_date">Do you want to block specific dates for booking tours?  (optional)
        </label>
        <div class="col-md-6">
            <input type="text" id="example-datepicker3" name="specific_block_booking_date" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$duration->specific_block_booking_date }}">
        </div>
    </div>

    
</fieldset>

</div>
<!-- END Tour Time Slot Block --> 
        </div>
        <div class="tab-pane" id="tabs5">

<!-- Start Cut Off Time Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Cut Off Time</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="cut_off_time">How would you like to set your cut-off time?
            
        </label>
        <div class="col-md-6">
            <span>Hours before the rental start time</span>
            <input type="text" id="hour_before_tour_time" name="hour_before_tour_time" class="form-control" value="{{ @$duration->hour_before_tour_time }}">
        </div>
    </div>
   
    
</fieldset>

</div>
<!-- END Cut Off Time Block -->
        </div>
        <div class="tab-pane" id="tabs6">
            <?php
            @$pricing=\DB::table('product_pricing')->where('product_id',@$edit->id)->first();
            ?> 
<!-- Start Pricing Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Pricing</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="currency">Select Currency</label>
        <div class="col-md-6">
                <select name="currency" id="currency" class="form-control select-select2" style="width:100% !important;">
                    <option @if (@$pricing->currency=='INR')
                        selected
                    @endif value="INR">INR</option>
                    <option @if (@$pricing->currency=='EUR')
                        selected
                    @endif value="EUR">EUR</option>
                </select>
        </div>
    </div>
    
    
    <div class="form-group">
        <label class="col-md-4 control-label" for="pricing_type">Select Pricing</label>
        <div class="col-md-6">
            <label class="radio-inline" for="pricing_type_person">
                <input @if (@$pricing->pricing_type=='Per Person')
                checked
                @endif type="radio" id="pricing_type_person" name="pricing_type" value="Per Person">Per Person
            </label>
            <label class="radio-inline" for="pricing_type_vehicle">
                <input @if (@$pricing->pricing_type=='Per Vehicle')
                checked
            @endif type="radio" id="pricing_type_vehicle" name="pricing_type" value="Per Vehicle">Per Vehicle
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="pricing_options">How many pricing options do you have for this product?
</label>
        <div class="col-md-6">
            <label class="radio-inline" for="pricing_options_one">
                <input @if (@$pricing->pricing_options=='One')
                checked
            @endif type="radio" id="pricing_options_one" name="pricing_options" value="One">One </label>
            <label class="radio-inline" for="pricing_options_more">
                <input @if (@$pricing->pricing_options=='More than one')
                checked
            @endif type="radio" id="pricing_options_more" name="pricing_options" value="More than one">More than one
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="add_attributes">Add attributes that apply to this option
           (Name this Option) </label>
        <div class="col-md-6">
            <input type="text" id="add_attributes" name="add_attributes" class="form-control" value="{{ @$pricing->add_attributes }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="price_for_tour">Price for the Rental
        </label>
        <div class="col-md-6">
           
                Minimum person required?
                <input type="text" id="min_person_required" name="min_person_required" class="form-control" value="{{ @$pricing->min_person_required }}">
                <br>

                <label class="col-md-4 control-label" for="adult_price">Adult Price</label>
                 <div class="col-md-6">
                     <input type="text" id="adult_price" name="adult_price" class="form-control" value="{{ @$pricing->adult_price }}">
                 </div>
                 <label class="col-md-4 control-label" for="child_price">Child Price</label>
                 <div class="col-md-6">
                     <input type="text" id="child_price" name="child_price" class="form-control" value="{{ @$pricing->child_price }}">
                 </div>
                 <label class="col-md-4 control-label" for="youth_price">Youth Price</label>
                 <div class="col-md-6">
                     <input type="text" id="youth_price" name="youth_price" class="form-control" value="{{ @$pricing->youth_price }}">
                 </div>

                 <br><br>
                
                    <label class="col-md-4 control-label" for="deposit">Deposit (if any)</label>
                    <div class="col-md-6">
                        <input type="text" id="deposit" name="deposit" class="form-control" value="{{ @$pricing->deposit }}">
                    </div>
               
                 
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="tax_on_pricing">Tax on Pricing</label>
        <div class="col-md-6">
            <div class="col-md-12">
            <span>Name tax </span>
            <input type="text" id="tax_name" name="tax_name" class="form-control" value="{{ @$pricing->tax_name }}">
        </div>
        <div class="col-md-12">
            <span>Tax Rate</span>
            <input type="text" id="tax_rate" name="tax_rate" class="form-control" value="{{ @$pricing->tax_rate }}">
        </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="pricing_days">Days when these pricing are applicable
        </label>
        <div class="col-md-6">
            <?php @$pricingdays = unserialize(@$pricing->pricing_days);?>
            

            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default @if (@in_array(1,@$pricingdays)) active @endif">
                    <input @if (@in_array(1,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="1">Mon
                </label>
                <label class="btn btn-default @if (@in_array(2,@$pricingdays)) active @endif">
                    <input @if (@in_array(2,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="2"> Tue
                </label>
                <label class="btn btn-default @if (@in_array(3,@$pricingdays)) active @endif">
                    <input @if (@in_array(3,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="3"> Wed
                </label>
                <label class="btn btn-default @if (@in_array(4,@$pricingdays)) active @endif">
                    <input @if (@in_array(4,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="4"> Thu
                </label>
                <label class="btn btn-default @if (@in_array(5,@$pricingdays)) active @endif">
                    <input @if (@in_array(5,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="5"> Fri
                </label>
                <label class="btn btn-default @if (@in_array(6,@$pricingdays)) active @endif">
                    <input @if (@in_array(6,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="6"> Sat
                </label>
                <label class="btn btn-default @if (@in_array(7,@$pricingdays)) active @endif">
                    <input @if (@in_array(7,@$pricingdays)) checked @endif type="checkbox" name="pricing_days[]" value="7"> Sun
                </label>
            </div>
        </div>
    </div>
</fieldset>

</div>
<!-- END Pricing Block -->

        </div>
        <div class="tab-pane" id="tabs7">

<!-- Start Cancellation Policy Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Cancellation Policy</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="cancellation_policy">Select your cancellation policy</label>
        <div class="col-md-6">
            <label class="radio-inline" for="cancellation_policy_24">
                <input @if (@$edit->cancellation_policy=='1') checked @endif type="radio" id="cancellation_policy_24" name="cancellation_policy" value="1">24 Hours (Recommended)
            </label>
            <label class="radio-inline" for="cancellation_policy_non">
                <input @if (@$edit->cancellation_policy=='0') checked @endif type="radio" id="cancellation_policy_non" name="cancellation_policy" value="0">Non-refundable
            </label>
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Cancellation Policy Block -->

        </div>
        <div class="tab-pane" id="tabs8">

<!-- Start Additional Info Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Additional Info</h2>
    </div>
<fieldset>
   
    <div class="form-group">
        <label class="col-md-4 control-label" for="additional_info">Additional Info
        </label>
        <div class="col-md-6">
            <input type="text" id="additional_info" name="additional_info" class="form-control" value="{{ @$edit->additional_info }}">
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Additional Info Block -->
        </div>
    </div>
</fieldset>

</div>
<!-- END Cancellation Policy Block -->

        <input type="hidden" name="id" value="{{ @$edit->id }}">
    
</form>
<!-- END Form Validation Example Content -->


</div>

</div>
</div>
<script>
    $(document).on('change', '.tour_duration', function(e)
{
    var ductionType=$(this).val();
    if(ductionType=='Fixed'){
        $('#flexible_timing').hide();
        $('#fixed_timing').show();
    }else{
        $('#fixed_timing').hide();
        $('#flexible_timing').show();
    }

});
$(document).on('change', '#country', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getcitybycountry",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#city').html(data);
                 }
                });
    
});
$(document).on('change', '#transport_category', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getrentcategory",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#rent_category').html(data);
                 }
                });
    
});
$(document).on('change', '#inclusion_category', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getinclusionsubcate",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#inclusion_sub_category').html(data);
                 }
    });
    
});

$(document).on('change', '#exclusion_category', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getinclusionsubcate",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#exclusion_sub_category').html(data);
                 }
    });
    
});

    $(document).on('click', '.removeimg', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('p').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deleteproductgalleryimage",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
 $(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="gi gi-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});


google.maps.event.addDomListener(window, 'load', function () {
          var options = {
			//types: ['(cities)'],
			};
            var places = new google.maps.places.Autocomplete(document.getElementById('pickup'),options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var lat = places.getPlace().geometry.location.lat();
                var lng = places.getPlace().geometry.location.lng();
                    $('#pickup_latitude').val(lat);
                    $('#pickup_longitude').val(lng);

                    var map = new google.maps.Map(document.getElementById('pickupmap'), {
            center: {lat: lat, lng: lng},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var marker = new google.maps.Marker({map: map,draggable:true, position: {lat: lat, lng: lng}});
          google.maps.event.addListener(marker, 'dragend', function() 
            {
                //console.log(marker.getPosition().lat());
                    $('#pickup_latitude').val(marker.getPosition().lat());
                    $('#pickup_longitude').val(marker.getPosition().lng());
            });

          if(lat && lng){
                $('#pickupmap').show();
            }
            });
            
            

            var dropplaces = new google.maps.places.Autocomplete(document.getElementById('dropoff'),options);
            google.maps.event.addListener(dropplaces, 'place_changed', function () {
                var droplat = dropplaces.getPlace().geometry.location.lat();
                var droplng = dropplaces.getPlace().geometry.location.lng();
                    $('#drop_latitude').val(droplat);
                    $('#drop_longitude').val(droplng);

            var dropmap = new google.maps.Map(document.getElementById('dropmap'), {
            center: {lat: droplat, lng: droplng},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var dropmarker=new google.maps.Marker({map: dropmap,draggable:true, position: {lat: droplat, lng: droplng}});
          google.maps.event.addListener(dropmarker, 'dragend', function() 
            {
                //console.log(marker.getPosition().lat());
                    $('#drop_latitude').val(dropmarker.getPosition().lat());
                    $('#drop_longitude').val(dropmarker.getPosition().lng());
            });
          if(droplat && droplng){
                $('#dropmap').show();
            }
            });
        });
</script>
@endsection