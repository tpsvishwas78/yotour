@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Setting
</h1>

</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2>All Product Categories</h2>
    <div class="addbutton">
        <a class="btn btn-info" href="{{ url('') }}/admin/add-category"><i class="gi gi-plus"></i> Add New Category</a>
    </div>
</div>
<!-- END Form Validation Example Title -->
@if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif

                <div class="table-responsive">
                        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">SNO</th>
                                    <th>Name</th>
                                    <th>Parent</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($results as $key => $result)
                                <?php
                                @$parent=\DB::table('product_categories')->where('id',$result->parent)->first();
                                                                    ?>                
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    
                                    <td>{{ @$result->name }}</td>
                                    <td>{{ @$parent->name }}</td>
                                    <td>
                                        @switch(@$result->type)
                                            @case(1)
                                            Tour
                                                @break
                                            @case(2)
                                            Activity
                                                @break
                                            @case(3)
                                            Tickets
                                                @break
                                            @case(4)
                                            Transport
                                                @break
                                            @case(5)
                                            Rental 
                                                @break
                                            @case(6)
                                            Services
                                                @break
                                            @case(7)
                                            Inclusions/Exclusions
                                                @break
                                           
                                            @default
                                            Holiday
                                        @endswitch
                                    </td>
                                    <td class="text-center">
                                        @if (@$result->status==1)
                                        <a href="{{ url('') }}/admin/status-category/{{ $result->id }}" class="label label-success" onClick="return confirm('Are you sure you want to Deactive?');">Active</a>
                                        @else
                                        <a onClick="return confirm('Are you sure you want to Active?');" href="{{ url('') }}/admin/status-category/{{ $result->id }}" class="label label-danger">Deactive</a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ url('') }}/admin/add-category/{{ $result->id }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            <a onClick="return confirm('Are you sure you want to Delete?');" href="{{ url('') }}/admin/delete-category/{{ $result->id }}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection