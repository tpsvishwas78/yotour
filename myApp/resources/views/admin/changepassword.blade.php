@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-user"></i>Change Password
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><strong>Change</strong>  Password</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/changepasswordsubmit')}}" class="form-horizontal form-bordered">
    <fieldset>
        <legend><i class="fa fa-angle-right"></i> Change Password</legend>
        @if(Session::has('success'))

                <div class="alert alert-success">
        
                    {{ Session::get('success') }}
        
                    @php
        
                        Session::forget('success');
        
                    @endphp
        
                </div>
        
                @endif

                @if(Session::has('warning'))

                <div class="alert alert-warning">
        
                    {{ Session::get('warning') }}
        
                    @php
        
                        Session::forget('warning');
        
                    @endphp
        
                </div>
        
                @endif
                @csrf
                                    
    
        <div class="form-group">
            <label class="col-md-4 control-label" for="old_password">Old Password <span class="text-danger">*</span></label>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" id="old_password" name="old_password" class="form-control">
                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                </div>
                @if ($errors->has('old_password'))
                                    <span class="text-danger">{{ $errors->first('old_password') }}</span>
                                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">New Password <span class="text-danger">*</span></label>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" id="password" name="password" class="form-control">
                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                </div>
                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="confirm_password">Confirm Password <span class="text-danger">*</span></label>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" id="confirm_password" name="confirm_password" class="form-control">
                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                </div>
                @if ($errors->has('confirm_password'))
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                @endif
            </div>
        </div>
        
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection