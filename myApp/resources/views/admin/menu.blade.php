@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Setting
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Category</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{ url('admin/submitMenu') }}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group row col-sm-6">
                    <label for="fname" class="col-sm-3 text-left control-label col-form-label">Add Menu</label>
                    <div class="col-sm-9">
                        <input type="text" name="menu_name" value="{{ (isset($edit) and $edit->menu_name!='') ? $edit->menu_name:''}}"  class="form-control" id="contact_no" placeholder="Menu Name">
                        <span class="text-danger">@if (isset($errors) and @$errors->first('menu_name'))
                            {{$errors->first('menu_name')}}
                        @endif
                        
                        </span>
                    </div>
                    </div>
                    <div class="form-group row col-sm-6">
                        <label for="email1" class="col-sm-3 text-left control-label col-form-label">Menu Order</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" value="{{ (isset($edit) and $edit->menu_order!='') ? $edit->menu_order:''}}"  name="menu_order" id="menuorder" placeholder="Menu Order">
                        </div>
                        </div>
                    <div class="form-group row col-sm-6">
                    <label for="lname" class="col-sm-3 text-left control-label col-form-label">Select Parent</label>
                    <div class="col-sm-9">
                        <select class="select2 form-control custom-select" name="parent_id">
                            <option value="0">Select</option>
                            @foreach ($parentmenu as $parentmenu)
                            <option value="{{$parentmenu->id}}" @if (@$edit->parent_id==$parentmenu->id) {{'selected'}}   @endif> {{$parentmenu->menu_name}}</option>
                          @endforeach
                           
                        </select>
                    </div>
                    </div>
                    
                    <div class="form-group row col-sm-6">
                    <label for="lname" class="col-sm-3 text-left control-label col-form-label">Menu Location</label>
                    <div class="col-sm-9">
                        <select class="select2 form-control custom-select" name="menu_location">
                            <option value="">Select Menu Location</option>
                            <option value="header" @if (@$edit->menu_location=='header') {{'selected'}}   @endif>Header</option>
                            <option value="footer" @if (@$edit->menu_location=='footer') {{'selected'}}   @endif>Footer</option>
                        </select>
                    </div>
                    </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->

<div class="table-responsive">
    <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
        <thead>
            <tr>
            <th>Sr No</th>
            <th>Menu</th>
            <th>Order</th>
            <th>Parent</th>
            <th>Location</th>
            <th>Status</th>
            <th>Action</th>
            </tr>
            </thead>
            <tbody>
    
                @foreach($menulist as $index=>$menulist)
                <?php 
                @$parmenu = \DB::table('menus')
               // ->where('parent_id', '!=', 0)
                ->where('id', $menulist->parent_id)            
                ->first();
                //dd($parmenu);
            ?> 
            
                {{-- {{dd($menulist)}} --}}
                <tr>
                <td>{{$index+1}}</td>
                <td>{{$menulist->menu_name}}</td>                           
                <td>{{$menulist->menu_order}}</td>
                <td>{{@$parmenu->menu_name ? @$parmenu->menu_name : 'No Parent'}}</td>    
                <td>{{$menulist->menu_location}}</td>
                {{-- <th><span class="badge {{ ($menulist->status=='active') ? 'badge-success ' : 'badge-danger' }}">{{$menulist->status}}</span></th>
                <td> 
              
                 @if($menulist->status=='active') 
                 <a href="javascript:void(0)" title="Deactive" id="statusid{{$menulist->id}}"  data-toggle="modal" onclick="status_confrim('deactive', {{ $menulist->id }})" ><span class="badge badge-success"><i class="mdi mdi-check"></i></span></a>
                 @else
                 <a href="javascript:void(0)" title="Active" id="statusid{{$menulist->id}}"  data-toggle="modal" onclick="status_confrim('active',{{$menulist->id}})" ><span class="badge badge-danger"><i class="mdi mdi-alert"></i></span></a>
                 @endif
                
                <a href="{{url('')}}/admin/menu/{{$menulist->id}}"><span class="badge badge-info"><i class="mdi mdi-pencil"></i></span></a>
                <a href="javascript:void(0)"  onclick="delete_confirm('{{ $menulist->id }}')"  id="deletecat{{ $menulist->id }}" data-toggle="modal"><span class="badge badge-danger"><i class="m-r-1 mdi mdi-delete"></i></span></a>
                </td> --}}
                <td class="text-center">
                    @if (@$menulist->status=='active')
                    <a href="{{url('')}}/admin/menu/menustatus/{{ $menulist->id }}" class="label label-success" onClick="return confirm('Are you sure you want to Deactive?');">Active</a>
                    @else
                    <a onClick="return confirm('Are you sure you want to Active?');" href="{{url('')}}/admin/menu/menustatus/{{ $menulist->id }}" class="label label-danger">Deactive</a>
                    @endif
                </td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{url('')}}/admin/menu/{{$menulist->id}}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                        <a onClick="return confirm('Are you sure you want to Delete?');" href="{{url('')}}/admin/menu/menudelete/{{ $menulist->id }}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                    </div>
                </td>
                </tr>                    
                @endforeach
                </tbody>
    </table>
</div>


</div>
<!-- END Validation Block -->
</div>

</div>
</div>



@endsection