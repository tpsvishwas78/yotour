@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-log_book"></i>Booking Management
</h1>
</div>
</div>

<style>
.fc-time{
    display: none;
}
.fc-title{
    font-size: 18px;
    font-weight: 600;
}
tr td > .fc-day-grid-event {
    margin-top: 2px;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    text-align: center;
    margin: 3px auto;
}
.fc-day-grid-event .fc-content {
    padding: 12px;
}
</style>

<div class="row text-center">
        <div class="col-sm-6 col-lg-4">
                <a href="{{ url('') }}/admin/upcoming-booking" class="widget widget-hover-effect2">
                    <div class="widget-extra upcoming-booking">
                        <h4 class="widget-content-light"><strong>FUTURE </strong> BOOKINGS</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 animation-expandOpen">{{ $upcomingbookingcount }}</span></div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-4">
                    <a href="{{ url('') }}/admin/unallocated-booking" class="widget widget-hover-effect2">
                        <div class="widget-extra unallocated-booking">
                            <h4 class="widget-content-light"><strong>UNALLOCATED </strong> BOOKINGS</h4>
                        </div>
                        <div class="widget-extra-full"><span class="h2 animation-expandOpen">{{ $unallocatebookingcount }}</span></div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                        <a href="{{ url('') }}/admin/past-booking" class="widget widget-hover-effect2">
                            <div class="widget-extra past-booking">
                                <h4 class="widget-content-light"><strong>PAST </strong> BOOKINGS</h4>
                            </div>
                            <div class="widget-extra-full"><span class="h2 animation-expandOpen">{{ $pastbookingcount }}</span></div>
                        </a>
                    </div>

                   
</div>

<div class="row">
        <div class="col-md-12">
            <div class="block">
                    <div id="calendar"></div>
            </div>
        </div>
</div>

</div>

<script>
var CompCalendar = function() {
    var calendarEvents  = $('.calendar-events');

    /* Function for initializing drag and drop event functionality */
    var initEvents = function() {
        calendarEvents.find('li').each(function() {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            var eventObject = { title: $.trim($(this).text()), color: $(this).css('background-color') };
            console.log('eventObject',eventObject);
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({ zIndex: 999, revert: true, revertDuration: 0 });
        });
    };

    return {
        init: function() {
            /* Initialize drag and drop event functionality */
            initEvents();       

            /* Initialize FullCalendar */
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            // console.log('d',d);
            // console.log('m',m);
            // console.log('y',y);

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next',
                    center: 'title',
                    //right: 'month,agendaWeek,agendaDay'
                    right: ''
                },
                firstDay: 1,
                editable: false,
                droppable: false,
                drop: function(date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // remove the element from the "Draggable Events" list
                    $(this).remove();
                },
                events: [
                    <?php 
                    foreach($upcomingbookings as $upcomingbooking){
                        $y=date('Y', strtotime($upcomingbooking->date));
                        $m=date('m', strtotime($upcomingbooking->date));
                        $d=date('d', strtotime($upcomingbooking->date));
                        @$count=\DB::table('bookings')->where('date',$upcomingbooking->date)->where('is_allocate',1)->count();
                    ?>
    
                    {
                        title: '<?php echo @$count; ?>',
                        start: new Date(<?php echo $y; ?>, <?php echo $m-1; ?>, <?php echo $d; ?>),
                        color: '#4CAF50'
                    },
                    <?php } ?>
                ]
            });
        }
    };
}();
</script>
<script>$(function(){ CompCalendar.init(); });</script>
@endsection