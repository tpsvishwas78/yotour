@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Product Management
</h1>
</div>
</div>
<style>
#pickupmap {
    height: 300px;
    display: none;
}
#dropmap {
    height: 300px;
    display: none;
}
#startingpintmap {
    height: 300px;
    display: none;
}
.btn-default.active,.btn-default.active.focus,.btn-default.active:hover{
    background-color: #00BCD4;
    border-color: #00BCD4;
    color: #fff;
}

.btn-default,.btn-default:focus{
    background-color: #eaedf1;
    border-color: #eaedf1;
    color: #000;
}
.btn-group .btn+.btn{
    margin-left: 5px;
}
</style>
<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->




<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submitholiday')}}" enctype="multipart/form-data" class="form-horizontal form-bordered-1">
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Holiday</h2>
    <div class="pull-right" style="margin: 1px 10px;">
        <button type="submit"  data-toggle="tooltip"  class="btn btn-primary" data-original-title="Save"><i class="fa fa-save"></i> Save</button>
       
</div>
<!-- END Form Validation Example Title -->

    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                
                <div class="block full">
                   
                    <!-- Block Tabs Title -->
                    <div class="block-title">
                        

                        <ul class="nav nav-tabs" data-toggle="tabs">
                            <li class="active"><a href="#tabs1">General</a></li>
                            <li><a href="#tabs2">Itinerary</a></li>
                            <li><a href="#tabs3">Photos</a></li>
                            <li><a href="#tabs4">Inclusions</a></li>
                            <li><a href="#tabs5">Time Slot</a></li>
                            <li><a href="#tabs6">Cut Off</a></li>
                            <li><a href="#tabs7">Language</a></li>
                            <li><a href="#tabs8">Accessibility</a></li>
                            <li><a href="#tabs9">Pricing</a></li>
                            <li><a href="#tabs10">Policy</a></li>
                            <li><a href="#tabs11">Info</a></li>
                            
                            
                        </ul>
                    </div>
                    <!-- END Block Tabs Title -->

                    <!-- Tabs Content -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabs1">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="product_name">Product Name </label>
                                <div class="col-md-6">
                                        <input type="text" id="product_name" name="product_name" class="form-control" value="{{ @$edit->product_name }}">
                                        
                                    @if ($errors->has('product_name'))
                                        <span class="text-danger">{{ $errors->first('product_name') }}</span>
                                    @endif
                                </div>
                            </div> 
                            <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Destination </label>
                                    <div class="col-md-3">
                                            <select name="country" id="country" class="form-control select-select2" style="width:100% !important;"> 
                                                <option value="">Select Country</option>
                                                @foreach ($countries as $country)
                                                <option @if (@$edit->country==$country->id)
                                                    selected
                                                @endif value="{{ $country->id }}">{{ $country->name }}</option> 
                                                @endforeach
                                                
                                            </select>
                                            
                                        @if ($errors->has('country'))
                                              <span class="text-danger">{{ $errors->first('country') }}</span>
                                         @endif
                                    </div>
                <?php
                @$cities=\DB::table('destinations')->where('country_id',@$edit->country)->get();
                ?>
                                    <div class="col-md-3">
                                        <select name="city" id="city" class="form-control select-select2" style="width:100% !important;">
                                            <option value="">Select City</option>
                                            @foreach ($cities as $city)
                                            <option @if ($edit->city==$city->id)
                                                selected
                                            @endif value="{{ $city->id }}">{{ $city->name }}</option>
                                            @endforeach
                                            
                                        </select>
                                        
                                    @if ($errors->has('city'))
                                        <span class="text-danger">{{ $errors->first('city') }}</span>
                                     @endif
                                </div>
                                </div> 
            
                                
            
                                <div class="form-group">
                                        <label class="col-md-4 control-label" for="holiday_category">Select Category</label>
                                        
                                        <div class="col-md-6">
                                            <span>For Holiday</span>
                                                <select name="holiday_category" id="holiday_category" class="form-control select-select2" style="width:100% !important;">
                                                    <option value="">Select</option>
                                                    @foreach ($holidaycats as $holidaycat)
                                                <option @if (@$edit->holiday_category==$holidaycat->id)
                                                    selected
                                                @endif value="{{ $holidaycat->id }}">{{ $holidaycat->name }}</option> 
                                                @endforeach
                                                   
                                                </select>
                                            @if ($errors->has('holiday_category'))
                                                <span class="text-danger">{{ $errors->first('holiday_category') }}</span>
                                            @endif
                                        </div>
                                       
                                </div>  
            
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Select Theme</label>
                                    <div class="col-md-6">
                                            <select name="theme" id="theme" class="form-control select-select2" style="width:100% !important;">
                                                <option value="">Select</option>
                                                @foreach ($themes as $theme)
                                                <option @if (@$edit->theme==$theme->id)
                                                    selected
                                                @endif value="{{ $theme->id }}"> {{ $theme->name }} </option>  
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                
                               
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="pickup">Meeting Point, Pick up & Drop Off </label>
                                <div class="col-md-6">
                                    <input type="text" id="pickup" name="pickup" class="form-control" value="{{ @$edit->pickup }}" placeholder="Pick Up Location">
                                    <input type="hidden" name="pickup_latitude" id="pickup_latitude" value="{{ @$edit->pickup_latitude }}">
                                    <input type="hidden" name="pickup_longitude" id="pickup_longitude" value="{{ @$edit->pickup_longitude }}">
                                    <div class="ground-map">
                                        <div id="pickupmap"></div>
                                    </div>
                                </div>
            
                                <label class="col-md-4 control-label" for="starting_point"></label>
                                <div class="col-md-6" style="margin-top: 30px;">
                                    <input type="text" id="starting_point" name="starting_point" class="form-control" value="{{ @$edit->starting_point }}" placeholder="Starting Point">
                                    <input type="hidden" name="starting_latitude" id="starting_latitude" value="{{ @$edit->starting_latitude }}">
                                    <input type="hidden" name="starting_longitude" id="starting_longitude" value="{{ @$edit->starting_longitude }}">
                                    <div class="ground-map">
                                        <div id="startingpintmap"></div>
                                    </div>
                                </div>
                                
                                <label class="col-md-4 control-label" for="dropoff"></label>
                                <div class="col-md-6" style="margin-top: 30px;">
                                    <input type="text" id="dropoff" name="dropoff" class="form-control" value="{{ @$edit->dropoff }}" placeholder="Drop Off Location">
                                    <input type="hidden" name="drop_latitude" id="drop_latitude" value="{{ @$edit->drop_latitude }}">
                                    <input type="hidden" name="drop_longitude" id="drop_longitude" value="{{ @$edit->drop_longitude }}">
                                    <div class="ground-map">
                                        <div id="dropmap"></div>
                                    </div>
                                </div>
                            </div>    
                        </fieldset>   
                        </div>
                        <div class="tab-pane" id="tabs2">
                            <?php
                            @$itineraries=\DB::table('product_itinerary')->where('product_id',@$edit->id)->get();
                            
                            ?>                         
<!-- Start Itinerary Block --> 
        <div class="block">           
            <div class="block-title">
                <h2>Itinerary</h2>
            </div>
        <fieldset>
            <div class="itinary-controls">
                <?php
            $mykey=0;
                ?>
            @foreach (@$itineraries as $key => $itinerary)
            <?php $mykey=$key;
            if($mykey>0){
                $mykey++;
            }
            ?>
            <div class="itinary-entry" style="border-bottom: 1px solid #ddd;
            margin-bottom: 20px;">
            <div class="form-group">
                <label class="col-md-4 control-label">Day Title</label>
                <div class="col-md-6">
                        <input type="text" name="day_title[]" class="form-control" value="{{ @$itinerary->day_title }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Name of the place in Itinerary</label>
                <div class="col-md-6">
                        <input type="text"  name="itinerary_title[]" class="form-control" value="{{ @$itinerary->itinerary_title }}">
                </div>
                <button class="btn btn-remove-itinary btn-danger" type="button" id="{{ @$itinerary->id }}">
                    <span class="gi gi-minus"></span>
              </button>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Photo </label>
                <div class="col-md-6">
                    <input type="file" name="itinerary_img[]" class="form-control">
                    @if (@$itinerary->itinerary_img)
                    <p>
                    <img width="100" src="{{ url('') }}/upload/images/{{ @$itinerary->itinerary_img }}" alt="">
                    </p> 
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Type of Place</label>
                <div class="col-md-6">
                        <input type="text"  name="place_type[]" class="form-control input-tags1" placeholder="Monument,Museum..etc" value="{{ @$itinerary->place_type }}">
                    <span>Monument,Museum..etc</span>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-4 control-label">Describe what travelers will see and do here if they book the experience:</label>
                <div class="col-md-6">
                        <input type="text"  name="describe_experience[]" class="form-control" value="{{ @$itinerary->describe_experience }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">How much time do travelers typically spend here?
                </label>
                <div class="col-md-6">
                    <?php @$spenttimeArray = unserialize(@$itinerary->spent_time);?>
                    <label class="checkbox-inline">
                        <input @if (@in_array('Pass by without stopping',@$spenttimeArray)) 
                         checked 
                         @endif type="checkbox"  name="spent_time[{{ $key }}][0]" value="Pass by without stopping">Pass by without stopping
                    </label>
                    <label class="checkbox-inline">
                        <input @if (@in_array('Minutes',@$spenttimeArray)) checked @endif type="checkbox" name="spent_time[{{ $key }}][1]" value="Minutes">Minutes
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Is admission to this place included in the price of your tour?
                </label>
                <div class="col-md-6">
                    <label class="radio-inline">
                        <input type="radio" @if (@$itinerary->included_price_tour=='Yes')
                            checked
                        @endif name="included_price_tour[{{ $key }}]" value="Yes">Yes
                    </label>
                    <label class="radio-inline">
                        <input @if (@$itinerary->included_price_tour=='No')
                        checked
                    @endif type="radio" name="included_price_tour[{{ $key }}]" value="No">No
                    </label>
                    <label class="radio-inline">
                        <input @if (@$itinerary->included_price_tour=='N/A (Admission is free)')
                        checked
                    @endif type="radio"  name="included_price_tour[{{ $key }}]" value="N/A (Admission is free)">N/A (Admission is free)
                    </label>
                </div>
            </div> 


            <div class="form-group">
                <label class="col-md-4 control-label">Add Meal</label>
                <div class="col-md-6">
                        <input type="text"  name="meal_type[]" class="form-control input-tags1" placeholder="Breakfast,Lunch,Dinner..etc" value="{{ @$itinerary->meal_type }}">
                    <span>Breakfast,Lunch,Dinner..etc</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Add description (optional)</label>
                <div class="col-md-6">
                        <textarea name="meal_description[]" class="form-control" cols="30" rows="5">{{ @$itinerary->meal_description }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Accomodation Details</label>
                <div class="col-md-6">
                        <textarea name="accomodation_details[]" class="form-control"  cols="30" rows="5">{{ @$itinerary->accomodation_details }}</textarea>
                </div>
            </div>

            <input type="hidden" name="itinerary_id[{{ $key }}]" value="{{ @$itinerary->id }}">
        @endforeach

        <div class="itinary-entry" style="border-bottom: 1px solid #ddd;
            margin-bottom: 20px;">
            <div class="form-group">
                <label class="col-md-4 control-label">Day Title</label>
                <div class="col-md-6">
                        <input type="text" name="day_title[]" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Name of the place in Itinerary</label>
                <div class="col-md-6">
                        <input type="text"  name="itinerary_title[]" class="form-control" >
                </div>
                <button class="btn btn-success btn-add-itinary" type="button">
                    <span class="gi gi-plus"></span>
              </button>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Photo </label>
                <div class="col-md-6">
                    <input type="file" name="itinerary_img[]" class="form-control">
                   
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Type of Place</label>
                <div class="col-md-6">
                        <input type="text" name="place_type[]" class="form-control input-tags1" placeholder="Monument,Museum..etc">
                    <span>Monument,Museum..etc</span>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-4 control-label">Describe what travelers will see and do here if they book the experience:</label>
                <div class="col-md-6">
                        <input type="text" name="describe_experience[]" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">How much time do travelers typically spend here?
                </label>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                        <input type="checkbox"  name="spent_time[{{ $mykey }}][0]" value="Pass by without stopping">Pass by without stopping
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="spent_time[{{ $mykey }}][1]" value="Minutes">Minutes
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Is admission to this place included in the price of your tour?
                </label>
                <div class="col-md-6">
                    <label class="radio-inline">
                        <input type="radio" name="included_price_tour[{{ $mykey }}]" value="Yes">Yes
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="included_price_tour[{{ $mykey }}]" value="No">No
                    </label>
                    <label class="radio-inline">
                        <input type="radio"  name="included_price_tour[{{ $mykey }}]" value="N/A (Admission is free)">N/A (Admission is free)
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Add Meal</label>
                <div class="col-md-6">
                        <input type="text"  name="meal_type[]" class="form-control input-tags1" placeholder="Breakfast,Lunch,Dinner..etc">
                    <span>Breakfast,Lunch,Dinner..etc</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Add description (optional)</label>
                <div class="col-md-6">
                        <textarea name="meal_description[]" class="form-control" cols="30" rows="5"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Accomodation Details</label>
                <div class="col-md-6">
                        <textarea name="accomodation_details[]" class="form-control"  cols="30" rows="5"></textarea>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="itinerary_id[]" value="">
        </fieldset>

        </div>
        <!-- END Itinerary Block -->  
                        </div>
                        <div class="tab-pane" id="tabs3">
                            <!-- Start Select Photos & Video Block --> 
        <div class="block">           
            <div class="block-title">
                <h2>Select Photos & Video</h2>
            </div>
        <fieldset>
            <?php
            @$images=\DB::table('product_images')->where('product_id',@$edit->id)->get();
            ?>
            <div class="form-group">
                <label class="col-md-4 control-label" for="gallary">Select Photos & Video </label>
                <div class="col-md-6">
                    <div class="controls">
                        <div class="entry input-group col-xs-3">
                          <input class="btn btn-default" name="gallary[]" type="file">
                          <span class="input-group-btn">
                        <button class="btn btn-success btn-add" type="button">
                                <span class="gi gi-plus"></span>
                          </button>
                          </span>
                        </div>
                    </div>
                    @if (@$images)
    @foreach (@$images as $image)
            <p>
            <img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
            <a href="javascript:void(0);"><i id="{{ $image->id }}" class="gi gi-delete removeimg"></i></a> 
            </p>
    @endforeach
    @endif
                  <input  style="margin-top: 20px;" type="text" name="video_link" placeholder="Youtube video link" class="form-control" value="{{ @$edit->video_link }}">      
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="featured_img">Featured Image </label>
                <div class="col-md-6">
                    <input type="file" id="featured_img" name="featured_img" class="form-control">
                    @if (@$edit->featured_img)
                    <p>
                    <img width="100" src="{{ url('') }}/upload/images/{{ @$edit->featured_img }}" alt="">
                    </p> 
                    @endif
                </div>
            </div>
        </fieldset>

        </div>
        <!-- END Select Photos & Video Block --> 
                        </div>
                        <div class="tab-pane" id="tabs4">
        <?php
        @$inclusions=\DB::table('product_inclusions_exclusions')->where('product_id',@$edit->id)->first();
        ?>                      
    <!-- Start Inclusions & Exclusions Block --> 
    <div class="block">           
        <div class="block-title">
            <h2>Inclusions & Exclusions</h2>
        </div>
    <fieldset>
        <div class="form-group">
            <label class="col-md-4 control-label" for="add_inclusion">Add an inclusion</label>
            <div class="col-md-8">
                <label class="radio-inline" for="for_basic_tour">
                    <input type="radio" @if (@$inclusions->inclusion_package=='For Basic Tour')
                    checked
                @endif id="for_basic_tour" name="inclusion_package" value="For Basic Tour">For Basic Tour
                </label>
                <label class="radio-inline" for="for_budget_tour">
                    <input type="radio" @if (@$inclusions->inclusion_package=='For Budget Tour')
                    checked
                @endif id="for_budget_tour" name="inclusion_package" value="For Budget Tour">For Budget Tour
                </label>
                <label class="radio-inline" for="for_premium_tour">
                    <input @if (@$inclusions->inclusion_package=='For Premium Tour')
                    checked
                @endif type="radio" id="for_premium_tour" name="inclusion_package" value="For Premium Tour">For Premium Tour
                </label>
                <label class="radio-inline" for="for_vip_tour">
                    <input @if (@$inclusions->inclusion_package=='For VIP Tour')
                    checked
                @endif type="radio" id="for_vip_tour" name="inclusion_package" value="For VIP Tour">For VIP Tour
                </label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="inclusion_category">Category</label>
            <div class="col-md-6">
                    <select name="inclusion_category" id="inclusion_category" class="form-control select-select2" style="width:100% !important;">
                        <option value="">Select</option>
                        @foreach ($inclusionscats as $inclusionscat)
                            <option @if (@$inclusions->inclusion_category==$inclusionscat->id)
                                selected
                            @endif value="{{ $inclusionscat->id }}">{{ $inclusionscat->name }}</option> 
                        @endforeach
                    </select>
            </div>
        </div>
        <?php
            @$inclusionsubcategorys=\DB::table('product_categories')->where('parent',$inclusions->inclusion_category)->get();
        ?>
        <div class="form-group">
            <label class="col-md-4 control-label" for="inclusion_sub_category">Sub Category</label>
            <div class="col-md-6">
                    <select name="inclusion_sub_category[]" id="inclusion_sub_category" class="form-control select-select2" multiple style="width:100% !important;">
                        <?php @$inclusionsubArray = unserialize(@$inclusions->inclusion_sub_category);?>
    
                        @foreach ($inclusionsubcategorys as $inclusionsubcategory)
                            <option @if (@in_array($inclusionsubcategory->id,@$inclusionsubArray))
                                selected
                            @endif value="{{ $inclusionsubcategory->id }}">{{ $inclusionsubcategory->name }}</option>
                        @endforeach
                       
                    </select>
               
            </div>
        </div> 
        <div class="form-group">
            <label class="col-md-4 control-label" for="inclusion_description">Description </label>
            <div class="col-md-6">
                    <input type="text" id="inclusion_description" name="inclusion_description" class="form-control" value="{{ @$inclusions->inclusion_description }}">
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label class="col-md-4 control-label" for="exclusion_for_basic_tour">Add an exclusion</label>
            <div class="col-md-8">
                <label class="radio-inline" for="exclusion_for_basic_tour">
                    <input @if (@$inclusions->exclusion_package=='For Basic Tour')
                        checked
                    @endif type="radio" id="exclusion_for_basic_tour" name="exclusion_package" value="For Basic Tour">For Basic Tour
                </label>
                <label class="radio-inline" for="exclusion_for_budget_tour">
                    <input type="radio" @if (@$inclusions->exclusion_package=='For Budget Tour')
                    checked
                @endif id="exclusion_for_budget_tour" name="exclusion_package" value="For Budget Tour">For Budget Tour
                </label>
                <label class="radio-inline" for="exclusion_for_premium_tour">
                    <input @if (@$inclusions->exclusion_package=='For Premium Tour')
                    checked
                @endif type="radio" id="exclusion_for_premium_tour" name="exclusion_package" value="For Premium Tour">For Premium Tour
                </label>
                <label class="radio-inline" for="exclusion_for_vip_tour">
                    <input @if (@$inclusions->exclusion_package=='For VIP Tour')
                    checked
                @endif type="radio" id="exclusion_for_vip_tour" name="exclusion_package" value="For VIP Tour">For VIP Tour
                </label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="exclusion_category">Category</label>
            <div class="col-md-6">
                    <select name="exclusion_category" id="exclusion_category" class="form-control select-select2" style="width:100% !important;">
                        <option value="">Select</option>
                        @foreach ($inclusionscats as $inclusionscat)
                        <option @if (@$inclusions->exclusion_category==$inclusionscat->id)
                            selected
                        @endif value="{{ $inclusionscat->id }}">{{ $inclusionscat->name }}</option> 
                        @endforeach
                    </select>
            </div>
        </div> 
        <?php
            @$exclusionsubcategorys=\DB::table('product_categories')->where('parent',$inclusions->exclusion_category)->get();
        ?>
        <div class="form-group">
            <label class="col-md-4 control-label" for="exclusion_sub_category">Sub Category</label>
            <div class="col-md-6">
                    <select name="exclusion_sub_category[]" id="exclusion_sub_category" class="form-control select-select2" multiple style="width:100% !important;">
                        <?php @$exclusionsubArray = unserialize(@$inclusions->exclusion_sub_category);?>
    
                        @foreach ($exclusionsubcategorys as $exclusionsubcategory)
                        <option @if (@in_array($exclusionsubcategory->id,@$exclusionsubArray))
                            selected
                        @endif value="{{ $exclusionsubcategory->id }}">{{ $exclusionsubcategory->name }}</option>
                    @endforeach
                    </select>
               
            </div>
        </div>  
        <div class="form-group">
            <label class="col-md-4 control-label" for="exclusion_description">Description </label>
            <div class="col-md-6">
                    <input type="text" id="exclusion_description" name="exclusion_description" class="form-control" value="{{ @$inclusions->exclusion_description }}">
            </div>
        </div>
    <input type="hidden" name="inclusion_id" value="{{ @$inclusions->id }}">
        
    </fieldset>
</div>
<!-- END Inclusions & Exclusions Block -->  
                        </div>
                        <div class="tab-pane" id="tabs5">
                            <?php
                        @$duration=\DB::table('product_time_duration_slot')->where('product_id',@$edit->id)->first();
                        ?> 
<!-- Start Tour Time Slot Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Tour Date & Time Slot</h2>
    </div>
<fieldset>
    <input type="hidden" name="tour_duration_id" value="{{ @$duration->id }}">
    <div class="form-group">
        <label class="col-md-4 control-label" for="example-daterange1">Holiday (Start Date To End Date)</label>
        <div class="col-md-6">
            <div class="input-group input-daterange" data-date-format="yyyy-mm-dd">
                <input type="text" id="example-daterange1" name="holiday_start_date" class="form-control text-center" placeholder="Holiday Start Date" value="{{ @$duration->holiday_start_date }}">
                <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                <input type="text" id="example-daterange2" name="holiday_end_date" class="form-control text-center" placeholder="Holiday End Date" value="{{ @$duration->holiday_end_date }}">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="example-timepicker">Holiday (Start Time To End Time)
        </label>
        <div class="col-md-3">
            <div class="input-group bootstrap-timepicker">
                <input type="text" id="example-timepicker" name="holiday_start_tour_time" class="form-control input-timepicker" value="{{ @$duration->holiday_start_tour_time }}">
                <span class="input-group-btn">
                    <a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a>
                </span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group bootstrap-timepicker">
                <input type="text" id="example-timepicker" name="holiday_end_tour_time" class="form-control input-timepicker" value="{{ @$duration->holiday_end_tour_time }}">
                <span class="input-group-btn">
                    <a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="additional_date_time_slot">Add additional date & time slots (any number of slots can be added)</label>
        <div class="col-md-6">
            <input type="text" id="additional_date_time_slot" name="additional_date_time_slot" class="form-control" value="{{ @$duration->additional_date_time_slot }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="min_people">MINIMUM & MAXIMUM PEOPLE FOR BOOKING</label>
        <div class="col-md-3">
            <input type="text" name="min_people" class="form-control" value="{{ @$duration->min_people }}" placeholder="Minimum">
        </div>
        <div class="col-md-3">
            <input type="text" name="max_people" class="form-control" value="{{ @$duration->max_people }}" placeholder="Maximum">
        </div>
    </div>

    
    
</fieldset>

</div>
<!-- END Tour Time Slot Block --> 
                        </div>
                        <div class="tab-pane" id="tabs6">
                            
<!-- Start Cut Off Time Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Cut Off Time</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="cut_off_time">How would you like to set your cut-off time?
            
        </label>
        <div class="col-md-6">
            <span>Hours before the tour start time</span>
            <input type="text" id="hour_before_tour_time" name="hour_before_tour_time" class="form-control" value="{{ @$duration->hour_before_tour_time }}">
        </div>
    </div>
    
    
</fieldset>

</div>
<!-- END Cut Off Time Block -->
                        </div>
                        <div class="tab-pane" id="tabs7">
                            
<!-- Start Tour Language Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Tour Language</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="tour_language">Select Tour Language</label>
        <div class="col-md-6">
            <?php @$languageArray = unserialize(@$edit->tour_language);?>
                <select name="tour_language[]" id="tour_language" class="form-control select-select2" multiple style="width:100% !important;">
                   @foreach ($languages as $language)
                   <option @if (@in_array($language->id,@$languageArray))
                       selected
                   @endif value="{{ $language->id }}">{{ $language->name }}</option>
                   @endforeach
                </select>
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Tour Language Block -->
                        </div>
                        <div class="tab-pane" id="tabs8">
                            
<!-- Start Accessibility Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Accessibility</h2>
    </div>
<fieldset>
    
    <label style="text-align:left;color: #000; font-size: 16px;" class="col-md-12 control-label" for="spent_time">General</label>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_wheelchair">Is it Wheelchair accessible?</label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_wheelchair_yes">
                <input @if (@$edit->is_wheelchair=='Yes')
                    checked
                @endif type="radio" id="is_wheelchair_yes" name="is_wheelchair" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_wheelchair_no">
                <input @if (@$edit->is_wheelchair=='No')
                checked
            @endif type="radio" id="is_wheelchair_no" name="is_wheelchair" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_stroller">Is it stroller accessible?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_stroller_yes">
                <input @if (@$edit->is_stroller=='Yes')
                checked
            @endif type="radio" id="is_stroller_yes" name="is_stroller" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_stroller_no">
                <input @if (@$edit->is_stroller=='No')
                checked
            @endif type="radio" id="is_stroller_no" name="is_stroller" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_animals_allowed">Are animals allowed?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_animals_allowed_yes">
                <input @if (@$edit->is_animals_allowed=='Yes')
                checked
            @endif type="radio" id="is_animals_allowed_yes" name="is_animals_allowed" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_animals_allowed_no">
                <input @if (@$edit->is_animals_allowed=='No')
                checked
            @endif type="radio" id="is_animals_allowed_no" name="is_animals_allowed" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_travelers_easily">Can travelers easily arrive/depart on public transportation?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_travelers_easily_yes">
                <input @if (@$edit->is_travelers_easily=='Yes')
                checked
            @endif type="radio" id="is_travelers_easily_yes" name="is_travelers_easily" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_travelers_easily_no">
                <input @if (@$edit->is_travelers_easily=='No')
                checked
            @endif type="radio" id="is_travelers_easily_no" name="is_travelers_easily" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_infants_required">Are infants required to sit on laps?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_infants_required_yes">
                <input @if (@$edit->is_infants_required=='Yes')
                checked
            @endif type="radio" id="is_infants_required_yes" name="is_infants_required" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_infants_required_no">
                <input @if (@$edit->is_infants_required=='No')
                checked
            @endif type="radio" id="is_infants_required_no" name="is_infants_required" value="No">No
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_infant_seats_available">Are infant seats available?
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="is_infant_seats_available_yes">
                <input @if (@$edit->is_infant_seats_available=='Yes')
                checked
            @endif type="radio" id="is_infant_seats_available_yes" name="is_infant_seats_available" value="Yes">Yes
            </label>
            <label class="radio-inline" for="is_infant_seats_available_no">
                <input @if (@$edit->is_infant_seats_available=='No')
                checked
            @endif type="radio" id="is_infant_seats_available_no" name="is_infant_seats_available" value="No">No
            </label>
        </div>
    </div>
    <label style="text-align:left;color: #000; font-size: 16px;" class="col-md-12 control-label" for="future_options">Give options to add more such options in future</label>
    <div class="form-group">
        <label class="col-md-4 control-label" for="health_restrictions">Health Restrictions
        </label>
        <div class="col-md-6">
            <label class="checkbox-inline" for="travelers_with_back_problems" style="margin-left: 10px;">
                <input @if (@$edit->travelers_with_back_problems=='1')
                checked
            @endif type="checkbox" id="travelers_with_back_problems" name="travelers_with_back_problems" value="1">Not recommended for travelers with back problems
            </label>
            <label class="checkbox-inline" for="pregnant_travelers">
                <input @if (@$edit->pregnant_travelers=='1')
                checked
            @endif type="checkbox" id="pregnant_travelers" name="pregnant_travelers" value="1">Not recommended for pregnant travelers
            </label>
            <label class="checkbox-inline" for="serious_medical">
                <input @if (@$edit->serious_medical=='1')
                checked
            @endif type="checkbox" id="serious_medical" name="serious_medical" value="1">Not recommended for travelers with heart problems or other serious medical conditions             
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="physical_difficulty">Select the physical difficulty level
        </label>
        <div class="col-md-6">
            <label class="radio-inline" for="physical_difficulty_easy">
                <input @if (@$edit->physical_difficulty=='Easy')
                checked
            @endif type="radio" id="physical_difficulty_easy" name="physical_difficulty" value="Easy">Easy
            </label>
            <label class="radio-inline" for="physical_difficulty_moderate">
                <input @if (@$edit->physical_difficulty=='Moderate')
                checked
            @endif type="radio" id="physical_difficulty_moderate" name="physical_difficulty" value="Moderate">Moderate
            </label>
            <label class="radio-inline" for="physical_difficulty_challenging">
                <input  @if (@$edit->physical_difficulty=='Challenging')
                checked
            @endif type="radio" id="physical_difficulty_challenging" name="physical_difficulty" value="Challenging">Challenging
            </label>
        </div>
    </div>
</fieldset>

</div>
<!-- END Accessibility Block -->
                        </div>
                        <div class="tab-pane" id="tabs9">
                            <?php
                        @$pricing=\DB::table('product_pricing')->where('product_id',@$edit->id)->first();
                        ?>   
                            <!-- Start Pricing Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Pricing</h2>
    </div>
<fieldset>
    <input type="hidden" name="price_id" value="{{ @$pricing->id }}">
    <div class="form-group">
        <label class="col-md-4 control-label" for="currency">Select Currency</label>
        <div class="col-md-6">
                <select name="currency" id="currency" class="form-control select-select2" style="width:100% !important;">
                    <option @if (@$pricing->currency=='INR')
                        selected
                    @endif value="INR">INR</option>
                    <option @if (@$pricing->currency=='EUR')
                        selected
                    @endif value="EUR">EUR</option>
                </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="age_group_pricing">Define Age Group for Pricing</label>
        <div class="col-md-6">
            <div class="col-md-12">
            <label class="col-md-2 control-label" for="adult_age">Adult</label>
            <div class="col-md-3">
                <span>Min. Age</span>
            <input type="text" id="min_age_adult" name="min_age_adult" class="form-control" value="{{ @$pricing->min_age_adult }}">
            </div>
            <div class="col-md-3">
                <span>Max. Age</span>
                <input type="text" id="max_age_adult" name="max_age_adult" class="form-control" value="{{ @$pricing->max_age_adult }}">
                </div>
            </div>
            <div class="col-md-12">
                <label class="col-md-2 control-label" for="child_age">Child</label>
                <div class="col-md-3">
                    <span>Min. Age</span>
                <input type="text" id="min_age_child" name="min_age_child" class="form-control" value="{{ @$pricing->min_age_child }}">
                </div>
                <div class="col-md-3">
                    <span>Max. Age</span>
                    <input type="text" id="max_age_child" name="max_age_child" class="form-control" value="{{ @$pricing->max_age_child }}">
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="col-md-2 control-label" for="youth_age">Youth</label>
                    <div class="col-md-3">
                        <span>Min. Age</span>
                    <input type="text" id="min_age_youth" name="min_age_youth" class="form-control" value="{{ @$pricing->min_age_youth }}">
                    </div>
                    <div class="col-md-3">
                        <span>Max. Age</span>
                        <input type="text" id="max_age_youth" name="max_age_youth" class="form-control" value="{{ @$pricing->max_age_youth }}">
                        </div>
                    </div>
        </div>
    </div>

    
    
    <div class="form-group">
        <label class="col-md-4 control-label" for="pricing_type">Select Pricing</label>
        <div class="col-md-6">
            <label class="radio-inline" for="pricing_type_person">
                <input @if (@$pricing->pricing_type=='Per Person')
                checked
                @endif type="radio" id="pricing_type_person" name="pricing_type" value="Per Person">Per Person
            </label>
            <label class="radio-inline" for="pricing_type_vehicle">
                <input @if (@$pricing->pricing_type=='Per Vehicle')
                checked
            @endif type="radio" id="pricing_type_vehicle" name="pricing_type" value="Per Vehicle">Per Vehicle
            </label>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-md-4 control-label" for="price_for_tour">Price for the Tour</label>
        <div class="col-md-6">
            <div class="col-md-12">
            <label class="col-md-2 control-label" for="adult_age">Adult</label>
            <div class="col-md-3">
                <span>Price</span>
            <input type="text" id="adult_price" name="adult_price" class="form-control" value="{{ @$pricing->adult_price }}">
            </div>
            <div class="col-md-3">
                <span>Min. Age</span>
            <input type="text" id="min_age_adult_price" name="min_age_adult_price" class="form-control" value="{{ @$pricing->min_age_adult_price }}">
            </div>
            <div class="col-md-3">
                <span>Max. Age</span>
                <input type="text" id="max_age_adult_price" name="max_age_adult_price" class="form-control" value="{{ @$pricing->max_age_adult_price }}">
                </div>
            </div>
            <div class="col-md-12">
                <label class="col-md-2 control-label" for="child_age">Child</label>
                <div class="col-md-3">
                    <span>Price</span>
                <input type="text" id="child_price" name="child_price" class="form-control" value="{{ @$pricing->child_price }}">
                </div>
                <div class="col-md-3">
                    <span>Min. Age</span>
                <input type="text" id="min_age_child_price" name="min_age_child_price" class="form-control" value="{{ @$pricing->min_age_child_price }}">
                </div>
                <div class="col-md-3">
                    <span>Max. Age</span>
                    <input type="text" id="max_age_child_price" name="max_age_child_price" class="form-control" value="{{ @$pricing->max_age_child_price }}">
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="col-md-2 control-label" for="youth_age">Youth</label>
                    <div class="col-md-3">
                        <span>Price</span>
                    <input type="text" id="youth_price" name="youth_price" class="form-control" value="{{ @$pricing->youth_price }}">
                    </div>
                    <div class="col-md-3">
                        <span>Min. Age</span>
                    <input type="text" id="min_age_youth_price" name="min_age_youth_price" class="form-control" value="{{ @$pricing->min_age_youth_price }}">
                    </div>
                    <div class="col-md-3">
                        <span>Max. Age</span>
                        <input type="text" id="max_age_youth_price" name="max_age_youth_price" class="form-control" value="{{ @$pricing->max_age_youth_price }}">
                        </div>
                    </div>
        </div>
    </div>
   
    
    <div class="form-group">
        <label class="col-md-4 control-label" for="tax_on_pricing">Tax on Pricing</label>
        <div class="col-md-6">
            <div class="col-md-12">
            <span>Name tax </span>
            <input type="text" id="tax_name" name="tax_name" class="form-control" value="{{ @$pricing->tax_name }}">
        </div>
        <div class="col-md-12">
            <span>Tax Rate</span>
            <input type="text" id="tax_rate" name="tax_rate" class="form-control" value="{{ @$pricing->tax_rate }}">
        </div>
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Pricing Block -->
                        </div>
                        <div class="tab-pane" id="tabs10">
                            
<!-- Start Cancellation Policy Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Cancellation Policy</h2>
    </div>
<fieldset>
    <div class="form-group">
        <label class="col-md-4 control-label" for="cancellation_policy">Select your cancellation policy</label>
        <div class="col-md-6">
            <label class="radio-inline" for="cancellation_policy_24">
                <input @if (@$edit->cancellation_policy=='1') checked @endif type="radio" id="cancellation_policy_24" name="cancellation_policy" value="1">24 Hours (Recommended)
            </label>
            <label class="radio-inline" for="cancellation_policy_non">
                <input @if (@$edit->cancellation_policy=='0') checked @endif type="radio" id="cancellation_policy_non" name="cancellation_policy" value="0">Non-refundable
            </label>
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Cancellation Policy Block -->
                        </div>
                        <div class="tab-pane" id="tabs11">
                            
<!-- Start Additional Info Block --> 
<div class="block">           
    <div class="block-title">
        <h2>Additional Info</h2>
    </div>
<fieldset>
   
    <div class="form-group">
        <label class="col-md-4 control-label" for="additional_info">Additional Info
        </label>
        <div class="col-md-6">
            <input type="text" id="additional_info" name="additional_info" class="form-control" value="{{ @$edit->additional_info }}">
        </div>
    </div>
    
</fieldset>

</div>
<!-- END Additional Info Block -->
                        </div>
                    </div>
                    <!-- END Tabs Content -->
                </div>
    
</fieldset>

</div>
<!-- END Cancellation Policy Block -->

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
   
</form>
<!-- END Form Validation Example Content -->


</div>

</div>
</div>
<script>

$(function()
{
    var i=0;
    $(document).on('click', '.btn-add-itinary', function(e)
    {
       
        e.preventDefault();
        
        var controlForm = $('.itinary-controls:first'),
            currentEntry = $(this).parents('.itinary-entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        //newEntry.find('input').val('');

        
        
        controlForm.find('.itinary-entry:not(:last) .btn-add-itinary')
            .removeClass('btn-add-itinary').addClass('btn-remove-itinary')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="gi gi-minus"></span>');

            var j=parseInt(i)+parseInt(1);
            // console.log('input[name="spent_time['+i+'][0]"]');
            // console.log('input[name="spent_time['+i+'][1]"]');

            newEntry.find('input[name="included_price_tour['+i+']"]').attr('name', 'included_price_tour['+j+']');
            newEntry.find('input[name="spent_time['+i+'][0]"]').attr('name', 'spent_time['+j+'][0]');
            newEntry.find('input[name="spent_time['+i+'][1]"]').attr('name', 'spent_time['+j+'][1]');
        i++;

    }).on('click', '.btn-remove-itinary', function(e)
    {
        var c = confirm('Are you sure you want to remove?');
        if(c){
            $(this).parents('.itinary-entry:first').remove();
            // e.preventDefault();
            // return false;
            var id=$(this).attr('id');
            if(id){
            
                var _token = $('input[name="_token"]').val();
                $.ajax({
                             url:"{{ url('') }}/admin/removeitinary",
                             method:"POST",
                             dataType:'Json',
                             data:{id:id, _token:_token},
                             success:function(data){
                                
                             }
                });
            }
        }
     
	});
});
$(document).on('change', '#country', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getcitybycountry",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#city').html(data);
                 }
                });
    
});
$(document).on('change', '#inclusion_category', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getinclusionsubcate",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#inclusion_sub_category').html(data);
                 }
    });
    
});

$(document).on('change', '#exclusion_category', function(e)
{
    var id=$(this).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
                 url:"{{ url('') }}/admin/getinclusionsubcate",
                 method:"POST",
                 dataType:'Json',
                 data:{id:id, _token:_token},
                 success:function(data){
                    $('#exclusion_sub_category').html(data);
                 }
    });
    
});

    $(document).on('click', '.removeimg', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('p').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deleteproductgalleryimage",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
 $(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="gi gi-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});


google.maps.event.addDomListener(window, 'load', function () {
          var options = {
			//types: ['(cities)'],
			};
            var places = new google.maps.places.Autocomplete(document.getElementById('pickup'),options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var lat = places.getPlace().geometry.location.lat();
                var lng = places.getPlace().geometry.location.lng();
                    $('#pickup_latitude').val(lat);
                    $('#pickup_longitude').val(lng);

                    var map = new google.maps.Map(document.getElementById('pickupmap'), {
            center: {lat: lat, lng: lng},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var marker = new google.maps.Marker({map: map,draggable:true, position: {lat: lat, lng: lng}});
          google.maps.event.addListener(marker, 'dragend', function() 
            {
                //console.log(marker.getPosition().lat());
                    $('#pickup_latitude').val(marker.getPosition().lat());
                    $('#pickup_longitude').val(marker.getPosition().lng());
            });

          if(lat && lng){
                $('#pickupmap').show();
            }
            });

            //starting point

            var statrtplaces = new google.maps.places.Autocomplete(document.getElementById('starting_point'),options);
            google.maps.event.addListener(statrtplaces, 'place_changed', function () {
                var startlat = statrtplaces.getPlace().geometry.location.lat();
                var startlng = statrtplaces.getPlace().geometry.location.lng();
                    $('#starting_latitude').val(startlat);
                    $('#starting_longitude').val(startlng);

                    var map = new google.maps.Map(document.getElementById('startingpintmap'), {
            center: {lat: startlat, lng: startlng},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var startmarker = new google.maps.Marker({map: map,draggable:true, position: {lat: startlat, lng: startlng}});
          google.maps.event.addListener(startmarker, 'dragend', function() 
            {
                //console.log(marker.getPosition().lat());
                    $('#starting_latitude').val(startmarker.getPosition().lat());
                    $('#starting_longitude').val(startmarker.getPosition().lng());
            });

          if(startlat && startlng){
                $('#startingpintmap').show();
            }
            });


            //staring point
            
            var dropplaces = new google.maps.places.Autocomplete(document.getElementById('dropoff'),options);
            google.maps.event.addListener(dropplaces, 'place_changed', function () {
                var droplat = dropplaces.getPlace().geometry.location.lat();
                var droplng = dropplaces.getPlace().geometry.location.lng();
                    $('#drop_latitude').val(droplat);
                    $('#drop_longitude').val(droplng);

            var dropmap = new google.maps.Map(document.getElementById('dropmap'), {
            center: {lat: droplat, lng: droplng},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
		  var dropmarker=new google.maps.Marker({map: dropmap,draggable:true, position: {lat: droplat, lng: droplng}});
          google.maps.event.addListener(dropmarker, 'dragend', function() 
            {
                //console.log(marker.getPosition().lat());
                    $('#drop_latitude').val(dropmarker.getPosition().lat());
                    $('#drop_longitude').val(dropmarker.getPosition().lng());
            });
          if(droplat && droplng){
                $('#dropmap').show();
            }
            });
        });
</script>
@endsection