@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-group"></i>Customers Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Customer</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submitcustomer')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Booking Channel <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                
                              <select class="form-control  select-select2" name="channel" id="channel">
                            <option value="">Select</option> 
                            @foreach ($channels as $channel)
                            <option @if (@$edit->channel_id==$channel->id)
                                selected
                            @endif value="{{ $channel->id }}">{{ $channel->name }}</option> 
                            @endforeach
                            </select>  
                            @if ($errors->has('channel'))
                                                <span class="text-danger">{{ $errors->first('channel') }}</span>
                                            @endif
                        </div>
                    </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="name" name="name" class="form-control" value="{{ @$edit->name }}">
                            
                        @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                    </div>
                </div>  
                <div class="form-group">
                        <label class="col-md-4 control-label" for="email">Email <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                <input type="text" id="email" name="email" class="form-control" value="{{ @$edit->email }}">
                                
                            @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                        </div>
                    </div> 
                    <div class="form-group">
                            <label class="col-md-4 control-label" for="phone">Phone <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                    <input type="text" id="phone" name="phone" class="form-control" value="{{ @$edit->phone }}">
                                    
                                @if ($errors->has('phone'))
                                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="dob">DOB </label>
                                <div class="col-md-6">
                                        
                                        <input type="text" id="example-datepicker5" name="dob" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$edit->dob }}">
                                    
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="age">Age </label>
                                <div class="col-md-6">
                                        <input type="text" id="age" name="age" class="form-control" value="{{ @$edit->age }}">
                                    
                                </div>
                        </div> 
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="gender">Gender <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                       
                                        <label class="radio-inline" for="example-inline-radio1">
                                                <input type="radio" id="example-inline-radio1" name="gender" value="1" @if (@$edit->gender==1)
                                                    checked
                                                @endif> Male
                                            </label>    
                                            <label class="radio-inline" for="example-inline-radio2">
                                                    <input type="radio" id="example-inline-radio2" name="gender" value="2" @if (@$edit->gender==2)
                                                    checked
                                                @endif> Female
                                                </label>    
                                                @if ($errors->has('gender'))
                                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                                            @endif
                                            </div>
                        </div> 
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="address">Address </label>
                                <div class="col-md-6">
                                        <input type="text" id="address" name="address" class="form-control" value="{{ @$edit->address }}">
                                    
                                </div>
                        </div>                   
        <div class="form-group">
            <label class="col-md-4 control-label" for="val_username">Profile Photo </label>
            <div class="col-md-6">
                @if (@$edit->profile_img)
                <img src="{{ url('') }}/upload/profileimages/{{@$edit->profile_img}}" class="w-50 rounded-circle" alt="{{@$edit->name}}" width="100">
                
                @endif
                
                    <input class="file-upload__input" type="file" name="profile_img">
                    
            </div>
        </div>
        
        
        <div class="form-group">
                <label class="col-md-4 control-label" for="val_username">Status </label>
                <div class="col-md-6">
                    <label class="switch switch-primary">
                        <input type="checkbox" @if (@$edit->status==1)
                        checked                        
                        @endif  name="status" value="1"><span></span>
                    </label>
                        
                </div>
            </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection