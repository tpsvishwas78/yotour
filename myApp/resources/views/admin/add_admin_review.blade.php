@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-chat"></i>Review Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Review</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submitadminreview')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group">
                        <label class="col-md-4 control-label" for="product">Products <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                
                              <select class="form-control  select-select2" name="product" id="product">
                            <option value="">Select</option> 
                            @foreach ($products as $product)
                            <option @if (@$edit->product_id==$product->id)
                                selected
                            @endif value="{{ $product->id }}">{{ $product->product_name }}</option> 
                            @endforeach
                            </select>  
                            @if ($errors->has('product'))
                                                <span class="text-danger">{{ $errors->first('product') }}</span>
                                            @endif
                        </div>
                    </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="customer_name">Customer Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="customer_name" name="customer_name" class="form-control" value="{{ @$edit->customer_name }}">
                            
                        @if ($errors->has('customer_name'))
                                            <span class="text-danger">{{ $errors->first('customer_name') }}</span>
                                        @endif
                    </div>
                </div>  
                <div class="form-group">
                    <label class="col-md-4 control-label" for="customer_profile_img">Profile Photo </label>
                    <div class="col-md-6">
                        @if (@$edit->customer_profile_img)
                        <img src="{{ url('') }}/upload/profileimages/{{@$edit->customer_profile_img}}" class="w-50 rounded-circle" alt="{{@$edit->customer_profile_img}}" width="100">
                        
                        @endif
                        
                            <input class="file-upload__input" type="file" name="profile_img">
                            
                    </div>
                </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="rating">Rating <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                       
                                        <label class="radio-inline" for="example-inline-radio1">
                                                <input type="radio" id="example-inline-radio1" name="rating" value="1" @if (@$edit->rating==1)
                                                    checked
                                                @endif> 1
                                            </label>    
                                            <label class="radio-inline" for="example-inline-radio2">
                                                    <input type="radio" id="example-inline-radio2" name="rating" value="2" @if (@$edit->rating==2)
                                                    checked
                                                @endif> 2
                                            </label> 
                                            <label class="radio-inline" for="example-inline-radio3">
                                                <input type="radio" id="example-inline-radio3" name="rating" value="3" @if (@$edit->rating==3)
                                                checked
                                            @endif> 3
                                        </label>
                                        <label class="radio-inline" for="example-inline-radio4">
                                            <input type="radio" id="example-inline-radio4" name="rating" value="4" @if (@$edit->rating==4)
                                            checked
                                        @endif> 4
                                    </label>
                                    <label class="radio-inline" for="example-inline-radio5">
                                        <input type="radio" id="example-inline-radio5" name="rating" value="5" @if (@$edit->rating==5)
                                        checked
                                    @endif> 5
                                </label>   
                                                @if ($errors->has('rating'))
                                                <span class="text-danger">{{ $errors->first('rating') }}</span>
                                            @endif
                                            </div>
                        </div> 
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="comment">Comment <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                        
                                        <textarea name="comment" class="form-control" id="comment" cols="30" rows="10">{{ @$edit->comment }}</textarea>
                                        @if ($errors->has('comment'))
                                        <span class="text-danger">{{ $errors->first('comment') }}</span>
                                    @endif
                                </div>
                        </div>                   
        
        
        
        <div class="form-group">
                <label class="col-md-4 control-label" for="val_username">Status </label>
                <div class="col-md-6">
                    <label class="switch switch-primary">
                        <input type="checkbox" @if (@$edit->status==1)
                        checked                        
                        @endif  name="status" value="1"><span></span>
                    </label>
                        
                </div>
            </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection