@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-chat"></i>Combo Product Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Combo</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submitcomboproduct')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group">
                        <label class="col-md-4 control-label" for="product">Products <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <select class="form-control  select-select2" name="product[]" id="product" multiple>
                                @foreach ($products as $product)
                                <?php
                                    @$check=\DB::table('combo_products')->where('combo_id',@$edit->id)->where('product_id',@$product->id)->first();
                                ?>
                                <option @if (@$check->product_id==$product->id)
                                    selected
                                @endif value="{{ $product->id }}">{{ $product->product_name }}</option> 
                                @endforeach
                            </select>
                            @if ($errors->has('product'))
                                <span class="text-danger">{{ $errors->first('product') }}</span>
                            @endif
                        </div>
                    </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="title">Title <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="title" name="title" class="form-control" value="{{ @$edit->title }}">
                            
                        @if ($errors->has('title'))
                                            <span class="text-danger">{{ $errors->first('title') }}</span>
                                        @endif
                    </div>
                </div>  
                <div class="form-group">
                    <label class="col-md-4 control-label" for="original_price">Original Price <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="original_price" name="original_price" class="form-control" value="{{ @$edit->original_price }}">
                            
                        @if ($errors->has('original_price'))
                                            <span class="text-danger">{{ $errors->first('original_price') }}</span>
                                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="combo_price">Combo Price <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="combo_price" name="combo_price" class="form-control" value="{{ @$edit->combo_price }}">
                            
                        @if ($errors->has('combo_price'))
                                            <span class="text-danger">{{ $errors->first('combo_price') }}</span>
                                        @endif
                    </div>
                </div>
                        
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="description">Description <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                        <textarea name="description" class="form-control" id="description" cols="30" rows="10">{{ @$edit->description }}</textarea>
                                        @if ($errors->has('description'))
                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                        </div>                   
        
        
        <div class="form-group">
                <label class="col-md-4 control-label" for="status">Status </label>
                <div class="col-md-6">
                    <label class="switch switch-primary">
                        <input type="checkbox" @if (@$edit->status==1)
                        checked                        
                        @endif  name="status" value="1"><span></span>
                    </label>
                        
                </div>
            </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection