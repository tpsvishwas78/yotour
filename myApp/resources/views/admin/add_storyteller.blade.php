@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-group"></i>Storyteller Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Storyteller</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation-storyteller"  method="POST" action="{{url('admin/submitstoryteller')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group">
                        <label class="col-md-4 control-label" for="name">City of Operation <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                
                              <select class="form-control  select-select2" name="destination" id="destination">
                            <option value="">Select</option> 
                            @foreach ($destinations as $destination)
                            <option @if (@$edit->destination_id==$destination->id)
                                selected
                            @endif value="{{ $destination->id }}">{{ $destination->name }}</option> 
                            @endforeach
                            </select>  
                            @if ($errors->has('destination'))
                                                <span class="text-danger">{{ $errors->first('destination') }}</span>
                                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Tour Types <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                    
                                  <select class="form-control  select-select2" name="tourtype" id="tourtype">
                                <option value="">Select</option> 
                                @foreach ($tourtypes as $tourtype)
                                <option @if (@$edit->tour_type_id==$tourtype->id)
                                    selected
                                @endif value="{{ $tourtype->id }}">{{ $tourtype->name }}</option> 
                                @endforeach
                                </select>  
                                @if ($errors->has('tourtype'))
                                                    <span class="text-danger">{{ $errors->first('tourtype') }}</span>
                                @endif
                            </div>
                        </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Name (as per submitted ID proof) <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="name" name="name" class="form-control" value="{{ @$edit->name }}">
                            
                        @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                    </div>
                </div>  
                <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Profile Photo </label>
                        <div class="col-md-6">
                            @if (@$edit->profile_img)
                            <img src="{{ url('') }}/upload/profileimages/{{@$edit->profile_img}}" class="w-50 rounded-circle" alt="{{@$edit->name}}" width="100">
                            
                            @endif
                            
                                <input class="file-upload__input" type="file" name="profile_img">
                                
                        </div>
                    </div>
                <div class="form-group">
                        <label class="col-md-4 control-label" for="email">Email ID<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                <input type="text" id="email" name="email" class="form-control" value="{{ @$edit->email }}">
                                
                            @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                        </div>
                    </div> 
                    <div class="form-group">
                            <label class="col-md-4 control-label" for="phone">Phone number <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                    <input type="text" id="phone" name="phone" class="form-control" value="{{ @$edit->phone }}">
                                    
                                @if ($errors->has('phone'))
                                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="emergency_number">Emergency contact number <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                        <input type="text" id="emergency_number" name="emergency_number" class="form-control" value="{{ @$edit->emergency_number }}">
                                        
                                    @if ($errors->has('emergency_number'))
                                                        <span class="text-danger">{{ $errors->first('emergency_number') }}</span>
                                                    @endif
                                </div>
                            </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="dob">D.O.B (as per submitted ID proof) <span class="text-danger">*</span>                                   </label>
                                <div class="col-md-6">
                                        
                                        <input type="text" id="example-datepicker5" name="dob" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$edit->dob }}" autocomplete="off">
                                        @if ($errors->has('dob'))
                                                    <span class="text-danger">{{ $errors->first('dob') }}</span>
                                                @endif
                                    
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="recruited_form">Recruited from <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="recruited_form" name="recruited_form" class="form-control" value="{{ @$edit->recruited_form }}">
                                        @if ($errors->has('recruited_form'))
                                        <span class="text-danger">{{ $errors->first('recruited_form') }}</span>
                                    @endif
                                </div>
                        </div> 
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="doj">Date of Joining (date of receiving signed MOA)  <span class="text-danger">*</span>                                   </label>
                                <div class="col-md-6">
                                        
                                        <input type="text" id="example-datepicker5" name="doj" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{{ @$edit->doj }}" autocomplete="off">
                                        @if ($errors->has('doj'))
                                                    <span class="text-danger">{{ $errors->first('doj') }}</span>
                                                @endif
                                    
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="poj">Period of Joining <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="poj" name="poj" class="form-control" value="{{ @$edit->poj }}">
                                        @if ($errors->has('poj'))
                                        <span class="text-danger">{{ $errors->first('poj') }}</span>
                                    @endif
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="ptf">Per tour fee (MOA Terms)<span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="ptf" name="ptf" class="form-control" value="{{ @$edit->ptf }}">
                                        @if ($errors->has('ptf'))
                                        <span class="text-danger">{{ $errors->first('ptf') }}</span>
                                    @endif
                                </div>
                        </div> 
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="ptp">Paid tour % (MOA Terms)<span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="ptp" name="ptp" class="form-control" value="{{ @$edit->ptp }}">
                                        @if ($errors->has('ptp'))
                                        <span class="text-danger">{{ $errors->first('ptp') }}</span>
                                    @endif
                                </div>
                        </div> 
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="allowance">Conveyance Allowance<span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="allowance" name="allowance" class="form-control" value="{{ @$edit->allowance }}">
                                        @if ($errors->has('allowance'))
                                        <span class="text-danger">{{ $errors->first('allowance') }}</span>
                                    @endif
                                </div>
                        </div>
                        
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="local_address">Local Address <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="local_address" name="local_address" class="form-control" value="{{ @$edit->local_address }}">
                                        @if ($errors->has('local_address'))
                                        <span class="text-danger">{{ $errors->first('local_address') }}</span>
                                    @endif
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="education">Education <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="education" name="education" class="form-control" value="{{ @$edit->education }}">
                                        @if ($errors->has('education'))
                                        <span class="text-danger">{{ $errors->first('education') }}</span>
                                    @endif
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="blood_group">Blood Group <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="blood_group" name="blood_group" class="form-control" value="{{ @$edit->blood_group }}">
                                        @if ($errors->has('blood_group'))
                                        <span class="text-danger">{{ $errors->first('blood_group') }}</span>
                                    @endif
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="id_proof_number">ID Proof Number <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="id_proof_number" name="id_proof_number" class="form-control" value="{{ @$edit->id_proof_number }}">
                                        @if ($errors->has('id_proof_number'))
                                        <span class="text-danger">{{ $errors->first('id_proof_number') }}</span>
                                    @endif
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="id_proof_type">ID Proof Type <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="id_proof_type" name="id_proof_type" class="form-control" value="{{ @$edit->id_proof_type }}">
                                        @if ($errors->has('id_proof_type'))
                                        <span class="text-danger">{{ $errors->first('id_proof_type') }}</span>
                                    @endif
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="police_clearance_certificate">Police Clearance Certificate <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        
                                <select name="police_clearance_certificate" id="police_clearance_certificate" class="form-control">
                                <option value="">Select</option> 
                                <option @if (@$edit->police_clearance_certificate=='pending')
                                    selected
                                @endif value="pending">Pending</option>
                                <option @if (@$edit->police_clearance_certificate=='obtained')
                                        selected
                                    @endif value="obtained">Obtained</option>   
                                </select> 
                                @if ($errors->has('police_clearance_certificate'))
                                <span class="text-danger">{{ $errors->first('police_clearance_certificate') }}</span>
                            @endif   
                                </div>
                        </div>
                    </fieldset>
                <fieldset>
                        <legend><i class="fa fa-angle-right"></i>Bank Account Details</legend>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="account_no">Account No <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="account_no" name="account_no" class="form-control" value="{{ @$edit->account_no }}">
                                        @if ($errors->has('account_no'))
                                        <span class="text-danger">{{ $errors->first('account_no') }}</span>
                                    @endif 
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="account_holder_name">Account Holder Name <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="account_holder_name" name="account_holder_name" class="form-control" value="{{ @$edit->account_holder_name }}">
                                        @if ($errors->has('account_holder_name'))
                                        <span class="text-danger">{{ $errors->first('account_holder_name') }}</span>
                                    @endif
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="ifsc_code">Ifsc Code <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="ifsc_code" name="ifsc_code" class="form-control" value="{{ @$edit->ifsc_code }}">
                                        @if ($errors->has('ifsc_code'))
                                        <span class="text-danger">{{ $errors->first('ifsc_code') }}</span>
                                    @endif 
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="bank_name">Bank Name <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="bank_name" name="bank_name" class="form-control" value="{{ @$edit->bank_name }}">
                                        @if ($errors->has('bank_name'))
                                        <span class="text-danger">{{ $errors->first('bank_name') }}</span>
                                    @endif 
                                </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="branch">Branch <span class="text-danger">*</span> </label>
                                <div class="col-md-6">
                                        <input type="text" id="branch" name="branch" class="form-control" value="{{ @$edit->branch }}">
                                        @if ($errors->has('branch'))
                                        <span class="text-danger">{{ $errors->first('branch') }}</span>
                                    @endif
                                </div>
                        </div>
                        
       
        
        
        <div class="form-group">
                <label class="col-md-4 control-label" for="val_username">Status </label>
                <div class="col-md-6">
                    <label class="switch switch-primary">
                        <input type="checkbox" @if (@$edit->status==1)
                        checked                        
                        @endif  name="status" value="1"><span></span>
                    </label>
                        
                </div>
            </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection