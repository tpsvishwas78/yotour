@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-group"></i> Experience Products
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2>All Experience Products</h2>
    

</div>
<!-- END Form Validation Example Title -->
@if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif

                <div class="table-responsive">
                        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">SNO</th>
                                    <th>Product Title</th>
                                    <th>Product Type</th>
                                    <th>City</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $key => $result)
                                <?php
        @$city=\DB::table('destinations')->where('id',$result->city)->first();
    ?>
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    
                                    <td>{{ @$result->product_name }}</td>
                                    <td>
                                        @if (@$result->product_sub_type==1)
                                        Tour/Activity
                                        @elseif(@$result->product_sub_type==2)
                                        Tickets
                                        @elseif(@$result->product_sub_type==3)
                                        Transport/Rental
                                        @else
                                        Services
                                        @endif
                                    </td>

                                   <td>{{ @$city->name }}</td>

                                    <td class="text-center">
                                        @if (@$result->status==1)
                                        <a href="{{ url('') }}/admin/status-exp-product/{{ $result->id }}" class="label label-success" onClick="return confirm('Are you sure you want to Deactive?');">Active</a>
                                        @else
                                        <a onClick="return confirm('Are you sure you want to Active?');" href="{{ url('') }}/admin/status-exp-product/{{ $result->id }}" class="label label-danger">Deactive</a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            @if (@$result->product_sub_type==1)
                                            <a href="{{ url('') }}/admin/experience/add-tour-activity/{{ $result->id }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @elseif(@$result->product_sub_type==2)
                                            <a href="{{ url('') }}/admin/experience/add-tickets/{{ $result->id }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @elseif(@$result->product_sub_type==3)
                                            <a href="{{ url('') }}/admin/experience/add-transport-rental/{{ $result->id }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @else
                                            <a href="{{ url('') }}/admin/experience/add-tour-activity/{{ $result->id }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            
                                            
                                            <a onClick="return confirm('Are you sure you want to Delete?');" href="{{ url('') }}/admin/delete-exp-product/{{ $result->id }}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection