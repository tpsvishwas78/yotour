@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Setting
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> City</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submitdestination')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf 
                <div class="form-group">
                    <label class="col-md-4 control-label" for="country">Country <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                           <select name="country" id="country" class="form-control">
                               <option value="">Select</option>
                               @foreach ($countries as $country)
                                    <option @if (@$edit->country_id==$country->id)
                                        selected
                                    @endif value="{{ $country->id }}">{{ $country->name }}</option>
                               @endforeach
                           </select>
                            
                        @if ($errors->has('country'))
                                            <span class="text-danger">{{ $errors->first('country') }}</span>
                                        @endif
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="name" name="name" class="form-control" value="{{ @$edit->name }}">
                            
                        @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                    </div>
                </div>                     
        <div class="form-group">
            <label class="col-md-4 control-label" for="val_username">Featured Photo </label>
            <div class="col-md-6">
                @if (@$edit->featured_img)
                <img src="{{ url('') }}/upload/images/{{@$edit->featured_img}}" class="w-50 rounded-circle" alt="{{@$edit->name}}" width="100">
                
                @endif
                
                    <input class="file-upload__input" type="file" name="featured_img">
                    
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="val_username">Banner Photo </label>
            <div class="col-md-6">
                @if (@$edit->banner_img)
                <img src="{{ url('') }}/upload/images/{{@$edit->banner_img}}" class="w-50 rounded-circle" alt="{{@$edit->name}}" width="100">
                
                @endif
                
                    <input class="file-upload__input" type="file" name="banner_img">
                    
            </div>
        </div>
        
        
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Description </label>
            <div class="col-md-8">
                <textarea id="textarea-ckeditor" name="description" class="ckeditor">{{ @$edit->description }}</textarea>
                    
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="val_username">Is Top </label>
            <div class="col-md-6">
                <label class="switch switch-primary">
                    <input type="checkbox" @if (@$edit->is_top==1)
                    checked                        
                    @endif  name="is_top" value="1"><span></span>
                </label>
                    
            </div>
        </div>
        <div class="form-group">
                <label class="col-md-4 control-label" for="val_username">Status </label>
                <div class="col-md-6">
                    <label class="switch switch-primary">
                        <input type="checkbox" @if (@$edit->status==1 || @$edit->status=='')
                        checked                        
                        @endif  name="status" value="1"><span></span>
                    </label>
                        
                </div>
            </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection