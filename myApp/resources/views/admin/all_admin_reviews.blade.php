@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-chat"></i>Review Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2>All Reviews</h2>
    <div class="addbutton">
        <a class="btn btn-info" href="{{ url('') }}/admin/add-admin-review"><i class="gi gi-plus"></i> Add New Review</a>
    </div>

</div>
<!-- END Form Validation Example Title -->
@if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif

                <div class="table-responsive">
                        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">SNO</th>
                                    <th class="text-center">Photo</th>
                                    <th>Customer Name</th>
                                    <th>Product Name</th>
                                    <th>Rating</th>
                                    <th>Comment</th>
                                    <th>Review Date</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($results as $key => $result)
                                <?php
                                @$product=\DB::table('products')->where('id',@$result->product_id)->first();
                                ?>
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td class="text-center">
                                            @if (@$result->customer_profile_img)
                                            <img src="{{ url('') }}/upload/profileimages/{{@$result->customer_profile_img}}" class="w-50 rounded-circle" width="100">
                                            @else
                                            <img src="{{ url('') }}/assets/admin/img/placeholders/avatars/avatar11.jpg" alt="avatar" class="img-circle">
                                            @endif
                                        
                                    </td>
                                    <td>{{ @$result->customer_name }}</td>
                                    <td>{{ @$product->product_name }}</td>
                                    <td>{{ @$result->rating }}</td>
                                    <td>{{ @$result->comment }}</td>
                                    <td>{{ date('d M, Y', strtotime(@$result->created_at)) }}</td>
                                    <td class="text-center">
                                        @if (@$result->status==1)
                                        <a href="{{ url('') }}/admin/status-review/{{ $result->id }}" class="label label-success" onClick="return confirm('Are you sure you want to Deactive?');">Active</a>
                                        @else
                                        <a onClick="return confirm('Are you sure you want to Active?');" href="{{ url('') }}/admin/status-review/{{ $result->id }}" class="label label-danger">Deactive</a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ url('') }}/admin/add-admin-review/{{ $result->id }}" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                            <a onClick="return confirm('Are you sure you want to Delete?');" href="{{ url('') }}/admin/delete-review/{{ $result->id }}" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection