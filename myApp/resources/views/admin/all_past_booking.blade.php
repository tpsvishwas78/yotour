@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-group"></i>Booking Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2>All Past Bookings</h2>
    <a style="float:right;" class="btn btn-info" href="{{ url('') }}/admin/all-booking">Go Back</a>
</div>

<!-- END Form Validation Example Title -->
@if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif

                <div class="table-responsive">
                        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Booking ID</th>
                                  <th class="text-center">Booking Channel</th>
                                  <th>Guest Name</th>
                                  <th>Product</th>
                                  <th>Product Type</th>
                                  <th>City</th>
                                  <th>Booking Date</th>
                                  <th>Participation Date</th>
                                  <th>Tour Time</th>
                                  <th>Payment</th>
                                  <th>No. Of People</th>
                                <th>Storyteller</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>Tour Expenses</th>
                                <th>Cash Received</th>
                                <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($results as $key => $result)
                                    
                                <?php
@$destination=\DB::table('destinations')->where('id',$result->destination_id)->first();
@$channel=\DB::table('channels')->where('id',$result->channel_id)->first();
@$tour=\DB::table('products')->where('id',$result->tour_id)->first();
@$customer=\DB::table('users')->where('id',$result->customer_id)->first();
@$storyteller=\DB::table('storytellers')->where('id',$result->storyteller)->first();
                                    ?>
                                <tr>
                                    <td class="text-center">{{ $result->id }}</td>
                                  <td>{{ @$channel->name }}</td>
                                  <td>{{ @$customer->name }}</td>
                                  <td>{{ @$tour->product_name }}</td>
                                  <td>
                                      @if ($result->tour_type_id==1)
                                          Experience
                                      @else
                                          Holiday
                                      @endif
                                  </td>
                                  <td>{{ @$destination->name }}</td>
                                  <td>{{ date('d M, Y', strtotime(@$result->date)) }}</td>
                                  <td>{{ date('d M, Y', strtotime(@$result->created_at)) }}</td>
                                  <td>{{ @$result->time }}</td>
                                  <td>{{ @$result->booking_amount }}</td>
                                  <td>{{ @$result->number_of_people }}</td>
                                    <td>{{ @$storyteller->name }}</td>
                                    
                                    
                                    <td class="text-center">
                                            @if (@$result->status==1)
                                            <a href="javascript:void(0)" class="label label-success" >Confirmed</a>
                                            @elseif(@$result->status==0)
                                            <a  href="javascript:void(0)" class="label label-warning">Unconfirmed</a>
                                            @else
                                            <a  href="javascript:void(0)" class="label label-danger">Canceled</a>
                                            @endif
                                    </td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection