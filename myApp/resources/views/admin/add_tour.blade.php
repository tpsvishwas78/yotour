@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-globe"></i>Tour Management
</h1>
</div>
</div>
<?php
@$images=\DB::table('tourimages')->where('tour_id',@$edit->id)->get();
?>

<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2><?= ((@$edit->id) ? 'Update' : 'Add New') ?> Tour</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form id="form-validation"  method="POST" action="{{url('admin/submittour')}}" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <fieldset>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif
                
                @csrf
                <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Destination <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                                <select name="destination" id="destination" class="form-control select-select2">
                                    <option value="">Select</option>
                                    @foreach ($destinations as $destination)
                                    <option @if (@$edit->destination_id==$destination->id)
                                        selected
                                    @endif value="{{ $destination->id }}">{{ $destination->name }}</option>
                                    @endforeach
                                    
                                </select>
                                
                            @if ($errors->has('destination'))
                                                <span class="text-danger">{{ $errors->first('destination') }}</span>
                                            @endif
                        </div>
                    </div> 
                    <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Tour Type <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                    <select name="type" id="type" class="form-control select-select2">
                                        <option value="">Select</option>
                                        @foreach ($tourtypes as $tourtype)
                                        <option @if (@$edit->type==$tourtype->id)
                                            selected
                                        @endif value="{{ $tourtype->id }}">{{ $tourtype->name }}</option>
                                        @endforeach
                                        
                                    </select>
                                    
                                @if ($errors->has('type'))
                                                    <span class="text-danger">{{ $errors->first('type') }}</span>
                                                @endif
                            </div>
                        </div>  
                <div class="form-group">
                    <label class="col-md-4 control-label" for="title">Title <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <input type="text" id="title" name="title" class="form-control" value="{{ @$edit->title }}">
                            
                        @if ($errors->has('title'))
                                            <span class="text-danger">{{ $errors->first('title') }}</span>
                                        @endif
                    </div>
                </div> 
                <div class="form-group">
                        <label class="col-md-4 control-label" for="price">Price </label>
                        <div class="col-md-6">
                                <input type="text" id="price" name="price" class="form-control" value="{{ @$edit->price }}">
                                
                            
                        </div>
                    </div>                     
        <div class="form-group">
            <label class="col-md-4 control-label" for="val_username">Featured Photo </label>
            <div class="col-md-6">
                @if (@$edit->featured_img)
                <img src="{{ url('') }}/upload/images/{{@$edit->featured_img}}" class="w-50 rounded-circle" alt="{{@$edit->name}}" width="100">
                
                @endif
                
                    <input class="file-upload__input" type="file" name="featured_img">
                    
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="val_username">Tour Photos </label>
            <div class="col-md-6">
                <div class="controls">
           
                    <div class="entry input-group col-xs-3">
                      
                   
                      <input class="btn btn-default" name="gallary[]" type="file">
                      <span class="input-group-btn">
                    <button class="btn btn-success btn-add" type="button">
                                      <span class="gi gi-plus"></span>
                      </button>
                      </span>
                    </div>
                 
                </div>
                @if (@$images)
@foreach (@$images as $image)
<p>
<img width="100" src="{{ url('') }}/upload/images/{{ $image->image }}" alt="">
{{-- <a href="{{ url('/admin/gamedeletegalleryimage') }}/{{ $image->id }}"><i class="zmdi zmdi-delete zmdi-hc-fw removeimg"></i></a> --}}

<a href="javascript:void(0);"><i id="{{ $image->id }}" class="gi gi-remove removeimg"></i></a> 
</p>
@endforeach
@endif
                    
            </div>
        </div>
        <div class="form-group">
                <label class="col-md-4 control-label" for="location">Location </label>
                <div class="col-md-6">
                        <input type="text" id="location" name="location" class="form-control" value="{{ @$edit->location }}">
                        
                   
                </div>
            </div>
            
        <div class="form-group">
                <label class="col-md-4 control-label" for="email">Short Description </label>
                <div class="col-md-6">
                    
                     <textarea name="short_description" id="" cols="30" rows="10" style="width:100%;">{{ @$edit->short_description }}</textarea>   
                </div>
            </div>    
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Description </label>
            <div class="col-md-8">
                <textarea id="textarea-ckeditor" name="description" class="ckeditor">{{ @$edit->description }}</textarea>
                    
            </div>
        </div>
        
        <div class="form-group">
                <label class="col-md-4 control-label" for="val_username">Status </label>
                <div class="col-md-6">
                    <label class="switch switch-primary">
                        <input type="checkbox" @if (@$edit->status==1)
                        checked
                        @endif  name="status" value="1"><span></span>
                    </label>
                        
                </div>
            </div>

        <input type="hidden" name="id" value="{{ @$edit->id }}">
        
       
    </fieldset>
    
    <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
        </div>
    </div>
</form>
<!-- END Form Validation Example Content -->


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
<script>
    $(document).on('click', '.removeimg', function(e)
    {

         var id=$(this).attr('id');
         $(this).closest('p').remove();
        var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ url('') }}/admin/deletegalleryimage",
                 method:"POST",
                 data:{id:id, _token:_token},
                 success:function(data){
                    
                 }
                });
        
    });
 $(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="gi gi-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});
</script>
@endsection