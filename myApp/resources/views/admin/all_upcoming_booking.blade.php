@extends('admin.layouts.master')
@section('content')
<div id="page-content">
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-group"></i>Booking Management
</h1>
</div>
</div>


<div class="row">
<div class="col-md-12">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
    <h2>All Future Bookings</h2>
    <a style="float:right;" class="btn btn-info" href="{{ url('') }}/admin/all-booking">Go Back</a>
</div>

<!-- END Form Validation Example Title -->
@if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                        Session::forget('success');
                    @endphp
                </div>
                @endif

                @if(Session::has('warning'))
                <div class="alert alert-warning">
                    {{ Session::get('warning') }}
                    @php
                        Session::forget('warning');
                    @endphp
                </div>
                @endif

                <div class="table-responsive">
                        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                  <th class="text-center">Booking ID</th>
                                  <th class="text-center">Booking Channel</th>
                                  <th>Guest Name</th>
                                  <th>Product</th>
                                  <th>Product Type</th>
                                  <th>City</th>
                                  <th>Booking Date</th>
                                  <th>Participation Date</th>
                                  <th>Tour Time</th>
                                  <th>Payment</th>
                                  <th>No. Of People</th>
                                    <th>Storyteller</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($results as $key => $result)
                                    
                                <?php
@$destination=\DB::table('destinations')->where('id',$result->destination_id)->first();
@$channel=\DB::table('channels')->where('id',$result->channel_id)->first();
@$tour=\DB::table('products')->where('id',$result->tour_id)->first();
@$customer=\DB::table('users')->where('id',$result->customer_id)->first();
@$storyteller=\DB::table('storytellers')->where('id',$result->storyteller)->first();

                                    ?>
                                <tr>
                                  <td class="text-center">{{ $result->id }}</td>
                                  <td>{{ @$channel->name }}</td>
                                  <td>{{ @$customer->name }}</td>
                                  <td>{{ @$tour->product_name }}</td>
                                  <td>
                                      @if ($result->tour_type_id==1)
                                          Experience
                                      @else
                                          Holiday
                                      @endif
                                  </td>
                                  <td>{{ @$destination->name }}</td>
                                  <td>{{ date('d M, Y', strtotime(@$result->date)) }}</td>
                                  <td>{{ date('d M, Y', strtotime(@$result->created_at)) }}</td>
                                  <td>{{ @$result->time }}</td>
                                  <td>{{ @$result->booking_amount }}</td>
                                  <td>{{ @$result->number_of_people }}</td>
                                    <td>{{ @$storyteller->name }}</td>
                                    
                                    
                                    <td class="text-center">
                                        @if (@$result->status==1)
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#changestatus{{ $result->id }}" class="label label-success" >Confirmed</a>
                                        @elseif(@$result->status==0)
                                        <a  href="javascript:void(0)" data-toggle="modal" data-target="#changestatus{{ $result->id }}" class="label label-warning">Unconfirmed</a>
                                        @else
                                        <a  href="javascript:void(0)" data-toggle="modal" data-target="#changestatus{{ $result->id }}" class="label label-danger">Canceled</a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#ModifyModal{{ $result->id }}" title="Modify" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i>Modify</a>
                                            
                                        </div>
                                        <!-- Modal -->
<div id="ModifyModal{{ $result->id }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Booking Modify</h4>
            </div>
            <div class="modal-body">
              <a class="btn btn-warning" href="{{ url('') }}/admin/import-booking/{{ $result->id }}">RESCHEDULE TOUR</a>
              <a class="btn btn-info" href="{{ url('') }}/admin/allocate/{{ $result->id }}">CHANGE STORYTELLER</a>
              <a class="btn btn-danger" href="javascript:void(0)" data-toggle="modal" data-target="#cancelmodel">CANCEL TOUR</a>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
      
        </div>
      </div>
      
      <div id="changestatus{{ $result->id }}" class="modal fade" role="dialog">
            <div class="modal-dialog">
          
              <!-- Modal content-->
              <div class="modal-content">
               <form action="{{ url('') }}/admin/changebookingstatus" method="POST">
                @csrf
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Change Booking Status</h4>
                </div>
                <div class="modal-body">
                 <input type="radio" @if ($result->status==0)
                     checked
                 @endif name="status{{ $result->id }}" value="0"><span class="label label-warning"> Unconfirm</span>
                 <input @if ($result->status==1)
                 checked
             @endif type="radio" name="status{{ $result->id }}" value="1"><span class="label label-success"> Confirm</span>
                 <input @if ($result->status==2)
                 checked
             @endif type="radio" name="status{{ $result->id }}" value="2"><span class="label label-danger"> Cancel</span>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="bid" value="{{ $result->id }}">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </form>
              </div>
          
            </div>
          </div>
      <div id="cancelmodel" class="modal fade" role="dialog">
            <div class="modal-dialog">
          
              <!-- Modal content-->
              <div class="modal-content">
                    <form action="{{ url('') }}/admin/cancelbooking" method="POST">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Cancel Tour</h4>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="id" value="{{ $result->id }}">
                    <textarea required name="cancelMsg" id="" cols="30" rows="10" style="width:100%;"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </form>
              </div>
          
            </div>
          </div>
                                    </td>
                                     <td> {{--Remarks--}} </td> 
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>


</div>
<!-- END Validation Block -->
</div>

</div>
</div>
@endsection