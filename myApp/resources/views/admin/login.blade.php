<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Admin | Sign In</title>
	<!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{ url('') }}/assets/admin/css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{ url('') }}/assets/admin/css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{ url('') }}/assets/admin/css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{ url('') }}/assets/admin/css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="{{ url('') }}/assets/admin/js/vendor/modernizr.min.js"></script>
</head>
<body>
	<!-- Login Background -->
	<div id="login-background">
		<!-- For best results use an image with a resolution of 2560x400 pixels (prefer a blurred image for smaller file size) -->
		<img src="{{ url('') }}/assets/admin/img/placeholders/headers/login_header.jpg" alt="Login Background" class="animation-pulseSlow">
	</div>
	<!-- END Login Background -->

	<!-- Login Container -->
	<div id="login-container" class="animation-fadeIn">
		<!-- Login Title -->
		<div class="login-title text-center">
			<h1><i class="gi gi-flash"></i> <strong>YoTour</strong><br><small>Please <strong>Login</strong></small></h1>
		</div>
		<!-- END Login Title -->

		<!-- Login Block -->
		<div class="block push-bit">
			<!-- Login Form -->
			<form action="{{ url('admin/loginSubmit') }}" method="post" id="form-login" class="form-horizontal form-bordered form-control-borderless">
					@csrf
					<div class="form-group">
							<div class="col-xs-12">
					@if ($errors->any())
                            <div class="text-danger">{{ implode('', $errors->all(':message')) }}</div>
							@endif
						</div>
					</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
							<input type="text" id="login-email" name="email" class="form-control input-lg" placeholder="Email">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
							<input type="password" id="login-password" name="password" class="form-control input-lg" placeholder="Password">
						</div>
					</div>
				</div>
				<div class="form-group form-actions">
					
					<div class="col-xs-8 text-right">
						<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
					</div>
				</div>
				
			</form>
			<!-- END Login Form -->

			
		</div>
		<!-- END Login Block -->

		
	</div>
	<!-- END Login Container -->

	

	<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
	<script src="{{ url('') }}/assets/admin/js/vendor/jquery.min.js"></script>
	<script src="{{ url('') }}/assets/admin/js/vendor/bootstrap.min.js"></script>
	<script src="{{ url('') }}/assets/admin/js/plugins.js"></script>
	<script src="{{ url('') }}/assets/admin/js/app.js"></script>

	<!-- Load and execute javascript code used only in this page -->
	<script src="{{ url('') }}/assets/admin/js/pages/login.js"></script>
	<script>$(function(){ Login.init(); });</script>
</body>
</html>
