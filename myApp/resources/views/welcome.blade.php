@extends('layouts.master')
@section('content')
<div class="banner">
     <div class="container">
         <div class="row content">
             <div class="col-lg-4 left">
         
            <h2 class="title">Truly discover the world</h2>
                 <br><br><br>
                <p>Yo Tours® conceptualizes, designs & 
operates its own unique travel experiences</p>
                 
                 
             </div>
             <div class="col-lg-4">
                 
                 
                 
             </div>
             <div class="col-lg-4">
                 <form class="border border-light mt-5 pt-3 top-form">

  <div class="text-left">
        <p class="h4 font-weight-bold"> Let's plan your next tour </p>

     
    </div>
        <label for=" ">Destination</label>
   <input type="text" id="defaultSubscriptionFormPassword" class="form-control mb-2" placeholder="Enter where you are travelling to">

             <label for=" ">Travelers</label>
            <input type="tel" class="form-control mb-3" placeholder="Couple">

            <label for=" ">Experience type</label>
    <input type="email" class="form-control mb-3" placeholder="Walking tour">

 


    <button class="btn btn-info gra-btn btn-block my-2" type="submit">Explore experience</button>

    <div class="text-center">
 


    </div>
</form>
             </div>
         </div>
         

        
        </div>
     
 </div>
 
 
    <style>.small-banner{    padding: 50px 0px;
    margin: 80px;
    background: url('{{url('/')}}/img/green-grass-field-2026451.png');
    background-size: 100%;
        position: relative;
    background-position-y: bottom;
    background-repeat: no-repeat;
        color: #fff;
        
        }
        .small-banner:before {
    content: " ";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    opacity: 0.85;
    background: transparent linear-gradient(90deg, #00000000 0%, #0000000E 22%, #000000E5 65%, #000000 79%, #000000 100%) 0% 0% no-repeat padding-box;
}
        
        
        
        .text-over{
            position: absolute;
              z-index: 10;
        }
        .steps-gal-text { bottom: 0px;
            left: 25%;
        } 
        .steps-gal-text h1{font-size: 18px;
        }
    </style>
<div class="small-banner"> 
  <div class="container">   <div class="row">
  
          <div class="col-lg-6">  </div> 
          <div class="col-lg-6">  <h3>40% off on all tours in the Netherlands</h3>
          <br>
       <p>This Christmas, come to visit the Netherlands and enjoy walking 
tours, local food tasting tours, multi-day trips and many more 
with Yo Tours.</p>
<br><br><br><br><br><br><br><br><br><br>

</div>
 
       
</div>
  
  </div>
   
    </div>
    
    
<div class="container"><div class="row">   
       
        <div class="col-lg-6 steps-gal float-left">
        <div class="black-left col-lg-6">
            <div class="steps-gal-img">     <img class="img-fluid" src="http://placehold.it/1000x550" alt="test" class="img-responsive">
          
            <div class="text-over steps-gal-text">
              <h1>Example headline.</h1></div>
            
        </div>  
          <div class="steps-gal-img">     <img class="img-fluid" src="http://placehold.it/1000x550" alt="test" class="img-responsive">
          
            <div class="text-over steps-gal-text">
              <h1>Example headline.</h1></div>
            
        </div>
        
    </div>
    <div class="black-left col-lg-6 float-left">
            <div class="steps-gal-img">     <img class="img-fluid" src="http://placehold.it/1000x550" alt="test" class="img-responsive">
          
            <div class="text-over steps-gal-text">
              <h1>Example headline.</h1></div>
            
        </div>  
          <div class="steps-gal-img">     <img class="img-fluid" src="http://placehold.it/1000x550" alt="test" class="img-responsive">
          
            <div class="text-over steps-gal-text">
              <h1>Example headline.</h1></div>
            
        </div>
        
    </div>
    
    </div>
    
    
    
    
    </div></div>

    
    <div class="uneven-gallery">
<!--
        
        <div class="message">
  Sorry, your browser does not support CSS Grid. 😅
</div>        
        <div class="message">
  Sorry, your browser does not support CSS Grid. 😅
</div>
-->
<section class="section">
  <h1>Nom Nom Gallery</h1>
  <div class="grid">
    <div class="item item--medium">
      <div class="item__details">
        jelly-o brownie sweet
      </div>
    </div>
    <div class="item item--small">
      <div class="item__details">
        Muffin jelly gingerbread 
      </div>
    </div>
    <div class="item item--medium">
      <div class="item__details">
        sesame snaps chocolate
      </div>
    </div>
    <div class="item item--large">
      <div class="item__details">
        Oat cake
      </div>
    </div>
    <div class="item item--full">
      <div class="item__details">
         jujubes cheesecake
      </div>
    </div>
    <div class="item item--medium">
      <div class="item__details">
        Dragée pudding brownie
      </div>
    </div>
    <div class="item item--large">
      <div class="item__details">
        Oat cake
      </div>
    </div>
    <div class="item">
      <div class="item__details">
        powder toffee
      </div>
    </div>
    <div class="item item--medium">
      <div class="item__details">
        pudding cheesecake
      </div>
    </div>
    <div class="item item--large">
      <div class="item__details">
        toffee bear claw 
      </div>
    </div>
    <div class="item">
      <div class="item__details">
        cake cookie croissant
      </div>
    </div>
    <div class="item item--medium">
      <div class="item__details">
        liquorice sweet roll
      </div>
    </div>
    <div class="item item--medium">
      <div class="item__details">
        chocolate marzipan
      </div>
    </div>
    <div class="item item--large">
      <div class="item__details">
        danish dessert lollipop
      </div>
    </div>
    <div class="item">
      <div class="item__details">
        sugar plum dragée
      </div>
    </div>
  </div>
</div>
        
    </div>
    
    
    
    
    <!-- TESTIMONIALS -->
<section class="testimonials">
	<div class="container">

      <div class="row">
        <div class="col-sm-12">
          <div id="customers-testimonials" class="owl-carousel">

            <!--TESTIMONIAL 1 -->
            <div class="item">
                <div class="owl-card">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
            </div>
            <!--END OF TESTIMONIAL 1 -->
            <!--TESTIMONIAL 2 -->
            <div class="item">
                <div class="owl-card">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
            </div>
            <!--END OF TESTIMONIAL 2 -->
            <!--TESTIMONIAL 3 -->
            <div class="item">
                <div class="owl-card">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
            </div>
            <!--END OF TESTIMONIAL 3 -->
            <!--TESTIMONIAL 4 -->
            <div class="item">
                <div class="owl-card">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
            </div>
            <!--END OF TESTIMONIAL 4 -->
            <!--TESTIMONIAL 5 -->
            <div class="item">
                <div class="owl-card">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
            </div>
            <!--END OF TESTIMONIAL 5 -->
          </div>
        </div>
      </div>
      </div>
    </section>
    <!-- END OF TESTIMONIALS -->
    
    
     
      <div class="container">
     <div class="row">
     <div class="col-lg-6"> <h1 class="oswald">B-2-B Services</h1>  <p class="half-opacity">To always strive to make a delightful, exciting and memorable experience
for the traveler is our only goal</p> </div>
          <div class="col-lg-6"><div class="border-button float-right"> Sell our experiences </div></div>
    
   
    <div class="col-lg-2 services-icons"><img src="img/interview.svg" alt="" class="img-fluid"><h5 class="heading oswald grey-text">MICE</h5></div>
    <div class="col-lg-2 services-icons"><img src="img/school.svg" alt="" class="img-fluid"><h5 class="heading oswald grey-text">Educational Institutions</h5></div>
    <div class="col-lg-2 services-icons"><img src="img/travel.svg" alt="" class="img-fluid"><h5 class="heading oswald grey-text">Corporate Travel</h5></div>
    <div class="col-lg-2 services-icons"><img src="img/flight-information.svg" alt="" class="img-fluid"><h5 class="heading oswald grey-text">Travel Agents</h5></div>
    <div class="col-lg-2 services-icons"><img src="img/room.svg" alt="" class="img-fluid"><h5 class="heading oswald grey-text">Accomodation Partners</h5></div>
    
          </div></div>
    
        
  <div class="container">
    <div class="row mb-5">
    
<style>    .cus-cards{
    
/*    box-shadow: 0px 3px 6px #00000040;*/
border: 1px solid #E7E7E7;padding: 20px;
    background: #F1F1F1;
        filter: drop-shadow(0px 3px 6px #00000040);}
        
  .cus-avtar {
    width: 80px;
    height: 80px;
    margin: 0 auto;
    border-radius: 50%;
    padding: 15px;
    transform: translate(0px, 60px);
    background: #f1f1f1;
}
    .cus-avtar img{ border-radius: 50%;box-shadow: 0px 3px 6px #00000040;border: 1px solid #E7E7E7;}
    .cus-des{text-align: center;
    opacity: 50%;
    font-size: 12px;
    margin-bottom: 0px;
    }
    .cus-name{
        font-size: 18px;
        text-align: center;
        margin-bottom: .2rem;
    }
    .cus-city{    font-size: 12px;
    padding: 0;
    margin: 0;
    text-align: center;}
    
    .cus-content {
    transform: translate(0px, 40px);
}
    .cus-name-city{transform: translate(0px,20px);}
        </style>
    <div class="col-lg-2">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
    
    
        <style>
            @media (min-width: 992px){
.col-lg-2.center {
    -ms-flex: 0 0 25% !important;
    flex: 0 0 25% !important;
    max-width: 25% !important;
    margin: 0 4%;
}
            .col-lg-2.center .cus-cards{    transform: scale(1.3);background: transparent linear-gradient(180deg, #01B148 0%, #06885C 100%) 0% 0% no-repeat; color:#fff;}
            }
            .col-lg-2.center .cus-cards .cus-avtar{    background: linear-gradient(#058f59,#06885c);}
            
        </style>
    <div class="col-lg-2">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
    
    <div class="col-lg-2 center">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
    
    
    <div class="col-lg-2">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
    <div class="col-lg-2">
        <div class="card cus-cards">
        
        <div class="cus-content">   <p class="cus-des">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
        <div class="cus-name-city"> <h4 class="cus-name oswald">Donna Zane</h4>
       <p class="cus-city" >Durban, South Africa</p>
       </div>
      

        </div>
               <div class="cus-avtar"> <img src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="" class="img-fluid" >
            </div>
        </div>
        
    </div>
    
      </div></div>
    
    
    
  <div class="container">
    <div class="row">
       <div class="col-lg-12 mt-3"><div  class="heading"><h1>Wanna be part of us?</h1>
       <p class="half-opacity">Join YoTours family and be a part of the extravaganza</p></div></div>
        <div class="col-lg-6 col-part"><div class="overlay part-text overlay-bottom"><h4 class="oswald">Become a story teller</h4><p>Apply now</p></div><div class="part-img"> <img class="img-fluid" src="img/part.png" alt=""></div></div>
        <div class="col-lg-6 col-part"><div class="overlay part-text overlay-bottom"><h4 class="oswald">Become a story teller</h4><p>Apply now</p></div><div class="part-img"> <img class="img-fluid" src="img/part2.png" alt=""></div></div>
    </div></div>
   
    <main role="main" class="container">


    </main>
    <style>
        .partners-row{padding: 30px 0px 30px 0px}
    </style>
    <div class="container">
       <div class="row partners-row">
       
        <div class="col-lg-2 partner-logos"><img class="img-fluid" src="img/GetYourGuide_company_logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="img/viator_owler_20180831_185805_original.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="img/Image 23.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="img/partner-logo-civitatis.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
   
        </div>    
         <div class="row partners-row">
       
        <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
   
        </div>  
           <div class="row partners-row">
       
        <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
         <div class="col-lg-2 partner-logos"><img class="img-fluid" src="http://www.agnitotechnologies.com/wp-content/uploads/2020/01/agnito-logo.png"></div>
   
        </div>
        
        
    </div>
    @stop