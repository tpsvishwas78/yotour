<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Home - TripNstay</title>
    <link rel="stylesheet" href="{{url('/')}}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700">
    
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/assets/owl.carousel.min.css">  
    <link rel="stylesheet" href="https://themes.audemedia.com/html/goodgrowth/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.6.1/css/pikaday.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('/')}}/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    
    
    
    <style>
       
        
/*        gallary */
        
/*
        @import url('https://fonts.googleapis.com/css?family=Arvo');
 body, html {
	 display: flex;
	 justify-content: center;
	 align-items: center;
	 position: relative;
	 width: 100%;
	 height: 100%;
	 background: #f5f4f4;
	 font-size: 13px;
	 font-family: 'Arvo', monospace;
}
*/
        
        .uneven-gallery{display: grid;}
 @supports (display: grid) {
	 display: block;
}
 .message {
	 border: 1px solid #d2d0d0;
	 padding: 2em;
	 font-size: 1.7vw;
	 box-shadow: -2px 2px 10px 0px rgba(68, 68, 68, 0.4);
}
 @supports (display: grid) {
	 display: none;
}
 .uneven-gallery .section {
	 display: grid;
	 padding: 2rem;
}
 @media screen and (min-width: 768px) {
	 .section {
		 padding: 4rem;
	}
}
 @supports (display: grid) {
	 display: block;
}
 .uneven-gallery h1 {
	 font-size: 2rem;
	 margin: 0 0 1.5em;
}
.uneven-gallery .grid {
	 display: grid;
	 grid-gap: 30px;
	 grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
	 grid-auto-rows: 150px;
	 grid-auto-flow: row dense;
}
 .uneven-gallery .item {
	 position: relative;
	 display: flex;
	 flex-direction: column;
	 justify-content: flex-end;
	 box-sizing: border-box;
	 background: #0c9a9a;
	 color: #fff;
	 grid-column-start: auto;
	 grid-row-start: auto;
	 color: #fff;
	 background: url('https://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/article_thumbnails/reference_guide/cats_and_excessive_meowing_ref_guide/1800x1200_cats_and_excessive_meowing_ref_guide.jpg');
	 background-size: cover;
	 background-position: center;
	 box-shadow: -2px 2px 10px 0px rgba(68, 68, 68, 0.4);
	 transition: transform 0.3s ease-in-out;
	 cursor: pointer;
	 counter-increment: item-counter;
}
         .uneven-gallery .item:nth-of-type(2n) {
	 background-image: url('https://images.unsplash.com/photo-1422255198496-21531f12a6e8?dpr=2&auto=format&fit=crop&w=1500&h=996&q=80&cs=tinysrgb&crop=');
}
 .uneven-gallery .item:nth-of-type(3n) {
	 background-image: url('https://images.unsplash.com/photo-1422255198496-21531f12a6e8?dpr=2&auto=format&fit=crop&w=1500&h=996&q=80&cs=tinysrgb&crop=');
}
 .uneven-gallery .item:nth-of-type(4n) {
	 background-image: url('https://images.unsplash.com/photo-1490914327627-9fe8d52f4d90?dpr=2&auto=format&fit=crop&w=1500&h=2250&q=80&cs=tinysrgb&crop=');
}
 .uneven-gallery .item:nth-of-type(5n) {
	 background-image: url('https://images.unsplash.com/photo-1476097297040-79e9e1603142?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=');
}
 .uneven-gallery .item:nth-of-type(6n) {
	 background-image: url('https://images.unsplash.com/photo-1464652149449-f3b8538144aa?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=');
}
         .uneven-gallery .item:nth-of-type(7n) {
	 background-image: url('https://images.unsplash.com/photo-1464652149449-f3b8538144aa?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=');
}
         .uneven-gallery .item:nth-of-type(8n) {
	 background-image: url('https://images.unsplash.com/photo-1464652149449-f3b8538144aa?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=');
}
                 .uneven-gallery .item:nth-of-type(9n) {
	 background-image: url('https://images.unsplash.com/photo-1464652149449-f3b8538144aa?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=');
}
                 .uneven-gallery .item:nth-of-type(10n) {
	 background-image: url('https://images.unsplash.com/photo-1464652149449-f3b8538144aa?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=');
}
                 .uneven-gallery .item:nth-of-type(11n) {
	 background-image: url('https://images.unsplash.com/photo-1464652149449-f3b8538144aa?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=');
}
                 .uneven-gallery .item:nth-of-type(12n) {
	 background-image: url('https://images.unsplash.com/photo-1464652149449-f3b8538144aa?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=');
}
 .uneven-gallery .item:after {
	 content: '';
	 position: absolute;
	 width: 100%;
	 height: 100%;
	 background-color: black;
	 opacity: 0.3;
	 transition: opacity 0.3s ease-in-out;
}
 .uneven-gallery .item:hover {
	 transform: scale(1.05);
}
 .uneven-gallery .item:hover:after {
	 opacity: 0;
}
 .uneven-gallery .item--medium {
	 grid-row-end: span 2;
}
 .uneven-gallery .item--large {
	 grid-row-end: span 3;
}
 .uneven-gallery .item--full {
	 grid-column-end: auto;
}
 @media screen and (min-width: 768px) {
	 .item--full {
		 grid-column: -1;
		 grid-row-end: span 2;
	}
}
 .uneven-gallery .item__details {
	 position: relative;
	 z-index: 1;
	 padding: 15px;
	 color: #444;
	 background: #fff;
	 text-transform: lowercase;
	 letter-spacing: 1px;
	 color: #828282;
}
 .uneven-gallery .item__details:before {
     display: none;
	 content: counter(item-counter);
	 font-weight: bold;
	 font-size: 1.1rem;
	 padding-right: 0.5em;
	 color: #444;
}
 
/*        gallary */
    </style>
</head>

<body>

@include('layouts.header')
                @yield('content')
                @include('layouts.footer')

                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="{{url('/')}}/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.6.1/pikaday.min.js"></script>
    <script src="{{url('/')}}/js/theme.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/owl.carousel.min.js"></script>
    
    <script>jQuery(document).ready(function($) {
        		"use strict";
        		//  TESTIMONIALS CAROUSEL HOOK
		        $('#customers-testimonials').owlCarousel({
		            loop: true,
		            center: true,
		            items: 5,
		            margin: 0,
//		            autoplay: true,
		            dots:true,
		            autoplayTimeout: 8500,
		            smartSpeed: 450,
//		            responsive: {
//		              0: {
//		                items: 1
//		              },
//		              768: {
//		                items: 3
//		              },
//		              1170: {
//		                items: 5
//		              }
//		            }
		        });
        	});</script>